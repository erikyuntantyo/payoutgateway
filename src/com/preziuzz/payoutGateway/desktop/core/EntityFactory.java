package com.preziuzz.payoutGateway.desktop.core;

import com.preziuzz.payoutGateway.desktop.utility.common.Logger;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import com.preziuzz.payoutGateway.desktop.utility.common.SessionManager;
import com.preziuzz.payoutGateway.desktop.utility.db.PrimaryKey;
import com.preziuzz.payoutGateway.desktop.utility.db.QueryBuilder;
import com.sun.deploy.util.StringUtils;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Represents the factory methods of entity.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class EntityFactory {
    /**
     * Create table from entity.
     *
     * @throws SQLException If a database access error occurs.
     */
    public void createTable() throws SQLException {
        if (isExists()) {
            return;
        }

        Class<? extends EntityFactory> type = getClass();
        List<String> primaryKeys = new Vector<>();

        String dataType;
        String query;

        query = "CREATE TABLE " + type.getSimpleName() + " (";

        for (Field field : type.getDeclaredFields()) {
            if (!field.isAnnotationPresent(PrimaryKey.class)) {
                continue;
            }

            primaryKeys.add(field.getName());
        }

        for (Field field : type.getDeclaredFields()) {
            if (query.contains("NULL")) {
                query += ",";
            }

            if ((field.getType() == Boolean.TYPE) || (field.getType() == Integer.TYPE)) {
                dataType = "INTEGER";
            } else if ((field.getType() == Double.TYPE) || (field.getType() == Float.TYPE)) {
                dataType = "FLOAT";
            } else {
                dataType = "TEXT";
            }

            query += String.format("%s %s ", field.getName(), dataType);

            if ((primaryKeys.size() <= 1) && field.isAnnotationPresent(PrimaryKey.class)) {
                query += "PRIMARY KEY ";
            }

            query += field.isAnnotationPresent(PrimaryKey.class) ? "NOT NULL" : "NULL";
        }

        if (primaryKeys.size() > 1) {
            query += " PRIMARY KEY (" + StringUtils.join(primaryKeys, ",") + ")";
        }

        query += ")";

        DatabaseManager.getConnection().createStatement().execute(query);
    }

    /**
     * Delete data from database.
     *
     * @return True if succeeded saving data, otherwise false.
     * @see boolean
     */
    public boolean delete() {
        try {
            String params = "";
            Class<? extends EntityFactory> type = getClass();
            List<Object> values = new Vector<>();

            PreparedStatement preparedStatement;
            String query;
            Object value;

            query = "delete from " + type.getSimpleName() + " where ";

            for (Field field : type.getDeclaredFields()) {
                if (!field.isAnnotationPresent(PrimaryKey.class)) {
                    continue;
                }

                value = field.get(this);

                if (value != null) {
                    params += (params.length() > 0 ? " and " : "") + field.getName() + " = ?";
                    values.add(value);
                } else {
                    throw new Exception("Primary key: " + field.getName() + " might not have been initialized or value is null.");
                }
            }

            if (values.size() == 0) {
                for (Field field : type.getDeclaredFields()) {
                    value = field.get(this);

                    if (value != null) {
                        params += (params.length() > 0 ? " and " : "") + field.getName() + " = ?";
                        values.add(value);
                    }
                }
            }

            if (values.size() == 0) {
                throw new Exception("No fields has been initialized.");
            }

            try {
                DatabaseManager.getConnection().setAutoCommit(false);

                preparedStatement = DatabaseManager.getConnection().prepareStatement(query + params);

                for (int i = 0; i < values.size(); i++) {
                    preparedStatement.setObject(i + 1, values.get(i));
                }

                preparedStatement.addBatch();
                preparedStatement.executeBatch();

                DatabaseManager.getConnection().commit();
                DatabaseManager.getConnection().setAutoCommit(true);
            } catch (SQLException exc) {
                DatabaseManager.getConnection().rollback();
                throw new Exception("Error thrown while delete " + type.getSimpleName() + ": " + exc.getMessage() + ".");
            }

            Logger.log(
                String.format("Deleted %s table.", type.getSimpleName()),
                this,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.DELETE);

            return true;
        } catch (RuntimeException exc) {
            String initial = "EntityFactory.delete.RuntimeException";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        } catch (Exception exc) {
            String initial = "EntityFactory.delete.Exception";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        }

        return false;
    }

    /**
     * Checks whether the table is exists or not.
     *
     * @return True if the table is exists, otherwise false.
     * @throws SQLException If a database access error occurs.
     * @see boolean
     */
    public boolean isExists() throws SQLException {
        String query = String.format("select name from sqlite_master where type='table' and name='%s';", getClass().getSimpleName());
        ResultSet resultSet = DatabaseManager.getConnection().createStatement().executeQuery(query);

        return resultSet.next();
    }

    /**
     * Saves data to database.
     *
     * @return True if succeeded saving data, otherwise false.
     * @see boolean
     */
    public boolean save() {
        try {
            Map<String, Object> fields = new HashMap<>();
            EntityManager manager = DatabaseManager.buildEntityManager();
            Map<String, Object> primaryKeys = new HashMap<>();
            String query = "";
            Class<? extends EntityFactory> type = getClass();

            boolean doInsert;
            PreparedStatement preparedStatement;

            for (Field field : type.getDeclaredFields()) {
                field.setAccessible(true);

                if (field.isAnnotationPresent(PrimaryKey.class)) {
                    if (!(field.get(this) instanceof String) && !(field.get(this) instanceof Integer)) {
                        throw new IllegalArgumentException("Primary Key must be String or Integer.");
                    }

                    if (field.getAnnotation(PrimaryKey.class).generatable()) {
                        if ((field.get(this) instanceof String) && ((field.get(this) == null) || field.get(this).toString().isEmpty())) {
                            field.set(this, UUID.randomUUID().toString());
                        } else if ((field.get(this) instanceof Integer) && (field.getInt(this) < 1)) {
                            field.set(this, manager.find(getClass(), QueryBuilder.select()).size() + 1);
                        }
                    } else {
                        if (((field.get(this) instanceof String) && ((field.get(this) == null) || field.get(this).toString().isEmpty())) ||
                            ((field.get(this) instanceof Integer) && (field.getInt(this) < 1))) {
                            throw new IllegalArgumentException(String.format("Primary key: %s might not have been initialized or value is null or empty.", field.getName()));
                        }
                    }

                    primaryKeys.put(field.getName(), field.get(this));
                } else {
                    if (field.getType().equals(Boolean.TYPE)) {
                        fields.put(field.getName(), field.getBoolean(this) ? field.getBoolean(this) : 0);
                    } else {
                        fields.put(field.getName(), field.get(this));
                    }
                }
            }

            if (primaryKeys.size() > 0) {
                for (Map.Entry<String, Object> primaryKey : primaryKeys.entrySet()) {
                    query += ((!query.isEmpty()) ? " and " : "") + String.format("%s = '%s'", primaryKey.getKey(), primaryKey.getValue());
                }

                doInsert = manager.find(type, QueryBuilder.select().where(query)).size() == 0;
            } else {
                doInsert = manager.find(type, QueryBuilder.select()).size() == 0;
            }

            query = "";

            for (Map.Entry<String, Object> primaryKey : primaryKeys.entrySet()) {
                if (doInsert) {
                    if (query.isEmpty()) {
                        query = String.format(
                            "insert into %s (%s, @f) values(?, @v)",
                            type.getSimpleName(),
                            primaryKey.getKey());
                    } else {
                        query = query
                            .replace("@f", String.format("%s, @f", primaryKey.getKey()))
                            .replace("@v", "?, @v");
                    }
                } else {
                    if (query.isEmpty()) {
                        query = String.format(
                            "update %s set @s where %s = ?",
                            type.getSimpleName(),
                            primaryKey.getKey());
                    } else {
                        query += String.format(" and %s = ?", primaryKey.getKey());
                    }
                }
            }

            for (Map.Entry<String, Object> field : fields.entrySet()) {
                if (doInsert) {
                    if (query.isEmpty()) {
                        query = String.format(
                            "insert into %s (%s, @f) values(?, @v)",
                            type.getSimpleName(),
                            field.getKey());
                    } else {
                        query = query
                            .replace("@f", String.format("%s, @f", field.getKey()))
                            .replace("@v", "?, @v");
                    }
                } else {
                    if (query.isEmpty()) {
                        query = String.format(
                            "update %s set %s = ?, @s",
                            type.getSimpleName(),
                            field.getKey());
                    } else {
                        query = query
                            .replace("@s", String.format("%s = ?, @s", field.getKey()));
                    }
                }
            }

            if (!query.isEmpty()) {
                query = query.replaceAll(", (@s|@f|@v)", "");

                try {
                    DatabaseManager.getConnection().setAutoCommit(false);

                    preparedStatement = DatabaseManager.getConnection().prepareStatement(query);

                    if (doInsert) {
                        int i = 1;

                        for (Object value : primaryKeys.values()) {
                            preparedStatement.setObject(i++, value);
                        }

                        for (Object value : fields.values()) {
                            preparedStatement.setObject(i++, value);
                        }
                    } else {
                        int i = 1;

                        for (Object value : fields.values()) {
                            preparedStatement.setObject(i++, value);
                        }

                        for (Object value : primaryKeys.values()) {
                            preparedStatement.setObject(i++, value);
                        }
                    }

                    preparedStatement.addBatch();
                    preparedStatement.executeBatch();

                    DatabaseManager.getConnection().commit();
                    DatabaseManager.getConnection().setAutoCommit(true);
                } catch (SQLException exc) {
                    DatabaseManager.getConnection().rollback();
                    throw new Exception("Error thrown while save " + type.getSimpleName() + ": " + exc.getMessage() + ".");
                }

                Logger.log(
                    String.format("Insert/Updated %s table.", type.getSimpleName()),
                    this,
                    SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                    Logger.LogType.UPDATE);

                return true;
            }
        } catch (RuntimeException exc) {
            String initial = "EntityFactory.save.RuntimeException";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        } catch (Exception exc) {
            String initial = "EntityFactory.save.Exception";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        }

        return false;
    }

    /**
     * Set entity data from model type.
     *
     * @param model Object of data model.
     */
    public void setDataFromModel(Object model) {
        try {
            Class<?> type = getClass();
            Class<?> modelType = model.getClass();

            Field f;

            for (Field field : modelType.getDeclaredFields()) {
                try {
                    f = type.getDeclaredField(field.getName());
                } catch (NoSuchFieldException ignore) {
                    continue;
                }

                field.setAccessible(true);
                f.set(this, field.get(model));
            }
        } catch (RuntimeException exc) {
            String initial = "EntityFactory.setDataFromModel.RuntimeException";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        } catch (Exception exc) {
            String initial = "EntityFactory.setDataFromModel.Exception";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        }
    }

    /**
     * Set entity data to data model type.
     *
     * @param modelType Object of data model type parameter.
     * @param <T>       Type paramater of data model.
     * @return Data model type.
     */
    public <T> T setDataToModel(Class<T> modelType) {
        try {
            Class<?> type = getClass();
            T model = modelType.newInstance();

            Field f;

            for (Field field : type.getDeclaredFields()) {
                try {
                    f = model.getClass().getDeclaredField(field.getName());
                } catch (NoSuchFieldException ignore) {
                    continue;
                }

                f.setAccessible(true);
                f.set(model, field.get(this));
            }

            return model;
        } catch (RuntimeException exc) {
            String initial = "EntityFactory.setDataToModel.RuntimeException";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        } catch (Exception exc) {
            String initial = "EntityFactory.setDataToModel.Exception";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        }

        return null;
    }
}
