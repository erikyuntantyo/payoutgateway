package com.preziuzz.payoutGateway.desktop.core.entity;

import com.preziuzz.payoutGateway.desktop.core.EntityFactory;

/**
 * Represents the setting entity.
 *
 * @author Erik P. Yuntantyo
 */
public class Setting extends EntityFactory {
    public String language;
    public String deviceModel;
    public String port;
    public boolean allowTransBank;
    public int logInTimeOut;
    public boolean autoLogOff;
    public boolean showExitConfirmation;
}
