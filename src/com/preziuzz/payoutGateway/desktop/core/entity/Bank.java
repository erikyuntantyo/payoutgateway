package com.preziuzz.payoutGateway.desktop.core.entity;

import com.preziuzz.payoutGateway.desktop.core.EntityFactory;
import com.preziuzz.payoutGateway.desktop.utility.db.PrimaryKey;

/**
 * Represents the bank entity.
 *
 * @author Erik P. Yuntantyo
 */
public class Bank extends EntityFactory {
    @PrimaryKey
    public String code;
    public String name;
    public String alternateName;
}
