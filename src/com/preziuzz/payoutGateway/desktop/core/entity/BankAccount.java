package com.preziuzz.payoutGateway.desktop.core.entity;

import com.preziuzz.payoutGateway.desktop.core.EntityFactory;
import com.preziuzz.payoutGateway.desktop.utility.common.Logger;
import com.preziuzz.payoutGateway.desktop.utility.db.PrimaryKey;

/**
 * Represents the bank account entity.
 *
 * @author Erik P. Yuntantyo
 */
public class BankAccount extends EntityFactory {
    @PrimaryKey
    public String id;
    public String name;
    public String code;
    public String accountNumber;
    @Logger.Invisible(displayValue = "The data is unable to show")
    public String pin;
    public String dialNumber;
    public String encryptedDialNumber;
    public String smsFormat1;
    public String smsFormat2;
    public String smsResponseFormat;
}
