package com.preziuzz.payoutGateway.desktop.core.entity;

import com.preziuzz.payoutGateway.desktop.core.EntityFactory;
import com.preziuzz.payoutGateway.desktop.utility.db.PrimaryKey;

/**
 * Represents the member bank account entity.
 *
 * @author Erik P. Yuntantyo
 */
public class MemberAccount extends EntityFactory {
    @PrimaryKey
    public String id;
    public String code;
    public String name;
    public String bankCode;
    public String bankName;
    public String accountNumber;
    public double totalReceived;
    public int createdDate;
    public int capturedDate;
    public int sentDate;
    public int status;
    public String comments;
}
