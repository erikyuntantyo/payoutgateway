package com.preziuzz.payoutGateway.desktop.core.entity;

import com.preziuzz.payoutGateway.desktop.core.EntityFactory;
import com.preziuzz.payoutGateway.desktop.utility.common.Logger;
import com.preziuzz.payoutGateway.desktop.utility.db.PrimaryKey;

/**
 * Represents the user entity.
 *
 * @author Erik P. Yuntantyo
 */
public class User extends EntityFactory {
    @PrimaryKey
    public String id;
    public String bankId;
    public String name;
    @Logger.Invisible(displayValue = "The data is unable to show")
    public String password;
    public int type;
    public boolean active;
    public int createdDate;
    public int lastLogin;
}
