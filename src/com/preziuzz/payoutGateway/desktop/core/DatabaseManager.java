package com.preziuzz.payoutGateway.desktop.core;

import com.preziuzz.payoutGateway.desktop.core.model.SettingModel;
import com.preziuzz.payoutGateway.desktop.core.model.UserModel;
import com.preziuzz.payoutGateway.desktop.core.service.*;
import com.preziuzz.payoutGateway.desktop.utility.common.Logger;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import com.preziuzz.payoutGateway.desktop.utility.db.ConnectionStringBuilder;
import com.preziuzz.payoutGateway.desktop.utility.db.QueryBuilder;
import com.preziuzz.simpleCrypto.Graphy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

/**
 * Represents the database manager methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class DatabaseManager {
    /**
     * <code>Table</code> enum specifies table types.
     */
    public enum Table {
        /**
         * A <code>BANK</code> is represent type of bank table.
         */
        BANK,
        /**
         * A <code>BANK_ACCOUNT</code> is represent type of bank account table.
         */
        BANK_ACCOUNT,
        /**
         * A <code>MEMBER_ACCOUNT</code> is represent type of member bank account table.
         */
        MEMBER_ACCOUNT,
        /**
         * A <code>SETTING</code> is represent type of setting table.
         */
        SETTING,
        /**
         * A <code>USER</code> is represent type of user table.
         */
        USER;

        /**
         * Returns a <code>String</code> object representing this Table's value.
         *
         * @return A string representation of this object.
         */
        @Override
        public String toString() {
            String[] rest = super.toString().split("_");
            String result = "";

            for (String s : rest) {
                result += s.substring(0, 1) + s.substring(1).toLowerCase();
            }

            return result;
        }
    }

    private static Connection connection = null;
    private static EntityManager manager = null;

    /**
     * Create database connection.
     *
     * @param builder The connection string builder.
     * @throws Exception If exceptional condition has occured.
     */
    public static void createConnection(ConnectionStringBuilder builder) throws Exception {
        if (connection == null) {
            builder.loadClass();

            if ((builder.userId == null) && (builder.password == null)) {
                connection = DriverManager.getConnection(builder.connectionString);
            } else {
                connection = DriverManager.getConnection(builder.connectionString, builder.userId, builder.password);
            }
        }
    }

    /**
     * Build entity manager.
     *
     * @return Entity manager instance.
     * @see EntityManager
     */
    public static EntityManager buildEntityManager() {
        if (manager == null) {
            manager = EntityManager.getInstance();
            manager.setConnection(connection);
        }

        return manager;
    }

    /**
     * Close database connection.
     *
     * @throws SQLException If database connection cannot be closed.
     */
    public static void closeConnection() throws SQLException {
        connection.close();
    }

    /**
     * @return True if succeeded create default data, otherwise false.
     */
    public static boolean createDefaultData() {
        UserModel userModel = new UserModel();
        SettingModel settingModel = new SettingModel();

        userModel.setCreatedDate(Calendar.getInstance().getTime());
        userModel.setId("admin");
        userModel.setLastLogin(Calendar.getInstance().getTime());
        userModel.setName("Administrator");

        try {
            userModel.setPassword(Graphy.encode("admin123"));
        } catch (Exception exc) {
            Logger.log(exc, "DatabaseManager.createDefaultData.encode.Exception", "admin", Logger.LogType.ERROR);
            return false;
        }

        userModel.setActive(true);
        userModel.setType(UserModel.UserType.ADMINISTRATOR);

        if (UserService.update(userModel)) {
            settingModel.setLanguage("id-ID");
            settingModel.setPort("COM5");

            return SettingService.update(settingModel);
        }

        return false;
    }

    /**
     * Create tables to database.
     *
     * @return True if succeeded create tables, otherwise false.
     */
    public static boolean createTables() {
        try {
            BankAccountService.createTable();
            BankService.createTable();
            MemberAccountService.createTable();
            SettingService.createTable();
            UserService.createTable();

            Logger.log("Created system tables.", "admin", Logger.LogType.ADMINISTRATION);

            return true;
        } catch (SQLException exc) {
            String initial = "DatabaseManager.createTables.SQLException";

            MessageDialog.showError(exc, initial);
            Logger.log(exc, initial, "admin", Logger.LogType.ERROR);

            return false;
        }
    }

    /**
     * Get current database connection.
     *
     * @return Current database connection.
     * @see Connection
     */
    public static Connection getConnection() {
        return connection;
    }

    /**
     * Get list of table name in the database.<br />
     * Query: <code>select name from sqlite_master where type='table';</code>
     *
     * @return List of table name in the table.
     * @see List
     */
    public static List<String> getTables() {
        return buildEntityManager().findSingleColumn("sqlite_master", String.class, QueryBuilder.select("name").where("type='table'"));
    }
}
