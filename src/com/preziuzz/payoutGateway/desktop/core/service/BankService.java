package com.preziuzz.payoutGateway.desktop.core.service;

import com.preziuzz.payoutGateway.desktop.core.DatabaseManager;
import com.preziuzz.payoutGateway.desktop.core.EntityManager;
import com.preziuzz.payoutGateway.desktop.core.entity.Bank;
import com.preziuzz.payoutGateway.desktop.core.model.BankModel;
import com.preziuzz.payoutGateway.desktop.utility.db.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Represents the common value methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class BankService {
    private static EntityManager manager;
    private static Bank entity;

    static {
        if (manager == null) {
            manager = DatabaseManager.buildEntityManager();
        }

        if (entity == null) {
            entity = new Bank();
        }
    }

    /**
     * @throws SQLException
     */
    public static void createTable() throws SQLException {
        entity.createTable();
    }

    /**
     * @param model
     * @return
     */
    public static boolean deleteBank(BankModel model) {
        if (BankAccountService.getBankAccountByCode(model.getCode()) == null) {
            entity.setDataFromModel(model);
            return entity.delete();
        } else {
            return false;
        }
    }

    /**
     * @return
     */
    public static List<BankModel> getAllBanks() {
        return manager.find(Bank.class, BankModel.class, QueryBuilder.select().orderBy("code"));
    }

    /**
     * @param code
     * @return
     */
    public static BankModel getBankByCode(String code) {
        List<BankModel> models = manager.find(Bank.class, BankModel.class, QueryBuilder.select().where("code = '%s'", code));

        if ((models != null) && (models.size() > 0)) {
            return models.get(0);
        }

        return null;
    }

    /**
     * @param keyword
     * @return
     */
    public static List<BankModel> searchBank(String keyword) {
        return manager.find(
            Bank.class,
            BankModel.class,
            QueryBuilder
                .select()
                .where("code like '%%%s%%' or name like '%%%s%%' or alternateName like '%%%s%%'", keyword, keyword, keyword)
                .orderBy("code"));
    }

    /**
     * @param model
     * @return
     */
    public static boolean update(BankModel model) {
        entity.setDataFromModel(model);
        return entity.save();
    }
}
