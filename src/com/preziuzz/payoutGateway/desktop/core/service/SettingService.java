package com.preziuzz.payoutGateway.desktop.core.service;

import com.preziuzz.payoutGateway.desktop.core.DatabaseManager;
import com.preziuzz.payoutGateway.desktop.core.EntityManager;
import com.preziuzz.payoutGateway.desktop.core.entity.Setting;
import com.preziuzz.payoutGateway.desktop.core.model.SettingModel;
import com.preziuzz.payoutGateway.desktop.utility.db.QueryBuilder;

import java.sql.SQLException;

/**
 * Represents the setting service methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class SettingService {
    private static EntityManager manager;
    private static Setting entity;

    static {
        if (manager == null) {
            manager = DatabaseManager.buildEntityManager();
        }

        if (entity == null) {
            entity = new Setting();
        }
    }

    public static void createTable() throws SQLException {
        entity.createTable();
    }

    public static SettingModel getSetting() {
        return manager.find(Setting.class, SettingModel.class, QueryBuilder.select()).get(0);
    }

    public static boolean update(SettingModel model) {
        entity.setDataFromModel(model);
        return entity.save();
    }
}
