package com.preziuzz.payoutGateway.desktop.core.service;

import com.preziuzz.payoutGateway.desktop.core.DatabaseManager;
import com.preziuzz.payoutGateway.desktop.core.EntityManager;
import com.preziuzz.payoutGateway.desktop.core.entity.BankAccount;
import com.preziuzz.payoutGateway.desktop.core.model.BankAccountModel;
import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;
import com.preziuzz.payoutGateway.desktop.utility.db.QueryBuilder;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * Represents the bank service methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class BankAccountService {
    private static EntityManager manager;
    private static BankAccount entity;

    static {
        if (manager == null) {
            manager = DatabaseManager.buildEntityManager();
        }

        if (entity == null) {
            entity = new BankAccount();
        }
    }

    /**
     *
     * @throws SQLException
     */
    public static void createTable() throws SQLException {
        entity.createTable();
    }

    /**
     *
     * @param model
     * @return
     */
    public static boolean deleteBankAccount(BankAccountModel model) {
        if (UserService.getUsersByBankId(model.getId()) == null) {
            entity.setDataFromModel(model);
            return entity.delete();
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public static List<BankAccountModel> getAllBankAccounts() {
        return manager.find(BankAccount.class, BankAccountModel.class, QueryBuilder.select().orderBy("name"));
    }

    /**
     * @param code
     * @return
     */
    public static BankAccountModel getBankAccountByCode(String code) {
        List<BankAccountModel> models = manager.find(BankAccount.class,
                                                     BankAccountModel.class,
                                                     QueryBuilder.select().where("code = '%s'", code).orderBy("id"));

        if ((models != null) && (models.size() > 0)) {
            return models.get(0);
        }

        return null;
    }

    /**
     *
     * @param id
     * @return
     */
    public static BankAccountModel getBankAccountById(String id) {
        List<BankAccountModel> models = manager.find(BankAccount.class,
                                                     BankAccountModel.class,
                                                     QueryBuilder.select().where("id = '%s'", id).orderBy("id"));

        if ((models != null) && (models.size() > 0)) {
            return models.get(0);
        }

        return null;
    }

    /**
     *
     * @param name
     * @return
     */
    public static BankAccountModel getBankAccountByName(String name) {
        List<BankAccountModel> models = manager.find(BankAccount.class,
                                                     BankAccountModel.class,
                                                     QueryBuilder.select().where("name = '%s'", name).orderBy("name"));

        if ((models != null) && (models.size() > 0)) {
            return models.get(0);
        }

        return null;
    }

    /**
     *
     * @param keyword
     * @return
     */
    public static List<BankAccountModel> searchBankAccount(String keyword) {
        return manager.find(BankAccount.class,
                            BankAccountModel.class,
                            QueryBuilder.select().where("name like '%%%s%%' or accountNumber like '%%%s%%'", keyword, keyword).orderBy("name"));
    }

    /**
     *
     * @param model
     * @return
     */
    public static boolean update(BankAccountModel model) {
        if (StringUtility.isNullOrEmpty(model.getId())) {
            model.setId(UUID.randomUUID().toString());
        }

        entity.setDataFromModel(model);

        return entity.save();
    }
}
