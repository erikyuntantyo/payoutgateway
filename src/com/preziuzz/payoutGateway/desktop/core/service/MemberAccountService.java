package com.preziuzz.payoutGateway.desktop.core.service;

import com.preziuzz.payoutGateway.desktop.core.DatabaseManager;
import com.preziuzz.payoutGateway.desktop.core.EntityManager;
import com.preziuzz.payoutGateway.desktop.core.entity.MemberAccount;
import com.preziuzz.payoutGateway.desktop.core.model.MemberAccountModel;
import com.preziuzz.payoutGateway.desktop.utility.common.NumberUtility;
import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;
import com.preziuzz.payoutGateway.desktop.utility.db.QueryBuilder;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Represents the member bank account service methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class MemberAccountService {
    private static EntityManager manager;
    private static MemberAccount entity;

    static {
        if (manager == null) {
            manager = DatabaseManager.buildEntityManager();
        }

        if (entity == null) {
            entity = new MemberAccount();
        }
    }

    /**
     *
     * @throws SQLException
     */
    public static void createTable() throws SQLException {
        entity.createTable();
    }

    /**
     *
     * @param code
     * @param name
     * @param bankName
     * @param accountNumber
     * @param createdDate
     * @return
     */
    public static List<MemberAccountModel> findMemberAccount(String code, String name, String bankName, String accountNumber, Date createdDate) {
        List<MemberAccountModel> models = manager.find(
            MemberAccount.class,
            MemberAccountModel.class,
            QueryBuilder.select().where(
                "code = '%s' and name = '%s' and bankName = '%s' and accountNumber = '%s' and createdDate = %s",
                code, name, bankName, accountNumber, NumberUtility.convertDateToInt(createdDate)));

        if (models != null && models.size() > 0) {
            return models;
        }

        return null;
    }

    /**
     *
     * @return
     */
    public static List<MemberAccountModel> getAllUnpaidMemberAccounts() {
        return manager.find(MemberAccount.class, MemberAccountModel.class, QueryBuilder.select().where("status = 1"));
    }

    /**
     *
     * @param model
     * @return
     */
    public static String update(MemberAccountModel model) {
        if (StringUtility.isNullOrEmpty(model.getId())) {
            model.setId(UUID.randomUUID().toString());
        }

        entity.setDataFromModel(model);

        return entity.save() ? model.getId() : null;
    }
}
