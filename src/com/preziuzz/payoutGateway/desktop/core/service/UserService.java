package com.preziuzz.payoutGateway.desktop.core.service;

import com.preziuzz.payoutGateway.desktop.core.DatabaseManager;
import com.preziuzz.payoutGateway.desktop.core.EntityManager;
import com.preziuzz.payoutGateway.desktop.core.entity.User;
import com.preziuzz.payoutGateway.desktop.core.model.UserModel;
import com.preziuzz.payoutGateway.desktop.utility.db.QueryBuilder;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

/**
 * Represents the user service methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class UserService {
    private static EntityManager manager;
    private static User entity;

    static {
        if (manager == null) {
            manager = DatabaseManager.buildEntityManager();
        }

        if (entity == null) {
            entity = new User();
        }
    }

    /**
     * Creates user table.
     *
     * @throws SQLException If a database access error occurs.
     */
    public static void createTable() throws SQLException {
        entity.createTable();
    }

    /**
     * @param model
     * @return
     */
    public static boolean deleteUser(UserModel model) {
        entity.setDataFromModel(model);
        return entity.delete();
    }

    /**
     * Gets all users.
     *
     * @return List of user.
     * @see List
     */
    public static List<UserModel> getAllUsers() {
        List<UserModel> models = manager.find(User.class, UserModel.class, QueryBuilder.select().orderBy("id"));

        if (models != null) {
            for (UserModel model : models) {
                model.setPassword(null);
            }
        }

        return models;
    }

    /**
     * Gets user by id (include password).
     *
     * @param userId   User id.
     * @return User data model if found in the database, otherwise null.
     */
    public static UserModel getUser(String userId) {
        List<UserModel> models = manager.find(
            User.class,
            UserModel.class,
            QueryBuilder.select().where("id = '%s'", userId));

        if ((models != null) && (models.size() == 1)) {
            return models.get(0);
        }

        return null;
    }

    /**
     * Gets user by id.
     *
     * @param userId User id.
     * @return User data model.
     */
    public static UserModel getUserById(String userId) {
        List<UserModel> models = manager.find(User.class, UserModel.class, QueryBuilder.select().where("id = '%s'", userId));

        if ((models != null) && (models.size() > 0)) {
            models.get(0).setPassword(null);
            return models.get(0);
        }

        return null;
    }

    /**
     * @param bankId
     * @return
     */
    public static List<UserModel> getUsersByBankId(String bankId) {
        List<UserModel> models = manager.find(User.class, UserModel.class, QueryBuilder.select().where("bankId = '%s'", bankId));

        if ((models != null) && (models.size() > 0)) {
            for (UserModel model : models) {
                model.setPassword(null);
            }

            return models;
        }

        return null;
    }

    /**
     * Log in.
     *
     * @return True if succeeded to log in, otherwise false.
     * @see boolean
     */
    public static boolean login(String userId, String password) {
        List<UserModel> models = manager.find(
            User.class,
            UserModel.class,
            QueryBuilder.select().where("id = '%s' and password = '%s'", userId, password));

        if ((models != null) && (models.size() == 1)) {
            models.get(0).setLastLogin(Calendar.getInstance().getTime());
            entity.setDataFromModel(models.get(0));
            return entity.save();
        } else {
            return false;
        }
    }

    /**
     * @param keyword
     * @return
     */
    public static List<UserModel> searchUser(String keyword) {
        List<UserModel> models = manager.find(
            User.class,
            UserModel.class,
            QueryBuilder.select().where("id like '%%%s%%' or name like '%%%s%%'", keyword, keyword));

        if ((models != null) && (models.size() > 0)) {
            for (UserModel model : models) {
                model.setPassword(null);
            }

            return models;
        } else {
            return null;
        }
    }

    /**
     * Updates user table.
     *
     * @param model User model.
     * @return True if succeeded to update data, otherwise false.
     * @see boolean
     */
    public static boolean update(UserModel model) {
        entity.setDataFromModel(model);
        return entity.save();
    }
}
