package com.preziuzz.payoutGateway.desktop.core.model;

/**
 * Represents the bank model.
 *
 * @author Erik P. Yuntantyo
 */
public class BankAccountModel {
    private String id;
    private String name;
    private String code;
    private String accountNumber;
    private String pin;
    private String dialNumber;
    private String encryptedDialNumber;
    private String smsFormat1;
    private String smsFormat2;
    private String smsResponseFormat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getDialNumber() {
        return dialNumber;
    }

    public void setDialNumber(String dialNumber) {
        this.dialNumber = dialNumber;
    }

    public String getEncryptedDialNumber() {
        return encryptedDialNumber;
    }

    public void setEncryptedDialNumber(String encryptedDialNumber) {
        this.encryptedDialNumber = encryptedDialNumber;
    }

    public String getSmsFormat1() {
        return smsFormat1;
    }

    public void setSmsFormat1(String smsFormat1) {
        this.smsFormat1 = smsFormat1;
    }

    public String getSmsFormat2() {
        return smsFormat2;
    }

    public void setSmsFormat2(String smsFormat2) {
        this.smsFormat2 = smsFormat2;
    }

    public String getSmsResponseFormat() {
        return smsResponseFormat;
    }

    public void setSmsResponseFormat(String smsResponseFormat) {
        this.smsResponseFormat = smsResponseFormat;
    }

    @Override
    public String toString() {
        return name;
    }
}
