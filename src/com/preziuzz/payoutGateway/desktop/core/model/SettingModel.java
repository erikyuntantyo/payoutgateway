package com.preziuzz.payoutGateway.desktop.core.model;

/**
 * Represents setting model.
 *
 * @author Erik P. Yuntantyo
 */
public class SettingModel {
    private String language;
    private String deviceModel;
    private String port;
    private boolean allowTransBank;
    private int logInTimeOut;
    private boolean autoLogOff;
    private boolean showExitConfirmation;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public boolean isAllowTransBank() {
        return allowTransBank;
    }

    public void setAllowTransBank(boolean allowTransBank) {
        this.allowTransBank = allowTransBank;
    }

    public int getLogInTimeOut() {
        return logInTimeOut;
    }

    public void setLogInTimeOut(int logInTimeOut) {
        this.logInTimeOut = logInTimeOut;
    }

    public boolean isAutoLogOff() {
        return autoLogOff;
    }

    public void setAutoLogOff(boolean autoLogOff) {
        this.autoLogOff = autoLogOff;
    }

    public boolean isShowExitConfirmation() {
        return showExitConfirmation;
    }

    public void setShowExitConfirmation(boolean showExitConfirmation) {
        this.showExitConfirmation = showExitConfirmation;
    }
}
