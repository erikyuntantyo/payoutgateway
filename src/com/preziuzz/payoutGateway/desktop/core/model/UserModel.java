package com.preziuzz.payoutGateway.desktop.core.model;

import com.preziuzz.payoutGateway.desktop.utility.common.NumberUtility;

import java.util.Date;

/**
 * Represents the user model.
 *
 * @author Erik P. Yuntantyo
 */
public class UserModel {
    /**
     * <code>UserType</code> enum specifies user types and their associated scopes.
     */
    public enum UserType {
        /**
         * An <code>ADMINISTRATION</code> user is allowed to access all features.
         */
        ADMINISTRATOR,
        /**
         * An <code>OPERATOR</code> user can access specified features for operator user only.
         */
        OPERATOR
    }

    private String id;
    private String bankId;
    private String name;
    private String password;
    private int type;
    private boolean active;
    private int createdDate;
    private int lastLogin;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return UserType.values()[type];
    }

    public void setType(UserType type) {
        this.type = type.ordinal();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getCreatedDate() {
        return NumberUtility.convertIntToDate(createdDate);
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = NumberUtility.convertDateToInt(createdDate);
    }

    public Date getLastLogin() {
        return NumberUtility.convertIntToDate(lastLogin);
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = NumberUtility.convertDateToInt(lastLogin);
    }
}
