package com.preziuzz.payoutGateway.desktop.core.model;

import com.preziuzz.payoutGateway.desktop.utility.common.NumberUtility;

import java.util.Date;

/**
 * Represents the member bank account model.
 *
 * @author Erik P. Yuntantyo
 */
public class MemberAccountModel {
    private String id;
    private String code;
    private String name;
    private String bankCode;
    private String bankName;
    private String accountNumber;
    private double totalReceived;
    private int createdDate;
    private int capturedDate;
    private int sentDate;
    private int status;
    private String comments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getTotalReceived() {
        return totalReceived;
    }

    public void setTotalReceived(double totalReceived) {
        this.totalReceived = totalReceived;
    }

    public Date getCreatedDate() {
        return NumberUtility.convertIntToDate(createdDate);
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = NumberUtility.convertDateToInt(createdDate);
    }

    public Date getCapturedDate() {
        return NumberUtility.convertIntToDate(capturedDate);
    }

    public void setCapturedDate(Date capturedDate) {
        this.capturedDate = NumberUtility.convertDateToInt(capturedDate);
    }

    public Date getSentDate() {
        return NumberUtility.convertIntToDate(sentDate);
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = NumberUtility.convertDateToInt(sentDate);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
