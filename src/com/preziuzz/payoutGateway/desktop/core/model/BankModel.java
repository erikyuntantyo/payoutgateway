package com.preziuzz.payoutGateway.desktop.core.model;

/**
 * Represents the bank model.
 *
 * @author Erik P. Yuntantyo
 */
public class BankModel {
    private String code;
    private String name;
    private String alternateName;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlternateName() {
        return alternateName;
    }

    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }
}
