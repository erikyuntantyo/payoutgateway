package com.preziuzz.payoutGateway.desktop.core;

import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import com.preziuzz.payoutGateway.desktop.utility.db.QueryBuilder;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

/**
 * Represents the entity manager methods.
 *
 * @author Erik P. Yuntantyo
 */
public final class EntityManager {
    private static EntityManager instance = null;

    private Connection connection;

    /**
     * Get instance of <code>EntityManager</code>.
     *
     * @return Instance of <code>EntityManager</code>.
     */
    public static EntityManager getInstance() {
        if (instance == null) {
            instance = new EntityManager();
        }

        return instance;
    }

    /**
     * @param connection
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * @param tableName
     * @param type
     * @param query
     * @param <T>
     * @return
     */
    public <T> List<T> findSingleColumn(String tableName, Class<T> type, QueryBuilder query) {
        try {
            List<T> rows = new Vector<>();

            ResultSet resultSet;
            T row;
            Statement statement;

            if (query.hasQuery()) {
                String sql = String.format(query.getQuery(), tableName);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                while (resultSet.next()) {
                    row = type.newInstance();

                    if (type == Boolean.TYPE) {
                        row = type.cast(resultSet.getBoolean(query.getSelectedFields()[0]));
                    } else if (type == Double.TYPE) {
                        row = type.cast(resultSet.getDouble(query.getSelectedFields()[0]));
                    } else if (type == Integer.TYPE) {
                        row = type.cast(resultSet.getInt(query.getSelectedFields()[0]));
                    } else if (type == String.class) {
                        row = type.cast(resultSet.getString(query.getSelectedFields()[0]));
                    }

                    rows.add(row);
                }

                resultSet.close();
            }

            return rows;
        } catch (RuntimeException exc) {
            MessageDialog.showError(exc.toString(), "EntityManager.find.RuntimeException");
        } catch (Exception exc) {
            MessageDialog.showError(exc.toString(), "EntityManager.find.Exception");
        }

        return null;
    }

    /**
     * @param type
     * @param query
     * @param <T>
     * @return
     */
    public <T extends EntityFactory> List<T> find(Class<T> type, QueryBuilder query) {
        try {
            List<T> rows = new Vector<>();

            ResultSet resultSet;
            T row;
            Statement statement;

            if (query.hasQuery()) {
                String sql = String.format(query.getQuery(), type.getSimpleName());
                Field f;

                statement = connection.createStatement();
                resultSet = statement.executeQuery(sql);

                while (resultSet.next()) {
                    row = type.newInstance();

                    for (Field field : type.getDeclaredFields()) {
                        f = row.getClass().getDeclaredField(field.getName());

                        if (f.getType() == Boolean.TYPE) {
                            f.set(row, resultSet.getBoolean(field.getName()));
                        } else if (f.getType() == Double.TYPE) {
                            f.set(row, resultSet.getDouble(field.getName()));
                        } else if (f.getType() == Integer.TYPE) {
                            f.set(row, resultSet.getInt(field.getName()));
                        } else if (f.getType() == String.class) {
                            f.set(row, resultSet.getString(field.getName()));
                        }
                    }

                    rows.add(row);
                }

                resultSet.close();
            }

            return rows;
        } catch (RuntimeException exc) {
            MessageDialog.showError(exc.toString(), "EntityManager.find.RuntimeException");
        } catch (Exception exc) {
            MessageDialog.showError(exc.toString(), "EntityManager.find.Exception");
        }

        return null;
    }

    /**
     * @param source
     * @param model
     * @param query
     * @param <T>
     * @param <E>
     * @return
     */
    public <T extends EntityFactory, E> List<E> find(Class<T> source, Class<E> model, QueryBuilder query) {
        List<E> results = new Vector<>();

        if (query.hasQuery()) {
            List<T> rows = find(source, query);

            if ((rows != null) && (rows.size() > 0)) {
                for (T row : rows) {
                    results.add(row.<E>setDataToModel(model));
                }
            } else {
                return null;
            }
        }

        return results;
    }
}
