/*
 * Copyright (c) 2013 preziuzz.
 * This product is licensed to Glutera™ Glutathione.
 */

package com.preziuzz.payoutGateway.desktop;

import com.preziuzz.SimpleGateway.GatewayManager;
import com.preziuzz.SimpleGateway.core.GatewayServiceTypes;
import com.preziuzz.os.Validator;
import com.preziuzz.payoutGateway.desktop.forms.UserAuthenticationDialog;
import com.preziuzz.payoutGateway.desktop.utility.common.Logger;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import com.preziuzz.payoutGateway.desktop.utility.common.SessionManager;
import com.preziuzz.simpleLic.License;

import javax.swing.*;
import java.awt.*;

/**
 * Represents the main application module.
 *
 * @author Erik P. Yuntantyo
 */
public class Main {
    /**
     * Deinitializes components.
     */
    public static void deinitialize() {
        GatewayManager.dispose();
        GatewayServiceTypes.dispose();
        SessionManager.dispose();
        System.exit(0);
    }

    /**
     * The beginning part of the application.
     *
     * @param args Given arguments.
     */
    public static void main(String[] args) {
        System.out.println("IN THE NAME OF ALLAH, JESUS CHRIST AND HOLLY SPIRIT\n" +
                           "I wish I can run with less error.\n" +
                           "And may the GOD blessed who codes me and anyone who used me.\n" +
                           "Amen...");

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    initializeUI();

                    Logger.initialize();
                    License.initialize();
                    GatewayServiceTypes.configure();
                    SessionManager.initialize();
                    UserAuthenticationDialog.showDialog();
                } catch (Exception exc) {
                    Logger.log(exc,
                               "Main.main.Exception",
                               SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                               Logger.LogType.ERROR);
                    MessageDialog.showError(exc);

                    deinitialize();
                }
            }
        });
    }

    private static void initializeUI() throws UnsupportedLookAndFeelException {
        try {
            if (Validator.isWindows()) {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } else {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            }
        } catch (Exception e) {
            throw new UnsupportedLookAndFeelException("Unable to initialize look and feel.");
        }

        UIManager.getDefaults().put("FileChooser.cancelButtonText", "Batal");
        UIManager.getDefaults().put("FileChooser.cancelButtonToolTipText", "Batal");
        UIManager.getDefaults().put("FileChooser.fileNameLabelText", "Nama Berkas");
        UIManager.getDefaults().put("FileChooser.filesOfTypeLabelText", "Tipe Berkas");
        UIManager.getDefaults().put("FileChooser.lookInLabelText", "Lihat di");
        UIManager.getDefaults().put("FileChooser.openButtonToolTipText", "Buka Berkas");
        UIManager.getDefaults().put("TabbedPane.contentBorderInsets", new Insets(-2, 0, 0, 0));
        UIManager.getDefaults().put("TabbedPane.tabsOverlapBorder", true);
    }
}
