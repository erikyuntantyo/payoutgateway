package com.preziuzz.payoutGateway.desktop.forms;

import com.preziuzz.SimpleGateway.GatewayManager;
import com.preziuzz.SimpleGateway.common.ConnectEvent;
import com.preziuzz.SimpleGateway.common.GatewayAdapter;
import com.preziuzz.payoutGateway.desktop.Main;
import com.preziuzz.payoutGateway.desktop.forms.panels.main.ContentPane;
import com.preziuzz.payoutGateway.desktop.forms.panels.main.RightPane;
import com.preziuzz.payoutGateway.desktop.forms.panels.main.TopPane;
import com.preziuzz.payoutGateway.desktop.utility.common.*;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import com.preziuzz.simpleLic.License;
import com.preziuzz.simpleLic.LicenseType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Represents the main form of the program.
 *
 * @author Erik P. Yuntantyo
 */
public class MainForm extends JFrame {
    private ContentPane contentPane;
    private RightPane rightPane;
    private TopPane topPane;

    private MainForm(String title) {
        super(title);
        initializeComponents();
    }

    /**
     * Show main form.
     */
    public static void showForm() {
        new MainForm(String.format("%s %s", Constants.PRODUCT_NAME, Constants.PRODUCT_VERSION)).setVisible(true);
    }

    private void initializeComponents() {
        Dimension minSize = new Dimension(1024, 600);
        String title = getTitle();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (MessageDialog.showConfirmation("Apakah anda ingin keluar program?")) {
                    Main.deinitialize();
                }
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setExtendedState(MAXIMIZED_BOTH);
        setIconImage(ImageUtility.ICON_MAIN.getImage());
        setLocationByPlatform(false);
        setMinimumSize(minSize);

        if (!License.getLicenseType().equals(LicenseType.FULL) && !License.getLicenseType().equals(LicenseType.RESOLVED)) {
            title = String.format("%s - %s", title, License.getLicenseType());
        }

        setTitle(title);

        contentPane = new ContentPane();

        rightPane = new RightPane();
        rightPane.addMessageListener(new MessageEventListener() {
            @Override
            public void published(MessageEvent e) {
                response(e);
            }
        });

        topPane = new TopPane();
        topPane.addMessageListener(new MessageEventListener() {
            @Override
            public void published(MessageEvent e) {
                response(e);
            }
        });

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(contentPane, BorderLayout.CENTER);
        getContentPane().add(rightPane, BorderLayout.EAST);
        getContentPane().add(topPane, BorderLayout.NORTH);

        if (!StringUtility.isNullOrEmpty(SessionManager.getCurrentSettings().getDeviceModel()) &&
            !StringUtility.isNullOrEmpty(SessionManager.getCurrentSettings().getPort())) {
            try {
                GatewayManager.createService(SessionManager.getCurrentSettings().getDeviceModel(), SessionManager.getCurrentSettings().getPort());
                GatewayManager.getService()
                    .addGatewayListener(new GatewayAdapter() {
                        @Override
                        public void connected(ConnectEvent e) {
                            contentPane.subscribe(MainForm.this, "DEVICE_CONNECTED", e.getOperatorName());
                            topPane.subscribe(MainForm.this, "DEVICE_CONNECTED", e.getOperatorName());
                        }

                        @Override
                        public void disconnected() {
                            contentPane.subscribe(MainForm.this, "DEVICE_DISCONNECTED");
                            topPane.subscribe(MainForm.this, "DEVICE_DISCONNECTED");
                        }
                    });

                contentPane.subscribe(this, "READY");
                rightPane.subscribe(this, "READY");
                topPane.subscribe(this, "READY");
            } catch (IllegalAccessException ignore) {
            }
        }
    }

    private void response(MessageEvent e) {
        switch (e.getMessage()) {
            case "REFRESH":
                rightPane.subscribe(e.getSource(), e.getMessage());
                break;
            case "SELECT_MEMBER":
                contentPane.subscribe(e.getSource(), e.getMessage(), e.getData());
                break;
            case "SIGN_OUT":
                if (MessageDialog.showConfirmation("Apakah anda ingin mengunci program?")) {
                    dispose();

                    GatewayManager.dispose();
                    UserAuthenticationDialog.showDialog();
                }

                break;
        }
    }
}
