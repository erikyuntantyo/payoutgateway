package com.preziuzz.payoutGateway.desktop.forms;

import com.preziuzz.payoutGateway.desktop.core.model.SettingModel;
import com.preziuzz.payoutGateway.desktop.core.model.UserModel;
import com.preziuzz.payoutGateway.desktop.core.service.SettingService;
import com.preziuzz.payoutGateway.desktop.core.service.UserService;
import com.preziuzz.payoutGateway.desktop.forms.panels.setting.*;
import com.preziuzz.payoutGateway.desktop.utility.common.*;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JButtonExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Represents the setting dialog.
 *
 * @author Erik P. Yuntantyo
 */
public class SettingDialog extends JDialog {
    private JButtonExt btnApply;
    private JButtonExt btnCancel;
    private JLabel lblTabTitle;
    private AboutPane pnlAbout;
    private BankPane pnlBank;
    private BankAccountPane pnlBankAccount;
    private GeneralSettingPane pnlGeneralSetting;
    private InboxPane pnlInbox;
    private LicensePane pnlLicense;
    private UserAccountPane pnlUserAccount;
    private USSDPane pnlUSSD;
    private JTabbedPane tabMain;

    private boolean dialogResult;
    private String tmpBankAccountId;
    private boolean tmpIsAutoLock;
    private boolean tmpIsConfirmExit;
    private String tmpPort;
    private String tmpDeviceModel;
    private boolean updateSetting;
    private boolean updateUser;

    private SettingDialog() {
        initializeComponents();
    }

    /**
     * @return
     */
    public static boolean showDialog() {
        SettingDialog instance = new SettingDialog();
        instance.setVisible(true);
        return instance.dialogResult;
    }

    private void initializeComponents() {
        int height = 600;
        int width = 500;
        int x = ((int)Constants.SCREEN_SIZE.getWidth() - width) / 2;
        int y = ((int)Constants.SCREEN_SIZE.getHeight() - height) / 2;

        dialogResult = false;

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close();
            }
        });
        setBounds(x, y, width, height);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocationByPlatform(false);
        setLocationRelativeTo(null);
        setModal(true);
        setResizable(false);
        setTitle("Pengaturan");

        pnlUserAccount = new UserAccountPane();

        pnlBankAccount = new BankAccountPane();

        pnlGeneralSetting = new GeneralSettingPane();
        pnlGeneralSetting.addMessageListener(new MessageEventListener() {
            @Override
            public void published(MessageEvent e) {
                response(e);
            }
        });

        pnlBank = new BankPane();

        pnlInbox = new InboxPane();

        pnlUSSD = new USSDPane();

        pnlLicense = new LicensePane();

        pnlAbout = new AboutPane();

        tabMain = new JTabbedPane();
        tabMain.addTab(null, pnlUserAccount);
        tabMain.addTab(null, pnlBankAccount);
        tabMain.addTab(null, pnlGeneralSetting);
        tabMain.addTab(null, pnlBank);
        tabMain.addTab(null, pnlInbox);
        tabMain.addTab(null, pnlUSSD);
        tabMain.addTab(null, pnlLicense);
        tabMain.addTab(null, pnlAbout);
        tabMain.setBounds(10, 10, getWidth() - 25, getHeight() - 100);

        lblTabTitle = new JLabel("Akun Pengguna");
        lblTabTitle.setForeground(SystemColor.textInactiveText);
        lblTabTitle.setPreferredSize(new Dimension(75, 20));

        tabMain.setTabComponentAt(0, lblTabTitle);

        lblTabTitle = new JLabel("Akun Bank");
        lblTabTitle.setForeground(SystemColor.textInactiveText);
        lblTabTitle.setPreferredSize(new Dimension(50, 20));

        tabMain.setTabComponentAt(1, lblTabTitle);

        lblTabTitle = new JLabel("Pengaturan Umum");
        lblTabTitle.setForeground(SystemColor.textInactiveText);
        lblTabTitle.setPreferredSize(new Dimension(90, 20));

        tabMain.setTabComponentAt(2, lblTabTitle);

        lblTabTitle = new JLabel("Daftar Bank");
        lblTabTitle.setForeground(SystemColor.textInactiveText);
        lblTabTitle.setPreferredSize(new Dimension(60, 20));

        tabMain.setTabComponentAt(3, lblTabTitle);

        lblTabTitle = new JLabel("Kotak Masuk");
        lblTabTitle.setForeground(SystemColor.textInactiveText);
        lblTabTitle.setPreferredSize(new Dimension(60, 20));

        tabMain.setTabComponentAt(4, lblTabTitle);

        lblTabTitle = new JLabel("USSD");
        lblTabTitle.setForeground(SystemColor.textInactiveText);
        lblTabTitle.setPreferredSize(new Dimension(35, 20));

        tabMain.setTabComponentAt(5, lblTabTitle);

        lblTabTitle = new JLabel("Lisensi");
        lblTabTitle.setForeground(SystemColor.textInactiveText);
        lblTabTitle.setPreferredSize(new Dimension(35, 20));

        tabMain.setTabComponentAt(6, lblTabTitle);

        lblTabTitle = new JLabel("Perihal");
        lblTabTitle.setForeground(SystemColor.textInactiveText);
        lblTabTitle.setPreferredSize(new Dimension(35, 20));

        tabMain.setTabComponentAt(7, lblTabTitle);

        if (!SessionManager.getCurrentUser().getType().equals(UserModel.UserType.ADMINISTRATOR)) {
            tabMain.setEnabledAt(0, false);
            tabMain.setEnabledAt(1, false);
            tabMain.setEnabledAt(4, false);
            tabMain.setSelectedIndex(2);
            tabMain.getTabComponentAt(0).setEnabled(false);
            tabMain.getTabComponentAt(1).setEnabled(false);
            tabMain.getTabComponentAt(4).setEnabled(false);
        }

        btnCancel = new JButtonExt("Batal");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });
        btnCancel.setBounds(getWidth() - 85, getHeight() - 70, 70, 30);

        btnApply = new JButtonExt("Terapkan");
        btnApply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (updateSetting) {
                    SettingModel model = SessionManager.getCurrentSettings();

                    model.setDeviceModel(tmpDeviceModel);
                    model.setPort(tmpPort);

                    SettingService.update(model);
                }

                if (updateUser) {
                    UserModel model = UserService.getUser(SessionManager.getCurrentUser().getId());
                    model.setBankId(tmpBankAccountId);
                    UserService.update(model);
                }

                btnApply.setEnabled(false);
                SessionManager.refreshSessionData();

                dialogResult = true;
                tmpBankAccountId = null;
                tmpDeviceModel = null;
                tmpPort = null;
            }
        });
        btnApply.setBounds(getWidth() - 160, getHeight() - 70, 70, 30);
        btnApply.setEnabled(false);
        btnApply.setFont(getContentPane().getFont().deriveFont(Font.BOLD));

        getContentPane().setBackground(SystemColor.control.darker());
        getContentPane().setLayout(null);
        getContentPane().add(tabMain);
        getContentPane().add(btnApply);
        getContentPane().add(btnCancel);

        getRootPane().setDefaultButton(btnApply);
    }

    private void close() {
        if (btnApply.isEnabled()) {
            if (MessageDialog.showConfirmation("Apakah ingin membatalkan perubahan data?")) {
                dispose();
            }
        } else {
            dispose();
        }
    }

    private void response(MessageEvent e) {
        if (e.getData() != null) {
            switch (e.getMessage()) {
                case "UPDATE_BANK_ID":
                    tmpBankAccountId = e.getData().toString();
                    break;
                case "UPDATE_DEVICE_MODEL":
                    tmpDeviceModel = e.getData().toString();
                    break;
                case "UPDATE_DEVICE_PORT":
                    tmpPort = e.getData().toString();
                    break;
            }

            updateSetting = (!StringUtility.isNullOrEmpty(tmpDeviceModel) && !tmpDeviceModel.equals(SessionManager.getCurrentSettings().getDeviceModel())) ||
                (!StringUtility.isNullOrEmpty(tmpPort) && !tmpPort.equals(SessionManager.getCurrentSettings().getPort()));
            updateUser = !StringUtility.isNullOrEmpty(tmpBankAccountId) && !tmpBankAccountId.equals(SessionManager.getCurrentUser().getBankId());

            btnApply.setEnabled(updateSetting || updateUser);
        }
    }
}
