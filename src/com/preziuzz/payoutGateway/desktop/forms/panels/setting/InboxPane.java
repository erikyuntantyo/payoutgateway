package com.preziuzz.payoutGateway.desktop.forms.panels.setting;

import com.preziuzz.SimpleGateway.GatewayManager;
import com.preziuzz.SimpleGateway.common.GatewayAdapter;
import com.preziuzz.SimpleGateway.common.GatewayListener;
import com.preziuzz.SimpleGateway.common.IncomingMessageEvent;
import com.preziuzz.payoutGateway.desktop.forms.panels.setting.cells.InboxCell;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.common.SMSData;
import com.preziuzz.payoutGateway.desktop.utility.extgui.CustomTableManager;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JButtonExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JPanelExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Represents the inbox panel.
 *
 * @author Erik P. Yuntantyo
 */
public class InboxPane extends JPanelExt {
    private JButtonExt btnDelete;
    private CustomTableManager tblInbox;

    private GatewayListener gatewayListener;

    @Override
    protected void preInitialize() {
        gatewayListener = new GatewayAdapter() {
            @Override
            public void inboxMessageRead(IncomingMessageEvent e) {
                smsLoaded(e);
            }
        };

        setBackground(SystemColor.window);
        setLayout(null);

        try {
            GatewayManager.getService().addGatewayListener(gatewayListener);
        } catch (IllegalAccessException ignore) {
        }

        btnDelete = new JButtonExt("Hapus SMS", ImageUtility.ICON_DELETE);
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteData();
            }
        });
        btnDelete.setEnabled(false);

        tblInbox = CustomTableManager.initializeTable("Isi SMS", SwingConstants.LEFT, new InboxCell());
        tblInbox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        add(btnDelete);
        add(tblInbox.getComponent());

        try {
            GatewayManager.getService().readAllMessages();
        } catch (IllegalAccessException ignore) {
        }
    }

    @Override
    protected void releaseResources() {
        try {
            GatewayManager.getService().removeGatewayListener(gatewayListener);
        } catch (IllegalAccessException ignore) {
        }
    }

    @Override
    protected void resized() {
        btnDelete.setBounds(10, 20, 100, 30);
        tblInbox.setBounds(10, 55, getWidth() - 20, getHeight() - btnDelete.getY() - btnDelete.getHeight() - 15);
    }

    private void deleteData() {
        if (MessageDialog.showConfirmation("Apakah ada akan menghapus SMS?", "Hapus SMS")) {

        }
    }

    private void smsLoaded(IncomingMessageEvent e) {
        tblInbox.addDataRow(0, SMSData.initialize(e.getIndex(), e.getSender(), e.getContents(), e.getSentDate(), new Date(), true));
    }
}
