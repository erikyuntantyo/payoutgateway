package com.preziuzz.payoutGateway.desktop.forms.panels.setting;

import com.preziuzz.payoutGateway.desktop.core.model.UserModel;
import com.preziuzz.payoutGateway.desktop.core.service.UserService;
import com.preziuzz.payoutGateway.desktop.utility.common.*;
import com.preziuzz.payoutGateway.desktop.utility.extgui.*;
import com.preziuzz.simpleCrypto.Graphy;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;

/**
 * Represents the user account panel.
 *
 * @author Erik P. Yuntantyo
 */
public class UserAccountPane extends JPanelExt {
    private JButtonExt btnAdd;
    private JButtonExt btnCancel;
    private JButtonExt btnChangePassword;
    private JButtonExt btnDelete;
    private JButtonExt btnEdit;
    private JButtonExt btnSave;
    private JButtonExt btnSearch;
    private JComboBox cboType;
    private JCheckBox chkActive;
    private JLabel lblActive;
    private JLabel lblId;
    private JLabel lblName;
    private JLabel lblPassword;
    private JLabel lblPasswordConfirm;
    private JLabel lblType;
    private JSeparator sprHorizontal;
    private JSeparator sprVertical1;
    private JSeparator sprVertical2;
    private JSeparator sprVertical3;
    private CustomTableManager tblUserAccount;
    private JTextField txtId;
    private JTextField txtName;
    private JPasswordField txtPassword;
    private JPasswordField txtPasswordConfirm;
    private JTextFieldExt txtSearch;

    private List<UserModel> dataModel;
    private EditorState editorState;

    @Override
    protected void preInitialize() {
        editorState = EditorState.NONE;

        setBackground(SystemColor.window);
        setLayout(null);

        lblId = new JLabel("Id");
        lblId.setFont(getFont().deriveFont(Font.BOLD));
        lblId.setForeground(SystemColor.textInactiveText.darker());
        lblId.setHorizontalAlignment(SwingConstants.RIGHT);

        txtId = new JTextField();
        txtId.setFont(getFont().deriveFont(12f));

        lblName = new JLabel("Nama");
        lblName.setFont(getFont().deriveFont(Font.BOLD));
        lblName.setForeground(SystemColor.textInactiveText.darker());
        lblName.setHorizontalAlignment(SwingConstants.RIGHT);

        txtName = new JTextField();
        txtName.setFont(getFont().deriveFont(12f));

        lblPassword = new JLabel("Kata Sandi");
        lblPassword.setFont(getFont().deriveFont(Font.BOLD));
        lblPassword.setForeground(SystemColor.textInactiveText.darker());
        lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);

        txtPassword = new JPasswordField();
        txtPassword.setFont(getFont().deriveFont(12f));

        lblPasswordConfirm = new JLabel("Konfirmasi Sandi");
        lblPasswordConfirm.setFont(getFont().deriveFont(Font.BOLD));
        lblPasswordConfirm.setForeground(SystemColor.textInactiveText.darker());
        lblPasswordConfirm.setHorizontalAlignment(SwingConstants.RIGHT);

        txtPasswordConfirm = new JPasswordField();
        txtPasswordConfirm.setFont(getFont().deriveFont(12f));

        lblType = new JLabel("Tipe");
        lblType.setFont(getFont().deriveFont(Font.BOLD));
        lblType.setForeground(SystemColor.textInactiveText.darker());
        lblType.setHorizontalAlignment(SwingConstants.RIGHT);

        cboType = new JComboBox(UserModel.UserType.values());
        cboType.setFont(getFont().deriveFont(12f));

        lblActive = new JLabel("Aktif");
        lblActive.setFont(getFont().deriveFont(Font.BOLD));
        lblActive.setForeground(SystemColor.textInactiveText.darker());
        lblActive.setHorizontalAlignment(SwingConstants.RIGHT);

        chkActive = new JCheckBox();
        chkActive.setOpaque(false);

        sprHorizontal = new JSeparator(SwingConstants.HORIZONTAL);

        btnAdd = new JButtonExt(ImageUtility.ICON_ADD);
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addData();
            }
        });
        btnAdd.setToolTipText("Tambah Akun Pengguna");

        btnEdit = new JButtonExt(ImageUtility.ICON_EDIT);
        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editData();
            }
        });
        btnEdit.setToolTipText("Ubah Akun Pengguna");

        btnDelete = new JButtonExt(ImageUtility.ICON_DELETE);
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteData();
            }
        });
        btnDelete.setToolTipText("Hapus Akun Pengguna");

        sprVertical1 = new JSeparator(SwingConstants.VERTICAL);

        btnSave = new JButtonExt(ImageUtility.ICON_SAVE);
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });
        btnSave.setToolTipText("Simpan Akun Pengguna");

        btnCancel = new JButtonExt(ImageUtility.ICON_CANCEL);
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelEdit();
            }
        });
        btnCancel.setToolTipText("Batal Tambah/Ubah Data");

        sprVertical2 = new JSeparator(SwingConstants.VERTICAL);

        btnChangePassword = new JButtonExt(ImageUtility.ICON_EDIT_PASSWORD);
        btnChangePassword.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changePassword();
            }
        });
        btnChangePassword.setToolTipText("Ubah Kata Sandi");

        sprVertical3 = new JSeparator(SwingConstants.VERTICAL);

        txtSearch = new JTextFieldExt("Pencarian");
        txtSearch.setPlaceHolderFont(getFont().deriveFont(Font.BOLD));

        btnSearch = new JButtonExt(ImageUtility.ICON_SEARCH);
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchData();
                updateState();
            }
        });
        btnSearch.setToolTipText("Cari Data");

        dataModel = UserService.getAllUsers();

        tblUserAccount = CustomTableManager.initializeTable(
            CustomTableColumnManager.initialize("Id", "id", 70),
            CustomTableColumnManager.initialize("Nama", "name", 100),
            CustomTableColumnManager.initialize("Tipe", "type", 100),
            CustomTableColumnManager.initialize("Aktif", "active", 70, new HashMap<Integer, ImageIcon>() {{
                put(0, ImageUtility.ICON_CROSS);
                put(1, ImageUtility.ICON_TICK);
            }}),
            CustomTableColumnManager.initialize("Terakhir Masuk", "lastLogin", SwingConstants.CENTER, new CustomTableCellFormatListener() {
                @Override
                public String format(Object value) {
                    return Constants.DATE_FORMAT_SHORT.format(value);
                }
            }));
        tblUserAccount.addRowSelectionChangedListener(new CustomTableRowSelectionListener() {
            @Override
            public void rowSelectionChanged(int row) {
                selectData(row);
            }
        });
        tblUserAccount.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblUserAccount.addData(dataModel);

        add(lblId);
        add(txtId);
        add(lblName);
        add(txtName);
        add(lblPassword);
        add(txtPassword);
        add(lblPasswordConfirm);
        add(txtPasswordConfirm);
        add(lblType);
        add(cboType);
        add(lblActive);
        add(chkActive);
        add(sprHorizontal);
        add(btnAdd);
        add(btnEdit);
        add(btnDelete);
        add(sprVertical1);
        add(btnSave);
        add(btnCancel);
        add(sprVertical2);
        add(btnChangePassword);
        add(sprVertical3);
        add(txtSearch);
        add(btnSearch);
        add(tblUserAccount.getComponent());

        selectData(0);
        updateState();
    }

    @Override
    protected void resized() {
        lblId.setBounds(20, 20, 100, 25);
        txtId.setBounds(130, 20, 100, 25);
        lblName.setBounds(20, 50, 100, 25);
        txtName.setBounds(130, 50, 200, 25);
        lblPassword.setBounds(20, 80, 100, 25);
        txtPassword.setBounds(130, 80, 150, 25);
        lblPasswordConfirm.setBounds(20, 110, 100, 25);
        txtPasswordConfirm.setBounds(130, 110, 150, 25);
        lblType.setBounds(20, 140, 100, 25);
        cboType.setBounds(130, 140, 150, 25);
        lblActive.setBounds(20, 170, 100, 25);
        chkActive.setBounds(130, 170, 25, 25);
        sprHorizontal.setBounds(10, 200, getWidth() - 20, 5);
        btnAdd.setBounds(10, 205, 30, 30);
        btnEdit.setBounds(45, 205, 30, 30);
        btnDelete.setBounds(80, 205, 30, 30);
        sprVertical1.setBounds(115, 205, 5, 30);
        btnSave.setBounds(120, 205, 30, 30);
        btnCancel.setBounds(155, 205, 30, 30);
        sprVertical2.setBounds(190, 205, 5, 30);
        btnChangePassword.setBounds(195, 205, 30, 30);
        sprVertical3.setBounds(230, 205, 5, 30);
        txtSearch.setBounds(
            sprVertical3.getX() + sprVertical3.getWidth(),
            205,
            getWidth() - sprVertical3.getX() - sprVertical3.getWidth() - 40,
            30);
        btnSearch.setBounds(txtSearch.getX() + txtSearch.getWidth(), 205, 30, 30);
        tblUserAccount.setBounds(10, 240, getWidth() - 20, getHeight() - sprHorizontal.getY() - sprHorizontal.getHeight() - 45);
    }

    private void addData() {
        editorState = EditorState.ADD_NEW;

        updateState();

        txtId.setText("");
        txtName.setText("");
        txtPassword.setText("");
        txtPasswordConfirm.setText("");
        cboType.setSelectedIndex(-1);
        chkActive.setSelected(true);
        txtId.requestFocus();
    }

    private void cancelEdit() {
        editorState = EditorState.NONE;

        txtSearch.setText("");
        searchData();
        selectData(0);
        updateState();
    }

    private void changePassword() {
        editorState = EditorState.CHANGE_PASSWORD;

        updateState();
        txtPassword.requestFocus();
    }

    private void deleteData() {
        if (SessionManager.getCurrentUser().getId().equals(txtId.getText())) {
            MessageDialog.showWarning("Data pengguna sedang dipakai.\nMasuk sebagai pengguna lain untuk menghapus.");
            return;
        }

        if (MessageDialog.showConfirmation("Apakah ada akan menghapus data pengguna?", "Hapus Data Pengguna")) {
            txtSearch.setText("");
            UserService.deleteUser(dataModel.get(tblUserAccount.getSelectedRow()));
            searchData();
            selectData(0);
            updateState();
        }
    }

    private void editData() {
        editorState = EditorState.EDIT;
        updateState();
    }

    private void saveData() {
        if (!new String(txtPassword.getPassword()).equals(new String(txtPasswordConfirm.getPassword()))) {
            MessageDialog.showWarning("Kata sandi tidak sama, coba lagi.");
            return;
        }

        if ((!StringUtility.isNullOrEmpty(txtId.getText()) || editorState.equals(EditorState.CHANGE_PASSWORD)) &&
            (!StringUtility.isNullOrEmpty(new String(txtPassword.getPassword())) || editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT))) {
            if ((UserService.getUserById(txtId.getText()) == null) || !editorState.equals(EditorState.ADD_NEW)) {
                txtSearch.setText("");

                UserModel model = new UserModel();

                model.setActive(chkActive.isSelected());
                model.setId(txtId.getText());
                model.setType((UserModel.UserType)cboType.getSelectedItem());

                if (!editorState.equals(EditorState.CHANGE_PASSWORD)) {
                    model.setName(txtName.getText());
                }

                if (!editorState.equals(EditorState.EDIT)) {
                    try {
                        model.setPassword(Graphy.encode(new String(txtPassword.getPassword())));
                    } catch (Exception ignore) {
                    }
                }

                if (UserService.update(model)) {
                    editorState = EditorState.NONE;

                    searchData();
                    selectData(0);
                    updateState();
                }
            } else {
                MessageDialog.showWarning("Id pengguna sudah ada.");
            }
        } else {
            MessageDialog.showWarning("Id dan Kata Sandi tidak boleh kosong.");
        }
    }

    private void searchData() {
        dataModel = UserService.searchUser(txtSearch.getText());
        tblUserAccount.addData(dataModel);
    }

    private void selectData(int row) {
        if ((dataModel != null) && (dataModel.size() > 0)) {
            txtId.setText(dataModel.get(row).getId());
            txtName.setText(dataModel.get(row).getName());
            txtPassword.setText("");
            txtPasswordConfirm.setText("");
            cboType.setSelectedItem(dataModel.get(row).getType());
            chkActive.setSelected(dataModel.get(row).isActive());
        } else {
            txtId.setText("");
            txtName.setText("");
            txtPassword.setText("");
            txtPasswordConfirm.setText("");
            cboType.setSelectedIndex(-1);
            chkActive.setSelected(false);
        }
    }

    private void updateState() {
        UserModel currentUser = SessionManager.getCurrentUser();

        txtId.setEnabled(editorState.equals(EditorState.ADD_NEW));
        txtName.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT));
        txtPassword.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.CHANGE_PASSWORD));
        txtPasswordConfirm.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.CHANGE_PASSWORD));
        cboType.setEnabled(
            editorState.equals(EditorState.ADD_NEW) ||
                (editorState.equals(EditorState.EDIT) && currentUser.getType().equals(UserModel.UserType.ADMINISTRATOR) && !currentUser.getId().equals(txtId.getText())));
        chkActive.setEnabled(
            editorState.equals(EditorState.ADD_NEW) ||
                (editorState.equals(EditorState.EDIT) && currentUser.getType().equals(UserModel.UserType.ADMINISTRATOR) && !currentUser.getId().equals(txtId.getText())));
        btnAdd.setEnabled(editorState.equals(EditorState.NONE));
        btnEdit.setEnabled((tblUserAccount.getSelectedRow() != -1) && editorState.equals(EditorState.NONE));
        btnDelete.setEnabled((tblUserAccount.getSelectedRow() != -1) && editorState.equals(EditorState.NONE));
        btnChangePassword.setEnabled(
            editorState.equals(EditorState.NONE) &&
                (currentUser.getType().equals(UserModel.UserType.ADMINISTRATOR) || currentUser.getId().equals(txtId.getText())));
        btnSave.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT) || editorState.equals(EditorState.CHANGE_PASSWORD));
        btnCancel.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT) || editorState.equals(EditorState.CHANGE_PASSWORD));
        txtSearch.setEnabled((tblUserAccount.getSelectedRow() != -1) && editorState.equals(EditorState.NONE));
        btnSearch.setEnabled((tblUserAccount.getSelectedRow() != -1) && editorState.equals(EditorState.NONE));
        tblUserAccount.setEnabled(editorState.equals(EditorState.NONE));
    }
}
