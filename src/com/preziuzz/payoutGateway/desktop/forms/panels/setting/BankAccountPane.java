package com.preziuzz.payoutGateway.desktop.forms.panels.setting;

import com.preziuzz.payoutGateway.desktop.core.model.BankAccountModel;
import com.preziuzz.payoutGateway.desktop.core.model.BankModel;
import com.preziuzz.payoutGateway.desktop.core.service.BankAccountService;
import com.preziuzz.payoutGateway.desktop.core.service.BankService;
import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import com.preziuzz.payoutGateway.desktop.utility.extgui.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Represents the bank account panel.
 *
 * @author Erik P. Yuntantyo
 */
public class BankAccountPane extends JPanelExt {
    private JButtonExt btnAdd;
    private JButtonExt btnCancel;
    private JButtonExt btnDelete;
    private JButtonExt btnEdit;
    private JButtonExt btnSave;
    private JButtonExt btnSearch;
    private JComboBox cboBankCode;
    private JCheckBox chkShowPin;
    private JLabel lblAccountNumber;
    private JLabel lblBankCode;
    private JLabel lblDialNumber;
    private JLabel lblEncryptedDialNumber;
    private JLabel lblName;
    private JLabel lblPin;
    private JLabel lblSMSFormat;
    private JLabel lblSMSFormatInfo;
    private JLabel lblSMSResponseFormat;
    private JLabel lblSMSResponseFormatInfo;
    private JSeparator sprHorizontal;
    private JSeparator sprVertical1;
    private JSeparator sprVertical2;
    private CustomTableManager tblBankAccount;
    private JTextField txtAccountNumber;
    private JTextField txtDialNumber;
    private JTextField txtEncryptedDialNumber;
    private JTextField txtName;
    private JTextField txtPin;
    private JPasswordField txtPinHidden;
    private JTextFieldExt txtSearch;
    private JTextField txtSMSFormat1;
    private JTextField txtSMSFormat2;
    private JTextField txtSMSResponseFormat;

    private List<BankAccountModel> dataModel;
    private EditorState editorState;
    private String id;

    @Override
    protected void preInitialize() {
        editorState = EditorState.NONE;

        setBackground(SystemColor.window);
        setLayout(null);

        lblName = new JLabel("Nama Bank");
        lblName.setFont(getFont().deriveFont(Font.BOLD));
        lblName.setForeground(SystemColor.textInactiveText.darker());
        lblName.setHorizontalAlignment(SwingConstants.RIGHT);

        txtName = new JTextField();
        txtName.setFont(getFont().deriveFont(12f));

        lblBankCode = new JLabel("Kode Bank");
        lblBankCode.setFont(getFont().deriveFont(Font.BOLD));
        lblBankCode.setForeground(SystemColor.textInactiveText.darker());
        lblBankCode.setHorizontalAlignment(SwingConstants.RIGHT);

        cboBankCode = new JComboBox();
        cboBankCode.setFont(getFont().deriveFont(12f));
        cboBankCode.setRenderer(CustomListCellRenderer.initialize());

        lblAccountNumber = new JLabel("No. Rekening");
        lblAccountNumber.setFont(getFont().deriveFont(Font.BOLD));
        lblAccountNumber.setForeground(SystemColor.textInactiveText.darker());
        lblAccountNumber.setHorizontalAlignment(SwingConstants.RIGHT);

        txtAccountNumber = new JTextField();
        txtAccountNumber.setFont(getFont().deriveFont(12f));

        lblPin = new JLabel("Pin");
        lblPin.setFont(getFont().deriveFont(Font.BOLD));
        lblPin.setForeground(SystemColor.textInactiveText.darker());
        lblPin.setHorizontalAlignment(SwingConstants.RIGHT);

        txtPinHidden = new JPasswordField();
        txtPinHidden.setFont(getFont().deriveFont(12f));

        txtPin = new JTextField();
        txtPin.setFont(getFont().deriveFont(12f));

        chkShowPin = new JCheckBox("Tampilkan Pin");
        chkShowPin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtPin.setVisible(((JCheckBox)e.getSource()).isSelected());
                txtPinHidden.setVisible(!((JCheckBox)e.getSource()).isSelected());

                if (((JCheckBox)e.getSource()).isSelected()) {
                    txtPin.setText(new String(txtPinHidden.getPassword()));
                } else {
                    txtPinHidden.setText(txtPin.getText());
                }
            }
        });
        chkShowPin.setForeground(SystemColor.textInactiveText.darker());
        chkShowPin.setOpaque(false);

        lblDialNumber = new JLabel("Nomor Tujuan");
        lblDialNumber.setFont(getFont().deriveFont(Font.BOLD));
        lblDialNumber.setForeground(SystemColor.textInactiveText.darker());
        lblDialNumber.setHorizontalAlignment(SwingConstants.RIGHT);

        txtDialNumber = new JTextField();
        txtDialNumber.setFont(getFont().deriveFont(12f));

        lblEncryptedDialNumber = new JLabel("Enkripsi Nomor");
        lblEncryptedDialNumber.setFont(getFont().deriveFont(Font.BOLD));
        lblEncryptedDialNumber.setForeground(SystemColor.textInactiveText.darker());
        lblEncryptedDialNumber.setHorizontalAlignment(SwingConstants.RIGHT);

        txtEncryptedDialNumber = new JTextField();
        txtEncryptedDialNumber.setFont(getFont().deriveFont(12f));

        lblSMSFormat = new JLabel("Format SMS");
        lblSMSFormat.setFont(getFont().deriveFont(Font.BOLD));
        lblSMSFormat.setForeground(SystemColor.textInactiveText.darker());
        lblSMSFormat.setHorizontalAlignment(SwingConstants.RIGHT);

        txtSMSFormat1 = new JTextField();
        txtSMSFormat1.setFont(getFont().deriveFont(12f));

        lblSMSFormatInfo = new JLabel(ImageUtility.ICON_INFORMATION_SMALL);
        lblSMSFormatInfo.setToolTipText("Karakter Pengganti: {ACC_NO} = no. rekening, {BANK_NO} = kode bank, {MONEY} = total transfer");

        txtSMSFormat2 = new JTextField();
        txtSMSFormat2.setFont(getFont().deriveFont(12f));

        lblSMSResponseFormat = new JLabel("Format Respon");
        lblSMSResponseFormat.setFont(getFont().deriveFont(Font.BOLD));
        lblSMSResponseFormat.setForeground(SystemColor.textInactiveText.darker());
        lblSMSResponseFormat.setHorizontalAlignment(SwingConstants.RIGHT);

        txtSMSResponseFormat = new JTextField();
        txtSMSResponseFormat.setFont(getFont().deriveFont(12f));

        lblSMSResponseFormatInfo = new JLabel(ImageUtility.ICON_INFORMATION_SMALL);
        lblSMSResponseFormatInfo.setToolTipText("Karakter Pengganti: {*} = semua karakter/non-karakter, {D} = angka, {A} = huruf");

        sprHorizontal = new JSeparator(SwingConstants.HORIZONTAL);

        btnAdd = new JButtonExt(ImageUtility.ICON_ADD);
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addData();
            }
        });
        btnAdd.setToolTipText("Tambah Akun Bank");

        btnEdit = new JButtonExt(ImageUtility.ICON_EDIT);
        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editData();
            }
        });
        btnEdit.setToolTipText("Ubah Akun Bank");

        btnDelete = new JButtonExt(ImageUtility.ICON_DELETE);
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteData();
            }
        });
        btnDelete.setToolTipText("Hapus Akun Bank");

        sprVertical1 = new JSeparator(SwingConstants.VERTICAL);

        btnSave = new JButtonExt(ImageUtility.ICON_SAVE);
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });
        btnSave.setToolTipText("Simpan Akun Bank");

        btnCancel = new JButtonExt(ImageUtility.ICON_CANCEL);
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelEdit();
            }
        });
        btnCancel.setToolTipText("Batal Tambah/Ubah Data");

        sprVertical2 = new JSeparator(SwingConstants.VERTICAL);

        txtSearch = new JTextFieldExt("Pencarian");
        txtSearch.setPlaceHolderFont(getFont().deriveFont(Font.BOLD));

        btnSearch = new JButtonExt(ImageUtility.ICON_SEARCH);
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchData();
                updateState();
            }
        });
        btnSearch.setToolTipText("Cari Data");

        dataModel = BankAccountService.getAllBankAccounts();

        tblBankAccount = CustomTableManager.initializeTable(
            CustomTableColumnManager.initialize("Bank", "name", 100),
            CustomTableColumnManager.initialize("No. Tujuan", "dialNumber", 100),
            CustomTableColumnManager.initialize("Format SMS", "smsFormat1"));
        tblBankAccount.addRowSelectionChangedListener(new CustomTableRowSelectionListener() {
            @Override
            public void rowSelectionChanged(int row) {
                selectData(row);
            }
        });
        tblBankAccount.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblBankAccount.addData(dataModel);

        add(lblName);
        add(txtName);
        add(lblBankCode);
        add(cboBankCode);
        add(lblAccountNumber);
        add(txtAccountNumber);
        add(lblPin);
        add(txtPinHidden);
        add(txtPin);
        add(chkShowPin);
        add(lblDialNumber);
        add(txtDialNumber);
        add(lblEncryptedDialNumber);
        add(txtEncryptedDialNumber);
        add(lblSMSFormat);
        add(txtSMSFormat1);
        add(lblSMSFormatInfo);
        add(txtSMSFormat2);
        add(lblSMSResponseFormat);
        add(txtSMSResponseFormat);
        add(lblSMSResponseFormatInfo);
        add(sprHorizontal);
        add(btnAdd);
        add(btnEdit);
        add(btnDelete);
        add(sprVertical1);
        add(btnSave);
        add(btnCancel);
        add(sprVertical2);
        add(txtSearch);
        add(btnSearch);
        add(tblBankAccount.getComponent());

        selectData(0);
        updateState();
    }

    @Override
    protected void resized() {
        lblName.setBounds(20, 20, 100, 25);
        txtName.setBounds(130, 20, 200, 25);
        lblBankCode.setBounds(20, 50, 100, 25);
        cboBankCode.setBounds(130, 50, 60, 25);
        lblAccountNumber.setBounds(20, 80, 100, 25);
        txtAccountNumber.setBounds(130, 80, 100, 25);
        lblPin.setBounds(20, 110, 100, 25);
        txtPinHidden.setBounds(130, 110, 100, 25);
        txtPin.setBounds(130, 110, 100, 25);
        chkShowPin.setBounds(235, 110, 100, 25);
        lblDialNumber.setBounds(20, 140, 100, 25);
        txtDialNumber.setBounds(130, 140, 100, 25);
        lblEncryptedDialNumber.setBounds(20, 170, 100, 25);
        txtEncryptedDialNumber.setBounds(130, 170, 100, 25);
        lblSMSFormat.setBounds(20, 200, 100, 25);
        txtSMSFormat1.setBounds(130, 200, 310, 25);
        lblSMSFormatInfo.setBounds(445, 200, 16, 25);
        txtSMSFormat2.setBounds(130, 230, 310, 25);
        lblSMSResponseFormat.setBounds(20, 260, 100, 25);
        txtSMSResponseFormat.setBounds(130, 260, 310, 25);
        lblSMSResponseFormatInfo.setBounds(445, 260, 16, 25);
        sprHorizontal.setBounds(10, 290, getWidth() - 20, 5);
        btnAdd.setBounds(10, 295, 30, 30);
        btnEdit.setBounds(45, 295, 30, 30);
        btnDelete.setBounds(80, 295, 30, 30);
        sprVertical1.setBounds(115, 295, 5, 30);
        btnSave.setBounds(120, 295, 30, 30);
        btnCancel.setBounds(155, 295, 30, 30);
        sprVertical2.setBounds(190, 295, 5, 30);
        txtSearch.setBounds(
            sprVertical2.getX() + sprVertical2.getWidth(),
            295,
            getWidth() - sprVertical2.getX() - sprVertical2.getWidth() - 40,
            30);
        btnSearch.setBounds(txtSearch.getX() + txtSearch.getWidth(), 295, 30, 30);
        tblBankAccount.setBounds(10, 330, getWidth() - 20, getHeight() - sprHorizontal.getY() - sprHorizontal.getHeight() - 45);
    }

    @Override
    protected void shown() {
        List<BankModel> models = BankService.getAllBanks();

        cboBankCode.removeAllItems();

        for (BankModel model : models) {
            cboBankCode.addItem(CustomListItem.initialize(model.getCode()));
        }

        if ((dataModel != null) && !StringUtility.isNullOrEmpty(dataModel.get(0).getCode())) {
            cboBankCode.setSelectedItem(CustomListItem.initialize(dataModel.get(0).getCode()));
        } else {
            cboBankCode.setSelectedIndex(-1);
        }
    }

    private void addData() {
        editorState = EditorState.ADD_NEW;
        id = null;

        updateState();

        txtName.setText("");
        cboBankCode.setSelectedIndex(-1);
        txtAccountNumber.setText("");
        txtPin.setText("");
        txtPinHidden.setText("");
        chkShowPin.setSelected(false);
        txtDialNumber.setText("");
        txtEncryptedDialNumber.setText("");
        txtSMSFormat1.setText("");
        txtSMSFormat2.setText("");
        txtSMSResponseFormat.setText("");
        txtName.requestFocus();
    }

    private void cancelEdit() {
        editorState = EditorState.NONE;

        txtSearch.setText("");
        searchData();
        selectData(0);
        updateState();
    }

    private void deleteData() {
        if (MessageDialog.showConfirmation("Apakah ada akan menghapus data akun bank?", "Hapus Data Akun Bank")) {
            txtSearch.setText("");

            if (BankAccountService.deleteBankAccount(dataModel.get(tblBankAccount.getSelectedRow()))) {
                searchData();
                selectData(0);
                updateState();
            } else {
                MessageDialog.showWarning("Akun bank tidak bisa dihapus, sedang digunakan.");
            }
        }
    }

    private void editData() {
        editorState = EditorState.EDIT;
        updateState();
    }

    private void saveData() {
        if ((!StringUtility.isNullOrEmpty(txtPin.getText()) || !StringUtility.isNullOrEmpty(new String(txtPinHidden.getPassword()))) &&
            !StringUtility.isNullOrEmpty(txtName.getText()) && !StringUtility.isNullOrEmpty(txtAccountNumber.getText()) &&
            (cboBankCode.getSelectedIndex() != -1) && !StringUtility.isNullOrEmpty(txtDialNumber.getText()) &&
            !StringUtility.isNullOrEmpty(txtEncryptedDialNumber.getText()) && !StringUtility.isNullOrEmpty(txtSMSFormat1.getText()) &&
            !StringUtility.isNullOrEmpty(txtSMSFormat2.getText()) && !StringUtility.isNullOrEmpty(txtSMSResponseFormat.getText())) {
            if ((BankAccountService.getBankAccountByName(txtName.getText()) == null) || !editorState.equals(EditorState.ADD_NEW)) {
                txtSearch.setText("");

                BankAccountModel model = new BankAccountModel();
                model.setId(id);
                model.setName(txtName.getText());
                model.setCode(((CustomListItem)cboBankCode.getSelectedItem()).getValueMember().toString());
                model.setAccountNumber(txtAccountNumber.getText());
                model.setDialNumber(txtDialNumber.getText());
                model.setEncryptedDialNumber(txtEncryptedDialNumber.getText());
                model.setPin(chkShowPin.isSelected() ? txtPin.getText() : new String(txtPinHidden.getPassword()));
                model.setSmsFormat1(txtSMSFormat1.getText());
                model.setSmsResponseFormat(txtSMSResponseFormat.getText());

                if (!StringUtility.isNullOrEmpty(txtSMSFormat2.getText())) {
                    model.setSmsFormat2(txtSMSFormat2.getText());
                }

                if (BankAccountService.update(model)) {
                    editorState = EditorState.NONE;

                    searchData();
                    selectData(0);
                    updateState();
                }
            } else {
                MessageDialog.showWarning("Nama Bank sudah ada.");
            }
        } else {
            MessageDialog.showWarning("Data tidak boleh kosong.");
        }
    }

    private void searchData() {
        dataModel = BankAccountService.searchBankAccount(txtSearch.getText());
        tblBankAccount.addData(dataModel);
    }

    private void selectData(int row) {
        if ((dataModel != null) && (dataModel.size() > 0)) {
            id = dataModel.get(row).getId();
            txtName.setText(dataModel.get(row).getName());
            cboBankCode.setSelectedItem(CustomListItem.initialize(dataModel.get(row).getCode()));
            txtAccountNumber.setText(dataModel.get(row).getAccountNumber());
            txtPin.setText(dataModel.get(row).getPin());
            txtPin.setVisible(false);
            txtPinHidden.setText(dataModel.get(row).getPin());
            txtPinHidden.setVisible(true);
            chkShowPin.setSelected(false);
            txtDialNumber.setText(dataModel.get(row).getDialNumber());
            txtEncryptedDialNumber.setText(dataModel.get(row).getEncryptedDialNumber());
            txtSMSFormat1.setText(dataModel.get(row).getSmsFormat1());
            txtSMSFormat2.setText(dataModel.get(row).getSmsFormat2());
            txtSMSResponseFormat.setText(dataModel.get(row).getSmsResponseFormat());
        } else {
            id = null;
            txtName.setText("");
            cboBankCode.setSelectedIndex(-1);
            txtAccountNumber.setText("");
            txtPin.setText("");
            txtPinHidden.setText("");
            chkShowPin.setSelected(false);
            txtDialNumber.setText("");
            txtEncryptedDialNumber.setText("");
            txtSMSFormat1.setText("");
            txtSMSFormat2.setText("");
            txtSMSResponseFormat.setText("");
        }
    }

    private void updateState() {
        boolean editMode = editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT);

        txtName.setEnabled(editMode);
        cboBankCode.setEnabled(editMode);
        txtAccountNumber.setEnabled(editMode);
        txtPin.setEnabled(editMode);
        txtPinHidden.setEnabled(editMode);
        chkShowPin.setEnabled(editMode);
        txtDialNumber.setEnabled(editMode);
        txtEncryptedDialNumber.setEnabled(editMode);
        txtSMSFormat1.setEnabled(editMode);
        txtSMSFormat2.setEnabled(editMode);
        txtSMSResponseFormat.setEnabled(editMode);
        btnAdd.setEnabled(editorState.equals(EditorState.NONE));
        btnEdit.setEnabled((tblBankAccount.getSelectedRow() != -1) && editorState.equals(EditorState.NONE));
        btnDelete.setEnabled((tblBankAccount.getSelectedRow() != -1) && editorState.equals(EditorState.NONE));
        btnSave.setEnabled(editMode);
        btnCancel.setEnabled(editMode);
        txtSearch.setEnabled((tblBankAccount.getSelectedRow() != -1) && editorState.equals(EditorState.NONE));
        btnSearch.setEnabled((tblBankAccount.getSelectedRow() != -1) && editorState.equals(EditorState.NONE));
        tblBankAccount.setEnabled(editorState.equals(EditorState.NONE));
    }
}
