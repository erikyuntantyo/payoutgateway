package com.preziuzz.payoutGateway.desktop.forms.panels.setting;

import com.preziuzz.payoutGateway.desktop.utility.common.Constants;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JButtonExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JPanelExt;
import com.preziuzz.simpleLic.License;

import javax.swing.*;
import java.awt.*;
import java.util.Calendar;

/**
 * Represent the license panel.
 *
 * @author Erik P. Yuntantyo
 */
public class LicensePane extends JPanelExt {
    private JButtonExt btnUpdate;
    private JLabel lblExpires;
    private JLabel lblId;
    private JLabel lblLicenseTo;
    private JLabel lblName;
    private JLabel lblType;
    private JLabel lblVersion;
    private JTextField txtExpires;
    private JTextField txtId;
    private JTextField txtLicenseTo;
    private JTextField txtName;
    private JTextField txtType;
    private JTextField txtVersion;

    @Override
    protected void preInitialize() {
        setBackground(SystemColor.window);
        setLayout(null);

        lblId = new JLabel("Produk Id");
        lblId.setFont(getFont().deriveFont(Font.BOLD));
        lblId.setForeground(SystemColor.textInactiveText.darker());
        lblId.setHorizontalAlignment(SwingConstants.RIGHT);

        txtId = new JTextField(License.getProductId());
        txtId.setEnabled(false);
        txtId.setFont(getFont().deriveFont(12f));

        lblName = new JLabel("Nama Produk");
        lblName.setFont(getFont().deriveFont(Font.BOLD));
        lblName.setForeground(SystemColor.textInactiveText.darker());
        lblName.setHorizontalAlignment(SwingConstants.RIGHT);

        txtName = new JTextField(Constants.PRODUCT_NAME);
        txtName.setEnabled(false);
        txtName.setFont(getFont().deriveFont(12f));

        lblVersion = new JLabel("Versi Produk");
        lblVersion.setFont(getFont().deriveFont(Font.BOLD));
        lblVersion.setForeground(SystemColor.textInactiveText.darker());
        lblVersion.setHorizontalAlignment(SwingConstants.RIGHT);

        txtVersion = new JTextField(Constants.PRODUCT_VERSION);
        txtVersion.setEnabled(false);
        txtVersion.setFont(getFont().deriveFont(12f));

        lblType = new JLabel("Tipe Lisensi");
        lblType.setFont(getFont().deriveFont(Font.BOLD));
        lblType.setForeground(SystemColor.textInactiveText.darker());
        lblType.setHorizontalAlignment(SwingConstants.RIGHT);

        txtType = new JTextField(License.getLicenseType().toString());
        txtType.setEnabled(false);
        txtType.setFont(getFont().deriveFont(12f));

        lblExpires = new JLabel("Tanggal Berakhir");
        lblExpires.setFont(getFont().deriveFont(Font.BOLD));
        lblExpires.setForeground(SystemColor.textInactiveText.darker());
        lblExpires.setHorizontalAlignment(SwingConstants.RIGHT);

        int dateRanges = (int)((License.getLicenseEndedDate().getTime() - Calendar.getInstance().getTimeInMillis()) / (1000 * 60 * 60 * 24)) + 1;

        txtExpires = new JTextField(String.format("%s (%s hari lagi)", Constants.DATE_FORMAT_SHORT.format(License.getLicenseEndedDate()), dateRanges));
        txtExpires.setEnabled(false);
        txtExpires.setFont(getFont().deriveFont(12f));

        lblLicenseTo = new JLabel("Lisensi Untuk");
        lblLicenseTo.setFont(getFont().deriveFont(Font.BOLD));
        lblLicenseTo.setForeground(SystemColor.textInactiveText.darker());
        lblLicenseTo.setHorizontalAlignment(SwingConstants.RIGHT);

        txtLicenseTo = new JTextField("-");
        txtLicenseTo.setEnabled(false);
        txtLicenseTo.setFont(getFont().deriveFont(12f));

        btnUpdate = new JButtonExt("Perbarui Lisensi", ImageUtility.ICON_LICENSE);
        btnUpdate.setEnabled(false);

        add(lblId);
        add(txtId);
        add(lblName);
        add(txtName);
        add(lblVersion);
        add(txtVersion);
        add(lblType);
        add(txtType);
        add(lblExpires);
        add(txtExpires);
        add(lblLicenseTo);
        add(txtLicenseTo);
        add(btnUpdate);
    }

    @Override
    protected void resized() {
        lblId.setBounds(20, 20, 100, 25);
        txtId.setBounds(130, 20, getWidth() - 150, 25);
        lblName.setBounds(20, 50, 100, 25);
        txtName.setBounds(130, 50, getWidth() - 150, 25);
        lblVersion.setBounds(20, 80, 100, 25);
        txtVersion.setBounds(130, 80, getWidth() - 150, 25);
        lblType.setBounds(20, 110, 100, 25);
        txtType.setBounds(130, 110, getWidth() - 150, 25);
        lblExpires.setBounds(20, 140, 100, 25);
        txtExpires.setBounds(130, 140, getWidth() - 150, 25);
        lblLicenseTo.setBounds(20, 170, 100, 25);
        txtLicenseTo.setBounds(130, 170, getWidth() - 150, 25);
        btnUpdate.setBounds(getWidth() - 130, getHeight() - 40, 120, 30);
    }
}
