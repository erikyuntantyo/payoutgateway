package com.preziuzz.payoutGateway.desktop.forms.panels.setting;

import com.preziuzz.payoutGateway.desktop.core.model.BankModel;
import com.preziuzz.payoutGateway.desktop.core.service.BankService;
import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import com.preziuzz.payoutGateway.desktop.utility.extgui.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Represents the bank list panel.
 *
 * @author Erik P. Yuntantyo
 */
public class BankPane extends JPanelExt {
    private JButtonExt btnAdd;
    private JButtonExt btnCancel;
    private JButtonExt btnDelete;
    private JButtonExt btnEdit;
    private JButtonExt btnSave;
    private JButtonExt btnSearch;
    private JLabel lblAlternateName;
    private JLabel lblCode;
    private JLabel lblName;
    private JSeparator sprHorizontal;
    private JSeparator sprVertical1;
    private JSeparator sprVertical2;
    private CustomTableManager tblBank;
    private JTextField txtAlternateName;
    private JTextField txtCode;
    private JTextField txtName;
    private JTextFieldExt txtSearch;

    private List<BankModel> dataModel;
    private EditorState editorState;

    @Override
    protected void preInitialize() {
        editorState = EditorState.NONE;

        setBackground(SystemColor.window);
        setLayout(null);

        lblCode = new JLabel("Kode Bank");
        lblCode.setFont(getFont().deriveFont(Font.BOLD));
        lblCode.setForeground(SystemColor.textInactiveText.darker());
        lblCode.setHorizontalAlignment(SwingConstants.RIGHT);

        txtCode = new JTextField();
        txtCode.setFont(getFont().deriveFont(12f));

        lblName = new JLabel("Nama Bank");
        lblName.setFont(getFont().deriveFont(Font.BOLD));
        lblName.setForeground(SystemColor.textInactiveText.darker());
        lblName.setHorizontalAlignment(SwingConstants.RIGHT);

        txtName = new JTextField();
        txtName.setFont(getFont().deriveFont(12f));

        lblAlternateName = new JLabel("Nama Alternatif");
        lblAlternateName.setFont(getFont().deriveFont(Font.BOLD));
        lblAlternateName.setForeground(SystemColor.textInactiveText.darker());
        lblAlternateName.setHorizontalAlignment(SwingConstants.RIGHT);

        txtAlternateName = new JTextField();
        txtAlternateName.setFont(getFont().deriveFont(12f));

        sprHorizontal = new JSeparator(SwingConstants.HORIZONTAL);

        btnAdd = new JButtonExt(ImageUtility.ICON_ADD);
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addData();
            }
        });
        btnAdd.setToolTipText("Tambah Bank");

        btnEdit = new JButtonExt(ImageUtility.ICON_EDIT);
        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editData();
            }
        });
        btnEdit.setToolTipText("Ubah Bank");

        btnDelete = new JButtonExt(ImageUtility.ICON_DELETE);
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteData();
            }
        });
        btnDelete.setToolTipText("Hapus Bank");

        sprVertical1 = new JSeparator(SwingConstants.VERTICAL);

        btnSave = new JButtonExt(ImageUtility.ICON_SAVE);
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });
        btnSave.setToolTipText("Simpan Bank");

        btnCancel = new JButtonExt(ImageUtility.ICON_CANCEL);
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelEdit();
            }
        });
        btnCancel.setToolTipText("Batal Tambah/Ubah Data");

        sprVertical2 = new JSeparator(SwingConstants.VERTICAL);

        txtSearch = new JTextFieldExt("Pencarian");
        txtSearch.setPlaceHolderFont(getFont().deriveFont(Font.BOLD));

        btnSearch = new JButtonExt(ImageUtility.ICON_SEARCH);
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchData();
                updateState();
            }
        });
        btnSearch.setToolTipText("Cari Data");

        dataModel = BankService.getAllBanks();

        tblBank = CustomTableManager.initializeTable(
            CustomTableColumnManager.initialize("Kode", "code", 100),
            CustomTableColumnManager.initialize("Nama", "name"));
        tblBank.addRowSelectionChangedListener(new CustomTableRowSelectionListener() {
            @Override
            public void rowSelectionChanged(int row) {
                selectData(row);
            }
        });
        tblBank.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblBank.addData(dataModel);

        add(lblCode);
        add(txtCode);
        add(lblName);
        add(txtName);
        add(lblAlternateName);
        add(txtAlternateName);
        add(sprHorizontal);
        add(btnAdd);
        add(btnEdit);
        add(btnDelete);
        add(sprVertical1);
        add(btnSave);
        add(btnCancel);
        add(sprVertical2);
        add(txtSearch);
        add(btnSearch);
        add(tblBank.getComponent());

        selectData(0);
        updateState();
    }

    @Override
    protected void resized() {
        lblCode.setBounds(20, 20, 100, 25);
        txtCode.setBounds(130, 20, 100, 25);
        lblName.setBounds(20, 50, 100, 25);
        txtName.setBounds(130, 50, 200, 25);
        lblAlternateName.setBounds(20, 80, 100, 25);
        txtAlternateName.setBounds(130, 80, 335, 25);
        sprHorizontal.setBounds(10, 110, getWidth() - 20, 5);
        btnAdd.setBounds(10, 115, 30, 30);
        btnEdit.setBounds(45, 115, 30, 30);
        btnDelete.setBounds(80, 115, 30, 30);
        sprVertical1.setBounds(115, 115, 5, 30);
        btnSave.setBounds(120, 115, 30, 30);
        btnCancel.setBounds(155, 115, 30, 30);
        sprVertical2.setBounds(190, 115, 5, 30);
        txtSearch.setBounds(
            sprVertical2.getX() + sprVertical2.getWidth(),
            115,
            getWidth() - sprVertical2.getX() - sprVertical2.getWidth() - 40,
            30);
        btnSearch.setBounds(txtSearch.getX() + txtSearch.getWidth(), 115, 30, 30);
        tblBank.setBounds(10, 150, getWidth() - 20, getHeight() - sprHorizontal.getY() - sprHorizontal.getHeight() - 45);
    }

    private void addData() {
        editorState = EditorState.ADD_NEW;

        updateState();

        txtCode.setText("");
        txtName.setText("");
        txtAlternateName.setText("");
        txtCode.requestFocus();
    }

    private void cancelEdit() {
        editorState = EditorState.NONE;

        txtSearch.setText("");
        searchData();
        selectData(0);
        updateState();
    }

    private void deleteData() {
        if (MessageDialog.showConfirmation("Apakah ada akan menghapus data bank?", "Hapus Data Bank")) {
            txtSearch.setText("");

            if (BankService.deleteBank(dataModel.get(tblBank.getSelectedRow()))) {
                searchData();
                selectData(0);
                updateState();
            } else {
                MessageDialog.showWarning("Data bank tidak bisa dihapus, sedang digunakan.");
            }
        }
    }

    private void editData() {
        editorState = EditorState.EDIT;
        updateState();
        txtName.requestFocus();
    }

    private void saveData() {
        if (!StringUtility.isNullOrEmpty(txtCode.getText()) && !StringUtility.isNullOrEmpty(txtName.getText())) {
            if ((BankService.getBankByCode(txtCode.getText()) == null) || !editorState.equals(EditorState.ADD_NEW)) {
                txtSearch.setText("");

                BankModel model = new BankModel();

                model.setCode(txtCode.getText());
                model.setName(txtName.getText());

                if (!StringUtility.isNullOrEmpty(txtAlternateName.getText())) {
                    model.setAlternateName(txtAlternateName.getText());
                }

                if (BankService.update(model)) {
                    editorState = EditorState.NONE;

                    searchData();
                    selectData(0);
                    updateState();
                }
            } else {
                MessageDialog.showWarning("Kode bank sudah ada.");
            }
        } else {
            MessageDialog.showWarning("Kode dan Nama bank tidak boleh kosong.");
        }
    }

    private void searchData() {
        dataModel = BankService.searchBank(txtSearch.getText());
        tblBank.addData(dataModel);
    }

    private void selectData(int row) {
        if ((dataModel != null) && (dataModel.size() > 0)) {
            txtCode.setText(dataModel.get(row).getCode());
            txtName.setText(dataModel.get(row).getName());
            txtAlternateName.setText(dataModel.get(row).getAlternateName());
        } else {
            txtCode.setText("");
            txtName.setText("");
        }
    }

    private void updateState() {
        txtCode.setEnabled(editorState.equals(EditorState.ADD_NEW));
        txtName.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT));
        txtAlternateName.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT));
        btnAdd.setEnabled(!editorState.equals(EditorState.ADD_NEW) && !editorState.equals(EditorState.EDIT));
        btnEdit.setEnabled((tblBank.getSelectedRow() != -1) && !editorState.equals(EditorState.ADD_NEW) && !editorState.equals(EditorState.EDIT));
        btnDelete.setEnabled((tblBank.getSelectedRow() != -1) && !editorState.equals(EditorState.ADD_NEW) && !editorState.equals(EditorState.EDIT));
        btnSave.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT));
        btnCancel.setEnabled(editorState.equals(EditorState.ADD_NEW) || editorState.equals(EditorState.EDIT));
        txtSearch.setEnabled((tblBank.getSelectedRow() != -1) && !editorState.equals(EditorState.ADD_NEW) && !editorState.equals(EditorState.EDIT));
        btnSearch.setEnabled((tblBank.getSelectedRow() != -1) && !editorState.equals(EditorState.ADD_NEW) && !editorState.equals(EditorState.EDIT));
        tblBank.setEnabled(!editorState.equals(EditorState.ADD_NEW) && !editorState.equals(EditorState.EDIT));
    }
}
