package com.preziuzz.payoutGateway.desktop.forms.panels.setting;

import com.preziuzz.SimpleGateway.core.GatewayDriverIdentifier;
import com.preziuzz.SimpleGateway.core.GatewayServiceTypes;
import com.preziuzz.SimpleGateway.factory.GatewayFactory;
import com.preziuzz.payoutGateway.desktop.core.model.BankAccountModel;
import com.preziuzz.payoutGateway.desktop.core.service.BankAccountService;
import com.preziuzz.payoutGateway.desktop.utility.common.SessionManager;
import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;
import com.preziuzz.payoutGateway.desktop.utility.extgui.CustomListCellRenderer;
import com.preziuzz.payoutGateway.desktop.utility.extgui.CustomListItem;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JPanelExt;

import javax.comm.CommPortIdentifier;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Represents the general setting panel.
 *
 * @author Erik P. Yuntantyo
 */
public class GeneralSettingPane extends JPanelExt {
    private JCheckBox chkAllowTransBank;
    private JCheckBox chkAutoLogOff;
    private JCheckBox chkConfirmOnExit;
    private JComboBox cboBankAccount;
    private JComboBox cboDeviceModel;
    private JComboBox cboLanguage;
    private JComboBox cboPorts;
    private JLabel lblAllowTransBank;
    private JLabel lblAutoLogOff;
    private JLabel lblLogInTimeOutLegend;
    private JLabel lblBankAccount;
    private JLabel lblConfirmOnExit;
    private JLabel lblLanguage;
    private JLabel lblDeviceModel;
    private JLabel lblPorts;
    private JSpinner spnLogInTimeOut;

    @Override
    protected void preInitialize() {
        setBackground(SystemColor.window);
        setLayout(null);

        lblBankAccount = new JLabel("Akun Bank");
        lblBankAccount.setFont(getFont().deriveFont(Font.BOLD));
        lblBankAccount.setForeground(SystemColor.textInactiveText.darker());
        lblBankAccount.setHorizontalAlignment(SwingConstants.RIGHT);

        cboBankAccount = new JComboBox();
        cboBankAccount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                publish("UPDATE_BANK_ID", ((CustomListItem)((JComboBox)e.getSource()).getSelectedItem()).getValue());
            }
        });
        cboBankAccount.setFont(getFont().deriveFont(12f));
        cboBankAccount.setRenderer(CustomListCellRenderer.initialize());

        lblAllowTransBank = new JLabel("Antar Bank");
        lblAllowTransBank.setFont(getFont().deriveFont(Font.BOLD));
        lblAllowTransBank.setForeground(SystemColor.textInactiveText.darker());
        lblAllowTransBank.setHorizontalAlignment(SwingConstants.RIGHT);

        chkAllowTransBank = new JCheckBox();
        chkAllowTransBank.setEnabled(false);
        chkAllowTransBank.setOpaque(false);

        lblLanguage = new JLabel("Bahasa");
        lblLanguage.setFont(getFont().deriveFont(Font.BOLD));
        lblLanguage.setForeground(SystemColor.textInactiveText.darker());
        lblLanguage.setHorizontalAlignment(SwingConstants.RIGHT);

        cboLanguage = new JComboBox(new Object[] { "Bahasa Indonesia" });
        cboLanguage.setEnabled(false);
        cboLanguage.setFont(getFont().deriveFont(12f));

        lblAutoLogOff = new JLabel("Kunci Otomatis");
        lblAutoLogOff.setFont(getFont().deriveFont(Font.BOLD));
        lblAutoLogOff.setForeground(SystemColor.textInactiveText.darker());
        lblAutoLogOff.setHorizontalAlignment(SwingConstants.RIGHT);

        chkAutoLogOff = new JCheckBox();
        chkAutoLogOff.setEnabled(false);
        chkAutoLogOff.setOpaque(false);

        spnLogInTimeOut = new JSpinner(new SpinnerNumberModel());
        spnLogInTimeOut.setEnabled(chkAutoLogOff.isSelected());
        spnLogInTimeOut.setValue(0);

        lblLogInTimeOutLegend = new JLabel("detik");
        lblLogInTimeOutLegend.setFont(getFont().deriveFont(Font.BOLD));
        lblLogInTimeOutLegend.setForeground(SystemColor.textInactiveText.darker());

        lblConfirmOnExit = new JLabel("Konfirmasi Keluar");
        lblConfirmOnExit.setFont(getFont().deriveFont(Font.BOLD));
        lblConfirmOnExit.setForeground(SystemColor.textInactiveText.darker());
        lblConfirmOnExit.setHorizontalAlignment(SwingConstants.RIGHT);

        chkConfirmOnExit = new JCheckBox();
        chkConfirmOnExit.setEnabled(false);
        chkConfirmOnExit.setOpaque(false);

        lblPorts = new JLabel("GSM Port");
        lblPorts.setFont(getFont().deriveFont(Font.BOLD));
        lblPorts.setForeground(SystemColor.textInactiveText.darker());
        lblPorts.setHorizontalAlignment(SwingConstants.RIGHT);

        cboPorts = new JComboBox();
        cboPorts.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                publish("UPDATE_DEVICE_PORT", ((JComboBox)e.getSource()).getSelectedItem());
            }
        });
        cboPorts.setFont(getFont().deriveFont(12f));
        cboPorts.setRenderer(CustomListCellRenderer.initialize());

        lblDeviceModel = new JLabel("Model Perangkat");
        lblDeviceModel.setFont(getFont().deriveFont(Font.BOLD));
        lblDeviceModel.setForeground(SystemColor.textInactiveText.darker());
        lblDeviceModel.setHorizontalAlignment(SwingConstants.RIGHT);

        cboDeviceModel = new JComboBox();
        cboDeviceModel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                publish("UPDATE_DEVICE_MODEL", ((CustomListItem)((JComboBox)e.getSource()).getSelectedItem()).getValue());
            }
        });
        cboDeviceModel.setFont(getFont().deriveFont(12f));
        cboDeviceModel.setRenderer(CustomListCellRenderer.initialize());

        add(lblBankAccount);
        add(cboBankAccount);
        add(lblAllowTransBank);
        add(chkAllowTransBank);
        add(lblLanguage);
        add(cboLanguage);
        add(lblAutoLogOff);
        add(chkAutoLogOff);
        add(spnLogInTimeOut);
        add(lblLogInTimeOutLegend);
        add(lblConfirmOnExit);
        add(chkConfirmOnExit);
        add(lblPorts);
        add(cboPorts);
        add(lblDeviceModel);
        add(cboDeviceModel);
    }

    @Override
    protected void resized() {
        lblBankAccount.setBounds(20, 20, 100, 25);
        cboBankAccount.setBounds(130, 20, 200, 25);
        lblAllowTransBank.setBounds(20, 50, 100, 25);
        chkAllowTransBank.setBounds(130, 50, 200, 25);
        lblLanguage.setBounds(20, 80, 100, 25);
        cboLanguage.setBounds(130, 80, 200, 25);
        lblAutoLogOff.setBounds(20, 110, 100, 25);
        chkAutoLogOff.setBounds(130, 110, 30, 25);
        spnLogInTimeOut.setBounds(160, 110, 40, 25);
        lblLogInTimeOutLegend.setBounds(205, 110, 50, 25);
        lblConfirmOnExit.setBounds(20, 140, 100, 25);
        chkConfirmOnExit.setBounds(130, 140, 30, 25);
        lblPorts.setBounds(20, 170, 100, 25);
        cboPorts.setBounds(130, 170, 200, 25);
        lblDeviceModel.setBounds(20, 200, 100, 25);
        cboDeviceModel.setBounds(130, 200, 200, 25);
    }

    @Override
    protected void shown() {
        List<BankAccountModel> models = BankAccountService.getAllBankAccounts();

        cboBankAccount.removeAllItems();
        cboBankAccount.addItem(CustomListItem.initialize());

        for (BankAccountModel model : models) {
            cboBankAccount.addItem(CustomListItem.initialize(model.getId(), model.getName()));
        }

        if (!StringUtility.isNullOrEmpty(SessionManager.getCurrentUser().getBankId())) {
            cboBankAccount.setSelectedItem(CustomListItem.initialize(SessionManager.getCurrentUser().getBankId()));
        } else {
            cboBankAccount.setSelectedIndex(0);
        }

        cboDeviceModel.removeAllItems();
        cboDeviceModel.addItem(CustomListItem.initialize());

        for (Class<? extends GatewayFactory> serviceType : GatewayServiceTypes.getServiceTypes()) {
            cboDeviceModel.addItem(CustomListItem.initialize(serviceType.getName(), serviceType.getSimpleName()));
        }

        if (!StringUtility.isNullOrEmpty(SessionManager.getCurrentSettings().getDeviceModel())) {
            cboDeviceModel.setSelectedItem(CustomListItem.initialize(SessionManager.getCurrentSettings().getDeviceModel()));
        } else {
            cboDeviceModel.setSelectedIndex(0);
        }

        cboPorts.removeAllItems();
        cboPorts.addItem(CustomListItem.initialize());

        for (CommPortIdentifier port : GatewayDriverIdentifier.getSerialPorts()) {
            cboPorts.addItem(CustomListItem.initialize(port.getName()));
        }

        if (!StringUtility.isNullOrEmpty(SessionManager.getCurrentSettings().getPort())) {
            cboPorts.setSelectedItem(CustomListItem.initialize(SessionManager.getCurrentSettings().getPort()));
        } else {
            cboPorts.setSelectedIndex(0);
        }
    }
}
