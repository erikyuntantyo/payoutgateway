package com.preziuzz.payoutGateway.desktop.forms.panels.setting.cells;

import com.preziuzz.payoutGateway.desktop.utility.common.Constants;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.common.SMSData;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JTablePanelCell;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the inbox table cell.
 *
 * @author Erik P. Yuntantyo
 */
public class InboxCell extends JTablePanelCell {
    private Map<Integer, ImageIcon> icons;
    private JLabel lblContent;
    private JLabel lblIcon;
    private JLabel lblInfo;
    private JLabel lblSender;
    private JLabel lblSentDate;

    public InboxCell() {
        initializeComponents();
    }

    @Override
    protected void setData(Object data) {
        SMSData sms = (SMSData)data;
        String smsContents = (sms.getContents().length() >= 120) ? sms.getContents().substring(0, 120) + "..." : sms.getContents();

        lblIcon.setIcon(icons.get(sms.isRead() ? 1 : 0));
        lblContent.setText(String.format("<html><body>%s</body></html>", smsContents));
        lblInfo.setText(String.format("<html><body>Indeks : %s | Pengirim : %s | Dikirim : %s</body></html>",
            String.valueOf(sms.getIndex()),
            sms.getSender(),
            Constants.DATE_FORMAT.format(sms.getSentDate())));
    }

    private void initializeComponents() {
        icons = new HashMap<Integer, ImageIcon>() {{
            put(0, ImageUtility.ICON_MAIL);
            put(1, ImageUtility.ICON_MAIL_OPEN);
        }};

        lblIcon = new JLabel();
        lblIcon.setBounds(3, 3, 16, 16);

        lblContent = new JLabel();
        lblContent.setBounds(22, 3, 410, 32);
        lblContent.setFont(getFont().deriveFont(Font.BOLD));
        lblContent.setVerticalAlignment(SwingConstants.TOP);

        lblInfo = new JLabel();
        lblInfo.setBounds(22, 40, 410, 16);
        lblInfo.setFont(getFont().deriveFont(10f));

        setLayout(null);
        setSize(1, 60);

        add(lblIcon);
        add(lblContent);
        add(lblInfo);
    }
}
