package com.preziuzz.payoutGateway.desktop.forms.panels.setting;

import com.preziuzz.SimpleGateway.GatewayManager;
import com.preziuzz.SimpleGateway.common.ErrorEvent;
import com.preziuzz.SimpleGateway.common.GatewayAdapter;
import com.preziuzz.SimpleGateway.common.GatewayListener;
import com.preziuzz.SimpleGateway.common.IncomingMessageEvent;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JButtonExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JPanelExt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Represents the USSD panel.
 *
 * @author Erik P. Yuntantyo
 */
public class USSDPane extends JPanelExt {
    private JButtonExt btnReset;
    private JButtonExt btnSend;
    private JLabel lblResponse;
    private JLabel lblUSSD;
    private JScrollPane scrResponse;
    private JTextArea txtResponse;
    private JTextField txtUSSD;

    private GatewayListener gatewayListener;

    @Override
    protected void preInitialize() {
        gatewayListener = new GatewayAdapter() {
            @Override
            public void errorReported(ErrorEvent e) {
                btnSend.setEnabled(true);
            }

            @Override
            public void ussdResponded(IncomingMessageEvent e) {
                txtResponse.setText(txtResponse.getText().isEmpty() ?
                                        e.getContents() :
                                        String.format("%s\n----------\n%s", txtResponse.getText(), e.getContents()));
                txtResponse.setCaretPosition(txtResponse.getText().length());
                btnSend.setEnabled(true);
            }
        };

        setBackground(SystemColor.window);
        setLayout(null);

        try {
            GatewayManager.getService().addGatewayListener(gatewayListener);
        } catch (IllegalAccessException ignore) {
        }

        lblUSSD = new JLabel("USSD");
        lblUSSD.setFont(getFont().deriveFont(Font.BOLD));
        lblUSSD.setForeground(SystemColor.textInactiveText.darker());
        lblUSSD.setHorizontalAlignment(SwingConstants.RIGHT);

        txtUSSD = new JTextField();
        txtUSSD.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if ((e.getKeyCode() == KeyEvent.VK_ENTER) && btnSend.isEnabled()) {
                    send();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtUSSD.setFont(getFont().deriveFont(12f));

        btnSend = new JButtonExt("Kirim");
        btnSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                send();
            }
        });

        btnReset = new JButtonExt("Reset");
        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtUSSD.setText("");

                try {
                    GatewayManager.getService().endUSSDSession();
                } catch (IllegalAccessException ignore) {
                }
            }
        });

        lblResponse = new JLabel("Balasan");
        lblResponse.setFont(getFont().deriveFont(Font.BOLD));
        lblResponse.setForeground(SystemColor.textInactiveText.darker());
        lblResponse.setHorizontalAlignment(SwingConstants.RIGHT);

        txtResponse = new JTextArea();
        txtResponse.setEditable(false);
        txtResponse.setFont(getFont().deriveFont(12f));
        txtResponse.setLineWrap(true);
        txtResponse.setWrapStyleWord(true);

        scrResponse = new JScrollPane(txtResponse);
        scrResponse.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrResponse.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrResponse.setWheelScrollingEnabled(true);

        add(lblUSSD);
        add(txtUSSD);
        add(btnSend);
        add(btnReset);
        add(lblResponse);
        add(scrResponse);
    }

    @Override
    protected void releaseResources() {
        try {
            GatewayManager.getService().removeGatewayListener(gatewayListener);
        } catch (IllegalAccessException ignore) {
        }
    }

    @Override
    protected void resized() {
        lblUSSD.setBounds(20, 20, 100, 25);
        txtUSSD.setBounds(130, 20, getWidth() - 140, 25);
        btnSend.setBounds(getWidth() - 60, 50, 50, 30);
        btnReset.setBounds(btnSend.getX() - 55, 50, 50, 30);
        lblResponse.setBounds(20, 85, 100, 25);
        scrResponse.setBounds(130, 85, getWidth() - 140, getHeight() - lblResponse.getY() - 10);
    }

    private void send() {
        try {
            GatewayManager.getService().sendUSSDCommand(txtUSSD.getText());
        } catch (IllegalAccessException ignore) {
        }

        btnSend.setEnabled(false);
        txtUSSD.setText("");
        txtUSSD.requestFocus();
    }
}
