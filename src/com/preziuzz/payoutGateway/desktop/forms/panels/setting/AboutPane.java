package com.preziuzz.payoutGateway.desktop.forms.panels.setting;

import com.preziuzz.payoutGateway.desktop.utility.common.Constants;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JPanelExt;

import javax.swing.*;
import java.awt.*;

/**
 * Represents the about panel.
 *
 * @author Erik P. Yuntantyo
 */
public class AboutPane extends JPanelExt {
    private JLabel lblCopyright;
    private JLabel lblProductLogo;
    private JLabel lblProductName;
    private JLabel lblProductVersion;

    @Override
    protected void preInitialize() {
        setBackground(SystemColor.window);
        setLayout(null);

        lblProductLogo = new JLabel(ImageUtility.ICON_LOGO);

        lblProductName = new JLabel(Constants.PRODUCT_NAME);
        lblProductName.setFont(getFont().deriveFont(Font.BOLD, 24f));
        lblProductName.setForeground(SystemColor.textInactiveText.darker());
        lblProductName.setHorizontalAlignment(SwingConstants.CENTER);

        lblProductVersion = new JLabel(Constants.PRODUCT_VERSION);
        lblProductVersion.setFont(getFont().deriveFont(Font.BOLD, 12f));
        lblProductVersion.setForeground(SystemColor.controlDkShadow);
        lblProductVersion.setHorizontalAlignment(SwingConstants.CENTER);

        lblCopyright = new JLabel("© 2013 by preziuzz");
        lblCopyright.setFont(getFont().deriveFont(10f));
        lblCopyright.setForeground(SystemColor.controlDkShadow);
        lblCopyright.setHorizontalAlignment(SwingConstants.CENTER);

        add(lblProductLogo);
        add(lblProductName);
        add(lblProductVersion);
        add(lblCopyright);
    }

    @Override
    protected void resized() {
        lblProductLogo.setBounds((getWidth() - 116) / 2, 80, 116, 116);
        lblProductName.setBounds(10, lblProductLogo.getY() + lblProductLogo.getHeight() + 30, getWidth() - 20, 30);
        lblProductVersion.setBounds(10, lblProductName.getY() + lblProductName.getHeight() + 5, getWidth() - 20, 25);
        lblCopyright.setBounds(10, lblProductVersion.getY() + lblProductVersion.getHeight() + 50, getWidth() - 20, 20);
    }
}
