package com.preziuzz.payoutGateway.desktop.forms.panels.main.cells;

import com.preziuzz.payoutGateway.desktop.utility.extgui.JTablePanelCell;

import javax.swing.*;
import java.awt.*;

/**
 * Represents the process table cell.
 *
 * @author Erik P. Yuntantyo
 */
public class ProcessCell extends JTablePanelCell {
    private JLabel lblIcon;
    private JLabel lblInfo;

    public ProcessCell() {
        initializeComponents();
    }

    @Override
    public void setData(Object data) {
        ProcessCellModel model = (ProcessCellModel)data;

        lblIcon.setIcon(model.getIcon());
        lblInfo.setText(model.getDescription());
        lblInfo.setToolTipText(model.getDescription());
    }

    private void initializeComponents() {
        lblIcon = new JLabel();
        lblIcon.setBounds(5, 3, 16, 16);

        lblInfo = new JLabel();
        lblInfo.setBounds(30, 3, 600, 16);
        lblInfo.setOpaque(false);

        setLayout(null);
        setSize(new Dimension(1, 22));

        add(lblIcon);
        add(lblInfo);
    }
}
