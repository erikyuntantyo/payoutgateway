package com.preziuzz.payoutGateway.desktop.forms.panels.main.cells;

import com.preziuzz.payoutGateway.desktop.core.model.MemberAccountModel;
import com.preziuzz.payoutGateway.desktop.utility.common.Constants;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JTablePanelCell;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the member account table cell.
 *
 * @author Erik P. Yuntantyo
 */
public class MemberAccountCell extends JTablePanelCell {
    private Map<Integer, ImageIcon> icons;
    private JLabel lblDate;
    private JLabel lblIcon;
    private JLabel lblInfo;
    private JLabel lblName;

    public MemberAccountCell() {
        initializeComponents();
    }

    @Override
    protected void setData(Object data) {
        MemberAccountModel model = (MemberAccountModel)data;

        lblName.setText(model.getName());
        lblInfo.setText(String.format("%s, %s, %s", model.getBankName(), model.getAccountNumber(), DecimalFormat.getCurrencyInstance().format(model.getTotalReceived())));
        lblIcon.setIcon(icons.get(model.getStatus()));
        lblDate.setText(Constants.DATE_FORMAT_SHORT.format(model.getCreatedDate()));
    }

    private void initializeComponents() {
        icons = new HashMap<Integer, ImageIcon>() {{
            put(-1, ImageUtility.ICON_ERROR);
            put(0, ImageUtility.ICON_CROSS);
            put(1, ImageUtility.ICON_TICK);
        }};

        lblName = new JLabel();
        lblName.setBounds(3, 3, 100, 16);
        lblName.setFont(getFont().deriveFont(Font.BOLD));

        lblInfo = new JLabel();
        lblInfo.setBounds(5, 20, 200, 16);

        lblDate = new JLabel();
        lblDate.setBounds(5, 37, 100, 16);

        lblIcon = new JLabel();
        lblIcon.setBounds(285, 3, 16, 16);

        setLayout(null);
        setSize(new Dimension(1, 58));

        add(lblName);
        add(lblInfo);
        add(lblDate);
        add(lblIcon);
    }
}
