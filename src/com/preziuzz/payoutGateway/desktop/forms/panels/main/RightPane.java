package com.preziuzz.payoutGateway.desktop.forms.panels.main;

import com.preziuzz.payoutGateway.desktop.core.model.BankModel;
import com.preziuzz.payoutGateway.desktop.core.model.MemberAccountModel;
import com.preziuzz.payoutGateway.desktop.core.service.BankService;
import com.preziuzz.payoutGateway.desktop.core.service.MemberAccountService;
import com.preziuzz.payoutGateway.desktop.forms.panels.main.cells.MemberAccountCell;
import com.preziuzz.payoutGateway.desktop.utility.common.*;
import com.preziuzz.payoutGateway.desktop.utility.extgui.*;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Represents the right panel.
 *
 * @author Erik P. Yuntantyo
 */
public class RightPane extends JPanelExt {
    private JButtonExt btnExcelBrowse;
    private JButtonExt btnMySqlTestConnection;
    private JButtonExt btnMySqlUpdate;
    private JFileChooser fileChooser;
    private JLabel lblInfo;
    private JLabel lblTabTitle;
    private JLabel lblTitle;
    private CustomTableManager tblAccount;
    private JPanel pnlExcel;
    private JPanel pnlMySql;
    private JSeparator sprTitle;
    private JTabbedPane tabData;
    private JTextFieldExt txtExcelFilePath;
    private JTextFieldExt txtMySqlHost;
    private JTextFieldExt txtMySqlUserId;
    private JPasswordFieldExt txtMySqlPassword;

    private List<MemberAccountModel> dataModel;
    private ExcelReader excelReader;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2D = (Graphics2D)g;

        g2D.setColor(SystemColor.control);
        g2D.drawLine(0, 0, getWidth(), 0);
        g2D.setColor(SystemColor.controlDkShadow);
        g2D.drawLine(0, 0, 0, getHeight());
        g2D.setColor(SystemColor.control);
        g2D.drawLine(1, 0, 1, getHeight());
    }

    @Override
    protected void preInitialize() {
        setBackground(SystemColor.control.darker());
        setLayout(null);
        setPreferredSize(new Dimension(350, 0));

        lblTitle = new JLabel("Daftar Akun Penerima Bonus");
        lblTitle.setFont(getFont().deriveFont(Font.BOLD, 12f));
        lblTitle.setForeground(SystemColor.controlLtHighlight);

        sprTitle = new JSeparator(SwingConstants.HORIZONTAL);

        txtExcelFilePath = new JTextFieldExt("Lokasi Berkas Excel");
        txtExcelFilePath.setEditable(false);
        txtExcelFilePath.setFont(getFont().deriveFont(12f));
        txtExcelFilePath.setPlaceHolderFont(getFont().deriveFont(Font.BOLD, 11f));

        btnExcelBrowse = new JButtonExt("Ambil Berkas", ImageUtility.ICON_ATTACH);
        btnExcelBrowse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileFilter fileFilter = new FileNameExtensionFilter("Excel 97-2003 Workbook (*.xls)", "xls");

                fileChooser = new JFileChooser();
                fileChooser.addChoosableFileFilter(fileFilter);
                fileChooser.setAcceptAllFileFilterUsed(false);
                fileChooser.setDialogTitle("Buka Berkas Excel");
                fileChooser.setFileFilter(fileFilter);
                fileChooser.setMultiSelectionEnabled(false);

                if (fileChooser.showDialog(null, "Buka...") == JFileChooser.APPROVE_OPTION) {
                    JWaitDialog.startDialog(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            updateList(fileChooser.getSelectedFile().getPath());
                        }
                    });
                }
            }
        });

        pnlExcel = new JPanel(null);
        pnlExcel.add(txtExcelFilePath);
        pnlExcel.add(btnExcelBrowse);
        pnlExcel.setBackground(SystemColor.window);

        txtMySqlHost = new JTextFieldExt("Alamat Server");
        txtMySqlHost.setFont(getFont().deriveFont(12f));
        txtMySqlHost.setPlaceHolderFont(getFont().deriveFont(Font.BOLD, 11f));

        txtMySqlUserId = new JTextFieldExt("Identitas");
        txtMySqlUserId.setFont(getFont().deriveFont(12f));
        txtMySqlUserId.setPlaceHolderFont(getFont().deriveFont(Font.BOLD, 11f));

        txtMySqlPassword = new JPasswordFieldExt("Kata Sandi");
        txtMySqlPassword.setFont(getFont().deriveFont(12f));
        txtMySqlPassword.setPlaceHolderFont(getFont().deriveFont(Font.BOLD, 11f));

        btnMySqlTestConnection = new JButtonExt("Tes Koneksi", ImageUtility.ICON_CONNECT);

        btnMySqlUpdate = new JButtonExt("Perbarui Data", ImageUtility.ICON_PUT);
        btnMySqlUpdate.setEnabled(false);

        pnlMySql = new JPanel(null);
        pnlMySql.add(txtMySqlHost);
        pnlMySql.add(txtMySqlUserId);
        pnlMySql.add(txtMySqlPassword);
        pnlMySql.add(btnMySqlUpdate);
        pnlMySql.add(btnMySqlTestConnection);
        pnlMySql.setBackground(SystemColor.window);

        tabData = new JTabbedPane();
        tabData.addTab(null, pnlExcel);
        tabData.addTab(null, pnlMySql);
        tabData.setEnabledAt(1, false);

        lblTabTitle = new JLabel("Import Excel");
        lblTabTitle.setForeground(SystemColor.textInactiveText.darker());
        lblTabTitle.setPreferredSize(new Dimension(60, 20));

        tabData.setTabComponentAt(0, lblTabTitle);

        lblTabTitle = new JLabel("Unduh Database (Belum Tersedia)");
        lblTabTitle.setEnabled(false);
        lblTabTitle.setForeground(SystemColor.textInactiveText.darker());
        lblTabTitle.setPreferredSize(new Dimension(165, 20));

        tabData.setTabComponentAt(1, lblTabTitle);

        tblAccount = CustomTableManager.initializeTable(new MemberAccountCell());
        tblAccount.addRowSelectionChangedListener(new CustomTableRowSelectionListener() {
            @Override
            public void rowSelectionChanged(int row) {
                publish("SELECT_MEMBER", dataModel.get(row));
            }
        });
        tblAccount.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        lblInfo = new JLabel();
        lblInfo.setHorizontalAlignment(SwingConstants.RIGHT);
        lblInfo.setFont(getFont().deriveFont(Font.BOLD, 11f));
        lblInfo.setForeground(SystemColor.controlHighlight);

        add(lblTitle);
        add(sprTitle);
        add(tabData);
        add(tblAccount.getComponent());
        add(lblInfo);

        //TODO: Create excel cell initialization in the setting dialog.
        excelReader = ExcelReader.initialize(
            ExcelColumnHeader.initialize("comments", -1),
            ExcelColumnHeader.initialize("code", 0),
            ExcelColumnHeader.initialize("name", 1),
            ExcelColumnHeader.initialize("bankName", 2),
            ExcelColumnHeader.initialize("accountNumber", 3),
            ExcelColumnHeader.initialize("createdDate", 4, Date.class),
            ExcelColumnHeader.initialize("totalReceived", 5, Double.TYPE));

        reloadUnpaidMemberAccounts();
    }

    @Override
    protected void received(MessageEvent e) {
        switch (e.getMessage()) {
            case "READY":
                btnExcelBrowse.setEnabled(!StringUtility.isNullOrEmpty(SessionManager.getCurrentUser().getBankId()) && (dataModel == null));
                break;
            case "REFRESH":
                reloadUnpaidMemberAccounts();
                break;
        }
    }

    @Override
    protected void resized() {
        lblTitle.setBounds(10, 10, getWidth() - 20, 20);
        sprTitle.setBounds(5, lblTitle.getY() + lblTitle.getHeight() + 5, getWidth() - 10, 5);
        tabData.setBounds(10, sprTitle.getY() + sprTitle.getHeight() + 5, getWidth() - 20, 170);
        txtExcelFilePath.setBounds(5, 10, pnlExcel.getWidth() - 10, 25);
        btnExcelBrowse.setBounds(pnlExcel.getWidth() - 105, pnlExcel.getHeight() - 35, 100, 30);
        txtMySqlHost.setBounds(5, 10, pnlMySql.getWidth() - 10, 25);
        txtMySqlUserId.setBounds(5, txtMySqlHost.getY() + txtMySqlHost.getHeight() + 5, pnlMySql.getWidth() - 10, 25);
        txtMySqlPassword.setBounds(5, txtMySqlUserId.getY() + txtMySqlUserId.getHeight() + 5, pnlMySql.getWidth() - 10, 25);
        btnMySqlUpdate.setBounds(pnlMySql.getWidth() - 105, pnlMySql.getHeight() - 35, 100, 30);
        btnMySqlTestConnection.setBounds(pnlMySql.getWidth() - btnMySqlUpdate.getWidth() - 110, pnlExcel.getHeight() - 35, 100, 30);
        tblAccount.setBounds(10, tabData.getY() + tabData.getHeight() + 5, getWidth() - 20, getHeight() - tabData.getY() - tabData.getHeight() - 35);
        lblInfo.setBounds(10, tblAccount.getY() + tblAccount.getHeight() + 5, tblAccount.getWidth(), 15);
    }

    private synchronized boolean backupList(String sourcePath, String backupPath) {
        File backupFile;
        byte[] buffer;
        Date fileDate;
        DateFormat format;
        InputStream inputStream;
        int length;
        DateFormat newFormat;
        OutputStream outputStream;
        File sourceFile;

        try {
            String javaHome = System.getProperty("java.home");

            buffer = new byte[1024];
            sourceFile = new File(sourcePath);
            format = new SimpleDateFormat("ddMMyyyyHHmmss");
            newFormat = new SimpleDateFormat("ssmmHHyyyyMMdd");
            fileDate = format.parse(sourceFile.getName().substring(0, sourceFile.getName().indexOf(".")));

            if (StringUtility.isNullOrEmpty(backupPath)) {
                backupFile = new File(String.format("%s/lib/security/log/%s.ex.cpy", javaHome, newFormat.format(fileDate)));
            } else {
                backupFile = new File(backupPath);
            }

            if (!backupFile.getParentFile().exists()) {
                if (!backupFile.getParentFile().mkdirs()) {
                    Logger.log("Tidak dapat membuat backup folder.",
                               SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                               Logger.LogType.ERROR);
                    MessageDialog.showError("Tidak dapat membuat backup folder.");
                    return false;
                }
            }

            if (backupFile.exists()) {
                Logger.log("Ditemukan file yang sama di backup folder.",
                           SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                           Logger.LogType.ERROR);
                MessageDialog.showError("Ditemukan file yang sama di backup folder.");
                return false;
            }

            inputStream = new FileInputStream(sourceFile);
            outputStream = new FileOutputStream(backupFile);

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            outputStream.close();
            outputStream.flush();
            inputStream.close();

            return true;
        } catch (ParseException exc) {
            Logger.log("Nama file tidak sesuai.",
                       "RightPane.backupList.ParseException",
                       SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                       Logger.LogType.ERROR);
            MessageDialog.showError("Nama file tidak sesuai.\n(nama file: ddMMyyyyHHmmss.xls/ddMMyyyyHHmmss.xlsx)");
        } catch (IOException ignore) {
            Logger.log("Tidak dapat membuat backup file.",
                       SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                       Logger.LogType.ERROR);
            MessageDialog.showError("Tidak dapat membuat backup file.");
        } finally {
            backupFile = null;
            fileDate = null;
            format = null;
            inputStream = null;
            newFormat = null;
            outputStream = null;
            sourceFile = null;
        }

        return false;
    }

    private void reloadUnpaidMemberAccounts() {
        dataModel = MemberAccountService.getAllUnpaidMemberAccounts();

        if (dataModel != null) {
            btnExcelBrowse.setEnabled(false);
            lblInfo.setText(String.format("%s data belum terkirim", dataModel.size()));
            tblAccount.addData(dataModel);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(100);
                        publish("SELECT_MEMBER", dataModel.get(0));
                    } catch (InterruptedException ignore) {
                    }
                }
            }).start();
        } else {
            lblInfo.setText("0 data belum terkirim");
            lblInfo.setToolTipText(lblInfo.getText());
        }
    }

    private void updateList(String filePath) {
        int errorCount = 0;
        int sentCount = 0;
        int unsentCount = 0;

        if (!backupList(filePath, null)) {
            return;
        }

        txtExcelFilePath.setText(fileChooser.getSelectedFile().getPath());

        dataModel = excelReader.read(fileChooser.getSelectedFile().getPath(), 1, MemberAccountModel.class);

        if (dataModel != null) {
            for (MemberAccountModel model : dataModel) {
                List<MemberAccountModel> tmp = MemberAccountService.findMemberAccount(
                    model.getCode(),
                    model.getName(),
                    model.getBankName(),
                    model.getAccountNumber(),
                    model.getCreatedDate());

                if ((tmp != null) && (tmp.size() == 1)) {
                    model.setId(tmp.get(0).getId());
                    model.setBankCode(tmp.get(0).getBankCode());
                    model.setStatus(tmp.get(0).getStatus());

                    if (tmp.get(0).getStatus() == 0) {
                        unsentCount++;
                    } else if (tmp.get(0).getStatus() == 1) {
                        sentCount++;
                    }

                    continue;
                } else if ((tmp != null) && (tmp.size() > 1)) {
                    Logger.log("Terdapat duplikasi data dari excel didalam database.",
                               "RightPane.updateList.Exception",
                               SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                               Logger.LogType.ERROR
                    );
                    MessageDialog.showError("Terdapat duplikasi data dari excel di dalam database.");
                    SessionManager.dispose();
                    System.exit(0);
                } else {
                    model.setStatus(-1);
                }

                if (model.getStatus() == -1) {
                    List<BankModel> bankModels = BankService.searchBank(model.getBankName());

                    if ((bankModels != null) && (bankModels.size() == 1) && !StringUtility.isNullOrEmpty(SessionManager.getCurrentUser().getBankId())) {
                        if (model.getComments().equals("succeeded") &&
                            (SessionManager.getCurrentBankAccount().getCode().equals(bankModels.get(0).getCode()) ||
                                (!SessionManager.getCurrentBankAccount().getCode().equals(bankModels.get(0).getCode()) &&
                                    SessionManager.getCurrentSettings().isAllowTransBank()))) {
                            model.setBankCode(bankModels.get(0).getCode());
                            model.setStatus(1);
                            model.setId(MemberAccountService.update(model));

                            unsentCount++;
                        } else {
                            model.setStatus(-1);
                            errorCount++;
                        }
                    } else {
                        model.setStatus(-1);
                        errorCount++;
                    }
                }
            }

            lblInfo.setText(String.format("%s belum terkirim | %s terkirim | %s tidak sesuai", unsentCount, sentCount, errorCount));
            tblAccount.addData(dataModel);
            publish("SELECT_MEMBER", dataModel.get(0));
        }
    }
}
