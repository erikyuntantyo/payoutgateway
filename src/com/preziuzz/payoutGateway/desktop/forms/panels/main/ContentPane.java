package com.preziuzz.payoutGateway.desktop.forms.panels.main;

import com.preziuzz.SimpleGateway.GatewayManager;
import com.preziuzz.SimpleGateway.common.GatewayAdapter;
import com.preziuzz.SimpleGateway.common.IncomingMessageEvent;
import com.preziuzz.payoutGateway.desktop.core.model.BankModel;
import com.preziuzz.payoutGateway.desktop.core.model.MemberAccountModel;
import com.preziuzz.payoutGateway.desktop.core.service.BankService;
import com.preziuzz.payoutGateway.desktop.forms.SMSResponderDialog;
import com.preziuzz.payoutGateway.desktop.forms.panels.main.cells.ProcessCell;
import com.preziuzz.payoutGateway.desktop.forms.panels.main.cells.ProcessCellModel;
import com.preziuzz.payoutGateway.desktop.utility.common.*;
import com.preziuzz.payoutGateway.desktop.utility.extgui.CustomTableManager;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JButtonExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JPanelExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JTextFieldExt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Calendar;

/**
 * Represents the content panel.
 *
 * @author Erik P. Yuntantyo
 */
public class ContentPane extends JPanelExt {
    private JButtonExt btnSendSMS;
    private JLabel lblAccountNumber;
    private JLabel lblAccountNumberTitle;
    private JLabel lblIconTitle;
    private JLabel lblInfoMessage;
    private JLabel lblInfoTitle;
    private JLabel lblMoney;
    private JLabel lblMoneyTitle;
    private JLabel lblName;
    private JLabel lblNameTitle;
    private JTextFieldExt txtSMSTemplate;
    private CustomTableManager tblProcess;

    private boolean allowToSend;
    private boolean connected;

    @Override
    protected void preInitialize() {
        setBackground(SystemColor.controlHighlight);
        setLayout(null);

        lblIconTitle = new JLabel(ImageUtility.ICON_INFORMATION);

        lblInfoTitle = new JLabel("Informasi Tujuan Transfer Bonus");
        lblInfoTitle.setFont(getFont().deriveFont(Font.BOLD, 14f));
        lblInfoTitle.setForeground(SystemColor.textInactiveText.darker());

        lblInfoMessage = new JLabel("Tidak ada informasi yang ditampilkan.");
        lblInfoMessage.setFont(getFont().deriveFont(12f));
        lblInfoMessage.setForeground(SystemColor.textInactiveText);

        lblNameTitle = new JLabel("Nama :");
        lblNameTitle.setFont(getFont().deriveFont(Font.BOLD, 12f));
        lblNameTitle.setForeground(SystemColor.textInactiveText);
        lblNameTitle.setHorizontalAlignment(SwingConstants.RIGHT);
        lblNameTitle.setVisible(false);

        lblName = new JLabel();
        lblName.setFont(getFont().deriveFont(Font.BOLD, 13f));
        lblName.setForeground(SystemColor.textInactiveText);
        lblName.setVisible(false);

        lblAccountNumberTitle = new JLabel("Bank / No. Rekening :");
        lblAccountNumberTitle.setFont(getFont().deriveFont(Font.BOLD, 12f));
        lblAccountNumberTitle.setForeground(SystemColor.textInactiveText);
        lblAccountNumberTitle.setHorizontalAlignment(SwingConstants.RIGHT);
        lblAccountNumberTitle.setVisible(false);

        lblAccountNumber = new JLabel();
        lblAccountNumber.setFont(getFont().deriveFont(13f));
        lblAccountNumber.setForeground(SystemColor.textInactiveText);
        lblAccountNumber.setVisible(false);

        lblMoneyTitle = new JLabel("Total Bonus :");
        lblMoneyTitle.setFont(getFont().deriveFont(Font.BOLD, 12f));
        lblMoneyTitle.setForeground(SystemColor.textInactiveText);
        lblMoneyTitle.setHorizontalAlignment(SwingConstants.RIGHT);
        lblMoneyTitle.setVisible(false);

        lblMoney = new JLabel();
        lblMoney.setForeground(SystemColor.textInactiveText);
        lblMoney.setFont(getFont().deriveFont(13f));
        lblMoney.setVisible(false);

        txtSMSTemplate = new JTextFieldExt("Isi Pesan");
        txtSMSTemplate.setFocusable(false);
        txtSMSTemplate.setFont(getFont().deriveFont(Font.BOLD, 12f));
        txtSMSTemplate.setPlaceHolderFont(getFont().deriveFont(Font.BOLD, 12f));

        btnSendSMS = new JButtonExt("SEND", ImageUtility.ICON_GO);
        btnSendSMS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                send();
            }
        });
        btnSendSMS.setEnabled(false);
        btnSendSMS.setFont(getFont().deriveFont(Font.BOLD));

        tblProcess = CustomTableManager.initializeTable(new ProcessCell());
        tblProcess.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        add(lblIconTitle);
        add(lblInfoTitle);
        add(lblInfoMessage);
        add(lblNameTitle);
        add(lblName);
        add(lblAccountNumberTitle);
        add(lblAccountNumber);
        add(lblMoneyTitle);
        add(lblMoney);
        add(txtSMSTemplate);
        add(btnSendSMS);
        add(tblProcess.getComponent());
    }

    @Override
    protected void received(MessageEvent e) {
        switch (e.getMessage()) {
            case "DEVICE_CONNECTED":
                btnSendSMS.setEnabled(allowToSend);
                connected = true;
                break;
            case "DEVICE_DISCONNECTED":
                btnSendSMS.setEnabled(false);
                connected = false;
                break;
            case "READY":
                try {
                    GatewayManager.getService().addGatewayListener(new GatewayAdapter() {
                        @Override
                        public void incomingMessageRead(IncomingMessageEvent e) {
                            if (e.getSender().contains(SessionManager.getCurrentBankAccount().getEncryptedDialNumber())) {
                                tblProcess.addDataRow(0, ProcessCellModel.initialize(e.getContents(), 1));
                                Logger.log(e.getContents(), SessionManager.getCurrentUser().getId(), Logger.LogType.UPDATE);

                                String rest = SMSResponderDialog.showDialog(e.getContents(), !allowToSend);

                                if (!StringUtility.isNullOrEmpty(rest)) {
                                    if (rest.contains("CANCEL")) {
                                        allowToSend = true;

                                        btnSendSMS.setEnabled(connected);
                                        tblProcess.addDataRow(0, ProcessCellModel.initialize("Proses dibatalkan oleh user.", 1));
                                        Logger.log("Proses dibatalkan oleh user.", SessionManager.getCurrentUser().getId(), Logger.LogType.UPDATE);
                                    } else {
                                        allowToSend = false;

                                        btnSendSMS.setEnabled(false);
                                        tblProcess.addDataRow(0, ProcessCellModel.initialize(rest, 1));
                                        Logger.log(rest, SessionManager.getCurrentUser().getId(), Logger.LogType.UPDATE);
                                    }
                                } else {
                                    allowToSend = true;
                                    btnSendSMS.setEnabled(connected);

                                    tblProcess.addDataRow(0, ProcessCellModel.initialize("Proses transfer telah dijalankan", 1));
                                    Logger.log("Proses transfer telah dijalankan", SessionManager.getCurrentUser().getId(), Logger.LogType.UPDATE);

                                    publish("REFRESH_LIST");
                                }
                            }
                        }
                    });
                } catch (IllegalAccessException ignore) {
                }

                break;
            case "SELECT_MEMBER":
                if ((e.getData() != null) && !StringUtility.isNullOrEmpty(SessionManager.getCurrentUser().getBankId())) {
                    MemberAccountModel model = (MemberAccountModel)e.getData();

                    lblInfoMessage.setVisible(false);
                    lblNameTitle.setVisible(true);
                    lblName.setText(model.getName());
                    lblName.setVisible(true);
                    lblAccountNumberTitle.setVisible(true);
                    lblAccountNumber.setText(String.format("%s - %s", model.getBankName(), model.getAccountNumber()));
                    lblAccountNumber.setVisible(true);
                    lblMoneyTitle.setVisible(true);
                    lblMoney.setText(DecimalFormat.getCurrencyInstance().format(model.getTotalReceived()));
                    lblMoney.setVisible(true);

                    if (model.getStatus() == 1) {
                        BankModel bankModel = BankService.getBankByCode(model.getBankCode());

                        allowToSend = true;

                        if (SessionManager.getCurrentBankAccount().getCode().equals(bankModel.getCode())) {
                            txtSMSTemplate.setText(SessionManager
                                                       .getCurrentBankAccount()
                                                       .getSmsFormat1()
                                                       .replace("{BANK_NO}", model.getBankCode())
                                                       .replace("{MONEY}", String.valueOf((int)model.getTotalReceived()))
                                                       .replace("{ACC_NO}", model.getAccountNumber()));
                        } else {
                            txtSMSTemplate.setText(SessionManager
                                                       .getCurrentBankAccount()
                                                       .getSmsFormat2()
                                                       .replace("{BANK_NO}", model.getBankCode())
                                                       .replace("{MONEY}", String.valueOf((int)model.getTotalReceived()))
                                                       .replace("{ACC_NO}", model.getAccountNumber()));
                        }

                        btnSendSMS.setEnabled(allowToSend && connected);
                    } else {
                        allowToSend = false;

                        txtSMSTemplate.setText("");
                        btnSendSMS.setEnabled(allowToSend && connected);
                    }
                } else {
                    allowToSend = false;

                    lblInfoMessage.setVisible(true);
                    lblNameTitle.setVisible(false);
                    lblName.setVisible(false);
                    lblAccountNumberTitle.setVisible(false);
                    lblAccountNumber.setVisible(false);
                    lblMoneyTitle.setVisible(false);
                    lblMoney.setVisible(false);
                    btnSendSMS.setEnabled(allowToSend && connected);
                }

                break;
        }
    }

    @Override
    protected void resized() {
        lblIconTitle.setBounds(10, 10, 32, 32);
        lblInfoTitle.setBounds(
            lblIconTitle.getX() + lblIconTitle.getWidth() + 10,
            lblIconTitle.getY(),
            getWidth() - lblIconTitle.getX() - lblIconTitle.getWidth() - 20,
            lblIconTitle.getHeight());
        lblInfoMessage.setBounds(
            lblInfoTitle.getX() + 20,
            lblIconTitle.getY() + lblIconTitle.getHeight() + 30,
            getWidth() - (lblInfoTitle.getX() + 20) - lblIconTitle.getX(),
            20);
        lblNameTitle.setBounds(50, lblInfoMessage.getY(), 140, 20);
        lblName.setBounds(lblNameTitle.getX() + lblNameTitle.getWidth() + 5, lblNameTitle.getY(), 200, 20);
        lblAccountNumberTitle.setBounds(
            lblNameTitle.getX(),
            lblNameTitle.getY() + lblAccountNumberTitle.getHeight() + 5,
            140,
            20);
        lblAccountNumber.setBounds(lblAccountNumberTitle.getX() + lblAccountNumberTitle.getWidth() + 5, lblAccountNumberTitle.getY(), 200, 20);
        lblMoneyTitle.setBounds(
            lblAccountNumberTitle.getX(),
            lblAccountNumberTitle.getY() + lblAccountNumberTitle.getHeight() + 5,
            140,
            20);
        lblMoney.setBounds(lblMoneyTitle.getX() + lblMoneyTitle.getWidth() + 5, lblMoneyTitle.getY(), 200, 20);
        txtSMSTemplate.setBounds(
            10,
            getHeight() - 200,
            getWidth() - 80,
            30);
        btnSendSMS.setBounds(txtSMSTemplate.getX() + txtSMSTemplate.getWidth(), txtSMSTemplate.getY(), 60, 30);
        tblProcess.setBounds(
            txtSMSTemplate.getX(),
            txtSMSTemplate.getY() + txtSMSTemplate.getHeight() + 5,
            getWidth() - 20,
            200 - txtSMSTemplate.getHeight() - 15);
    }

    private void send() {
        try {
            String contents = String.format("%s - %s : %s",
                                            Constants.DATE_FORMAT.format(Calendar.getInstance().getTime()),
                                            SessionManager.getCurrentBankAccount().getDialNumber(),
                                            txtSMSTemplate.getText());

            allowToSend = false;

            GatewayManager.getService().sendMessage(SessionManager.getCurrentBankAccount().getDialNumber(), txtSMSTemplate.getText());
            tblProcess.addDataRow(0, ProcessCellModel.initialize(contents, 0));
            Logger.log(contents, SessionManager.getCurrentUser().getId(), Logger.LogType.UPDATE);
            btnSendSMS.setEnabled(allowToSend && connected);
        } catch (IllegalAccessException ignore) {
            allowToSend = true;
            btnSendSMS.setEnabled(connected);
        }
    }
}
