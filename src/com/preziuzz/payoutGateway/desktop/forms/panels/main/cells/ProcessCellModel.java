package com.preziuzz.payoutGateway.desktop.forms.panels.main.cells;

import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the process cell model.
 *
 * @author Erik P. Yuntantyo
 */
public class ProcessCellModel {
    private String description;
    private Map<Integer, ImageIcon> icons;
    private int type;

    private ProcessCellModel(String description, int type) {
        icons = new HashMap<Integer, ImageIcon>() {{
            put(-1, ImageUtility.ICON_ERROR);
            put(0, ImageUtility.ICON_SEND);
            put(1, ImageUtility.ICON_RECEIVE);
        }};

        this.description = description;
        this.type = type;
    }

    public static ProcessCellModel initialize(String description, int type) {
        return new ProcessCellModel(description, type);
    }

    public String getDescription() {
        return description;
    }

    public ImageIcon getIcon() {
        return icons.get(type);
    }
}
