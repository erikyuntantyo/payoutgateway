package com.preziuzz.payoutGateway.desktop.forms.panels.main;

import com.preziuzz.payoutGateway.desktop.forms.SettingDialog;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.common.MessageEvent;
import com.preziuzz.payoutGateway.desktop.utility.common.SessionManager;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JButtonExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JPanelExt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Represents the top panel.
 *
 * @author Erik P. Yuntantyo
 */
public class TopPane extends JPanelExt {
    private JButtonExt btnLogOut;
    private JButtonExt btnSetting;
    private JLabel lblConnectionStatus;
    private JLabel lblOperatorName;
    private JLabel lblUserIcon;
    private JLabel lblUserInfo;
    private JSeparator separator;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2D = (Graphics2D)g;
        int w = getWidth();
        int h = getHeight();
        GradientPaint gp = new GradientPaint(0, 0, SystemColor.control, 0, h, SystemColor.control.brighter());

        g2D.setPaint(gp);
        g2D.fillRect(0, 0, w, h);
        g2D.setColor(SystemColor.control.brighter());
        g2D.drawLine(0, 0, getWidth(), 0);
        g2D.setColor(SystemColor.controlDkShadow);
        g2D.drawLine(0, 39, getWidth(), 39);
    }

    @Override
    protected void preInitialize() {
        String userInfo = String.format(
            "%s/%s (%s)",
            SessionManager.getCurrentUser().getId(),
            SessionManager.getCurrentUser().getName(),
            SessionManager.getCurrentUser().getType());

        setLayout(null);
        setPreferredSize(new Dimension(0, 40));

        lblUserIcon = new JLabel(ImageUtility.ICON_USER);

        lblUserInfo = new JLabel(userInfo);
        lblUserInfo.setFont(getFont().deriveFont(Font.BOLD, 11f));
        lblUserInfo.setForeground(SystemColor.textInactiveText.darker());

        btnLogOut = new JButtonExt("Kunci", ImageUtility.ICON_LOCK);
        btnLogOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                publish("SIGN_OUT");
            }
        });
        btnLogOut.setFocusable(false);

        btnSetting = new JButtonExt("Pengaturan", ImageUtility.ICON_SETTING);
        btnSetting.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SettingDialog.showDialog();
            }
        });
        btnSetting.setFocusable(false);

        separator = new JSeparator(SwingConstants.VERTICAL);

        lblOperatorName = new JLabel();
        lblOperatorName.setFont(getFont().deriveFont(Font.BOLD, 11f));
        lblOperatorName.setForeground(SystemColor.textInactiveText.darker());

        lblConnectionStatus = new JLabel(ImageUtility.ICON_SIGNAL);
        lblConnectionStatus.setEnabled(false);

        add(lblUserIcon);
        add(lblUserInfo);
        add(btnLogOut);
        add(btnSetting);
        add(separator);
        add(lblOperatorName);
        add(lblConnectionStatus);
    }

    @Override
    protected void received(MessageEvent e) {
        switch (e.getMessage()) {
            case "DEVICE_CONNECTED" :
                    lblConnectionStatus.setEnabled(true);
                    lblOperatorName.setText(e.getData().toString());
                    lblOperatorName.setBounds(
                        lblConnectionStatus.getX() - lblOperatorName.getFontMetrics(lblOperatorName.getFont()).stringWidth(lblOperatorName.getText()) - 5,
                        (getHeight() - 20) / 2,
                        lblOperatorName.getFontMetrics(lblOperatorName.getFont()).stringWidth(lblOperatorName.getText()),
                        20);

                break;
            case "DEVICE_DISCONNECTED" :
                lblConnectionStatus.setEnabled(false);
                lblOperatorName.setText("");
                lblOperatorName.setBounds(
                    lblConnectionStatus.getX() - lblOperatorName.getFontMetrics(lblOperatorName.getFont()).stringWidth(lblOperatorName.getText()) - 5,
                    (getHeight() - 20) / 2,
                    lblOperatorName.getFontMetrics(lblOperatorName.getFont()).stringWidth(lblOperatorName.getText()),
                    20);

                break;
        }
    }

    @Override
    protected void resized() {
        lblUserIcon.setBounds(5, (getHeight() - 16) / 2, 16, 16);
        lblUserInfo.setBounds(30, (getHeight() - 20) / 2, 250, 20);
        btnLogOut.setBounds(getWidth() - 75, (getHeight() - 30) / 2, 70, 30);
        btnSetting.setBounds(btnLogOut.getX() - 105, (getHeight() - 30) / 2, 100, 30);
        separator.setBounds(btnSetting.getX() - 10, (getHeight() - 30) / 2, 5, 30);
        lblConnectionStatus.setBounds(separator.getX() - 21, (getHeight() - 16) / 2, 16, 16);
        lblOperatorName.setBounds(
            lblConnectionStatus.getX() - lblOperatorName.getFontMetrics(lblOperatorName.getFont()).stringWidth(lblOperatorName.getText()) - 5,
            (getHeight() - 20) / 2,
            lblOperatorName.getFontMetrics(lblOperatorName.getFont()).stringWidth(lblOperatorName.getText()),
            20);
    }
}
