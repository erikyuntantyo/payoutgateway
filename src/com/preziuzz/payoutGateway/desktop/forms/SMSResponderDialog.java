package com.preziuzz.payoutGateway.desktop.forms;

import com.preziuzz.SimpleGateway.GatewayManager;
import com.preziuzz.payoutGateway.desktop.utility.common.Constants;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.common.SessionManager;
import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JButtonExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents the incoming message responder.
 *
 * @author Erik P. Yuntantyo
 */
public class SMSResponderDialog extends JDialog {
    private JButton btnCancel;
    private JButton btnSubmit;
    private JLabel lblIncomingMessage;
    private JLabel lblResponseMessage;
    private JScrollPane scrIncomingMessage;
    private JTextArea txtIncomingMessage;
    private JTextField txtResponseMessage;

    private boolean allowCancel;
    private String response;

    private SMSResponderDialog() {
        initializeComponents();
    }

    public static String showDialog(String incomingMessage, boolean allowCancel) {
        SMSResponderDialog dialog = new SMSResponderDialog();

        dialog.allowCancel = allowCancel;

        dialog.btnCancel.setEnabled(allowCancel);
        dialog.txtIncomingMessage.setText(incomingMessage);
        dialog.setVisible(true);

        return dialog.response;
    }

    private void initializeComponents() {
        int height = 210;
        int width = 500;
        int x = ((int)Constants.SCREEN_SIZE.getWidth() - width) / 2;
        int y = ((int)Constants.SCREEN_SIZE.getHeight() - height) / 2;

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                StringBuilder pattern = new StringBuilder();

                pattern.append(SessionManager.getCurrentBankAccount()
                                             .getSmsResponseFormat()
                                             .replace("{*}", "[\\w\\W]*")
                                             .replace("{D}", "([\\d]*)")
                                             .replace("{A}", "([a-zA-Z]*)"));

                Matcher matcher = Pattern.compile(pattern.toString()).matcher(txtIncomingMessage.getText());
                StringBuilder rest = new StringBuilder();

                if (matcher.matches() && matcher.groupCount() > 0) {
                    for (int i = 0; i < matcher.groupCount(); i++) {
                        rest.append(matcher.group(i + 1));
                    }
                } else {
                    allowCancel = false;
                    btnCancel.setEnabled(false);
                }

                txtResponseMessage.setText(rest.toString());
            }

            @Override
            public void windowClosing(WindowEvent e) {
                if (allowCancel) {
                    if (MessageDialog.showConfirmation("Apakah anda ingin membatalkan respon?")) {
                        response = "CANCEL";
                        dispose();
                    }
                } else {
                    response = null;
                    dispose();
                }
            }
        });
        setBounds(x, y, width, height);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocationByPlatform(false);
        setLocationRelativeTo(null);
        setModal(true);
        setResizable(false);
        setTitle("SMS Responder");

        lblIncomingMessage = new JLabel("SMS Masuk");
        lblIncomingMessage.setBounds(10, 10, 70, 25);
        lblIncomingMessage.setFont(getContentPane().getFont().deriveFont(Font.BOLD));
        lblIncomingMessage.setForeground(SystemColor.textInactiveText.darker());
        lblIncomingMessage.setHorizontalAlignment(SwingConstants.RIGHT);

        txtIncomingMessage = new JTextArea();
        txtIncomingMessage.setEditable(false);
        txtIncomingMessage.setFont(getContentPane().getFont().deriveFont(12f));
        txtIncomingMessage.setLineWrap(true);
        txtIncomingMessage.setWrapStyleWord(true);

        scrIncomingMessage = new JScrollPane(txtIncomingMessage);
        scrIncomingMessage.setBounds(85, 10, getWidth() - 100, 75);
        scrIncomingMessage.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrIncomingMessage.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        lblResponseMessage = new JLabel("SMS Respon");
        lblResponseMessage.setBounds(10, 90, 70, 25);
        lblResponseMessage.setFont(getContentPane().getFont().deriveFont(Font.BOLD));
        lblResponseMessage.setForeground(SystemColor.textInactiveText.darker());
        lblResponseMessage.setHorizontalAlignment(SwingConstants.RIGHT);

        txtResponseMessage = new JTextField();
        txtResponseMessage.setBounds(85, 90, getWidth() - 100, 25);
        txtResponseMessage.setFont(getContentPane().getFont().deriveFont(12f));

        btnCancel = new JButton("Batal");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (MessageDialog.showConfirmation("Apakah anda ingin membatalkan respon?")) {
                    response = "CANCEL";
                    dispose();
                }
            }
        });
        btnCancel.setBounds(10, 140, 75, 30);

        btnSubmit = new JButtonExt("Respon", ImageUtility.ICON_SEND);
        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (allowCancel && !StringUtility.isNullOrEmpty(txtResponseMessage.getText())) {
                    response = String.format("%s - %s : %s",
                                             Constants.DATE_FORMAT.format(Calendar.getInstance().getTime()),
                                             SessionManager.getCurrentBankAccount().getDialNumber(),
                                             txtResponseMessage.getText());

                    try {
                        GatewayManager
                            .getService()
                            .sendMessage(SessionManager.getCurrentBankAccount().getDialNumber(), txtResponseMessage.getText());
                    } catch (IllegalAccessException ignore) {
                    }

                    dispose();
                } else if (allowCancel) {
                    MessageDialog.showWarning("Tidak bisa mengirimkan pesan kosong.");
                } else {
                    response = null;
                    dispose();
                }
            }
        });
        btnSubmit.setBounds(getWidth() - 90, 140, 75, 30);
        btnSubmit.setFont(getContentPane().getFont().deriveFont(Font.BOLD));

        getContentPane().setBackground(SystemColor.control.darker());
        getContentPane().setLayout(null);
        getContentPane().add(lblIncomingMessage);
        getContentPane().add(scrIncomingMessage);
        getContentPane().add(lblResponseMessage);
        getContentPane().add(txtResponseMessage);
        getContentPane().add(btnCancel);
        getContentPane().add(btnSubmit);

        getRootPane().setDefaultButton(btnSubmit);
    }
}
