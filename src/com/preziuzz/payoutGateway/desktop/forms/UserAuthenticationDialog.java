package com.preziuzz.payoutGateway.desktop.forms;

import com.preziuzz.payoutGateway.desktop.Main;
import com.preziuzz.payoutGateway.desktop.core.service.UserService;
import com.preziuzz.payoutGateway.desktop.utility.common.Constants;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import com.preziuzz.payoutGateway.desktop.utility.common.SessionManager;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JButtonExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JPasswordFieldExt;
import com.preziuzz.payoutGateway.desktop.utility.extgui.JTextFieldExt;
import com.preziuzz.simpleCrypto.Graphy;
import com.preziuzz.simpleLic.License;
import com.preziuzz.simpleLic.LicenseType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Represents the user authentication dialog.
 *
 * @author Erik P. Yuntantyo
 */
public class UserAuthenticationDialog extends JDialog {
    private JButtonExt btnClose;
    private JButtonExt btnLogin;
    private JPasswordFieldExt txtPassword;
    private JTextFieldExt txtUserId;

    private UserAuthenticationDialog() {
        super(null, ModalityType.APPLICATION_MODAL);
        initializeComponents();
    }

    /**
     * Shows dialog.
     */
    public static void showDialog() {
        new UserAuthenticationDialog().setVisible(true);
    }

    private void close() {
        if (MessageDialog.showConfirmation("Apakah ingin keluar?", "Keluar Program")) {
            dispose();
            Main.deinitialize();
        }
    }

    private void initializeComponents() {
        String title = "Otentikasi";
        int height = 150;
        int width = 350;
        int x = ((int)Constants.SCREEN_SIZE.getWidth() - width) / 2;
        int y = ((int)Constants.SCREEN_SIZE.getHeight() - height) / 2;

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close();
            }
        });
        setBounds(x, y, width, height);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setIconImage(ImageUtility.ICON_LOGIN.getImage());
        setLocationByPlatform(false);
        setLocationRelativeTo(null);
        setResizable(false);

        txtUserId = new JTextFieldExt("Identitas Pengguna");
        txtUserId.setBounds(10, 10, width - 25, 25);
        txtUserId.setFont(getContentPane().getFont().deriveFont(12f));
        txtUserId.setPlaceHolderFont(getContentPane().getFont().deriveFont(Font.BOLD, 12f));

        txtPassword = new JPasswordFieldExt("Kata Sandi");
        txtPassword.setBounds(10, 40, width - 25, 25);
        txtPassword.setEchoChar('*');
        txtPassword.setFont(getContentPane().getFont().deriveFont(12f));
        txtPassword.setPlaceHolderFont(getContentPane().getFont().deriveFont(Font.BOLD, 12f));


        btnClose = new JButtonExt("Batal");
        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });
        btnClose.setBounds(width - 85, height - 70, 70, 30);
        btnClose.setFont(getContentPane().getFont().deriveFont(11f));

        btnLogin = new JButtonExt("Masuk", ImageUtility.ICON_LOCK_OPEN);
        btnLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                authenticate();
            }
        });
        btnLogin.setBounds(width - 170, height - 70, 80, 30);
        btnLogin.setFont(getContentPane().getFont().deriveFont(Font.BOLD, 11f));

        getContentPane().setLayout(null);
        getContentPane().add(txtUserId);
        getContentPane().add(txtPassword);
        getContentPane().add(btnLogin);
        getContentPane().add(btnClose);

        getRootPane().setDefaultButton(btnLogin);

        if (!License.getLicenseType().equals(LicenseType.FULL) && !License.getLicenseType().equals(LicenseType.RESOLVED)) {
            title = String.format("%s - %s", title, License.getLicenseType());
        }

        setTitle(String.format("%s %s - %s", Constants.PRODUCT_NAME, Constants.PRODUCT_VERSION, title));
        btnLogin.setEnabled(!License.getLicenseType().equals(LicenseType.UNREGISTERED) && !License.getLicenseType().equals(LicenseType.EXPIRED));

        txtPassword.setText("admin123");
        txtUserId.setText("admin");
    }

    private void authenticate() {
        if (txtUserId.getText().isEmpty() || (txtPassword.getPassword().length == 0)) {
            MessageDialog.showWarning("Id pengguna dan kata sandi tidak boleh kosong.");
            txtUserId.requestFocus();
            return;
        }

        try {
            if (UserService.login(txtUserId.getText(), Graphy.encode(new String(txtPassword.getPassword())))) {
                dispose();
                SessionManager.setCurrentUser(UserService.getUserById(txtUserId.getText()));
                MainForm.showForm();
            } else {
                MessageDialog.showWarning("Id pengguna dan kata sandi tidak sesuai.");
                txtPassword.setText("");
                txtUserId.setText("");
                txtUserId.requestFocus();
            }
        } catch (Exception exc) {
            MessageDialog.showError(exc);
            Main.deinitialize();
        }
    }
}
