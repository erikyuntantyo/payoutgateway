package com.preziuzz.payoutGateway.desktop.utility.extgui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.io.Serializable;

/**
 * Represents the custom table label cell renderer.
 *
 * @author Erik P. Yuntantyo
 */
public final class CustomTableLabelCellRenderer extends JLabel implements TableCellRenderer, Serializable {
    public static CustomTableLabelCellRenderer initialize(Font font) {
        return initialize(-1, font);
    }

    public static CustomTableLabelCellRenderer initialize(int horizontalAlignment) {
        return initialize(horizontalAlignment, null);
    }

    public static CustomTableLabelCellRenderer initialize(int horizontalAlignment, Font font) {
        CustomTableLabelCellRenderer cellRenderer = new CustomTableLabelCellRenderer();

        if (font != null) {
            cellRenderer.setFont(font);
        }

        if (horizontalAlignment != -1) {
            cellRenderer.setHorizontalAlignment(horizontalAlignment);
        }

        return cellRenderer;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } else {
            if (row % 2 == 0) {
                setBackground(SystemColor.control.brighter());
            } else {
                setBackground(SystemColor.control);
            }

            setForeground(table.getForeground());
        }

        setBorder(new EmptyBorder(new Insets(0, 4, 0, 4)));
        setOpaque(true);
        setText(value.toString());

        return this;
    }
}
