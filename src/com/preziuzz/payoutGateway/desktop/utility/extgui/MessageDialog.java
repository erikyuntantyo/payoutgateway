package com.preziuzz.payoutGateway.desktop.utility.extgui;

import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;

import javax.swing.*;

/**
 * Represents the message dialog methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class MessageDialog {
    /**
     * Show confirmation dialog.
     *
     * @param message Message to display on the dialog.
     * @return True if "yes" choosed, otherwise false.
     * @see boolean
     */
    public static boolean showConfirmation(String message) {
        return showConfirmation(message, "Konfirmasi");
    }

    /**
     * Show confirmation dialog.
     *
     * @param message Message to display on the dialog.
     * @param title   Confirmation dialog title.
     * @return True if "yes" choosed, otherwise false.
     * @see boolean
     */
    public static boolean showConfirmation(String message, String title) {
        return JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    }

    /**
     * Show error dialog from exception.
     *
     * @param exc Exception value.
     */
    public static void showError(Exception exc) {
        showError(exc, "Kesalahan");
    }

    /**
     * Show error dialog from exception.
     *
     * @param exc   Exception value.
     * @param title Error dialog title.
     */
    public static void showError(Exception exc, String title) {
        String msg = StringUtility.isNullOrEmpty(exc.getMessage()) ?
            String.format("Kesalahan tak terdefinisi: %s", exc) :
            String.format("%s\n%s", exc.getMessage(), exc.getCause());
        showError(msg, title);
    }

    /**
     * Show error dialog.
     *
     * @param message Message to display on the dialog.
     */
    public static void showError(String message) {
        showError(message, "Kesalahan");
    }

    /**
     * Show error dialog.
     *
     * @param message Message to display on the dialog.
     * @param title   Error dialog title.
     */
    public static void showError(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Show information dialog.
     *
     * @param message Message to display on the dialog.
     */
    public static void showInformation(String message) {
        showInformation(message, "Informasi");
    }

    /**
     * Show information dialog.
     *
     * @param message Message to display on the dialog.
     * @param title   Information dialog title.
     */
    public static void showInformation(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Show warning dialog.
     *
     * @param message Message to display on the dialog.
     */
    public static void showWarning(String message) {
        showWarning(message, "Peringatan");
    }

    /**
     * Show warning dialog.
     *
     * @param message Message to display on the dialog.
     * @param title   Warning dialog title.
     */
    public static void showWarning(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.WARNING_MESSAGE);
    }
}
