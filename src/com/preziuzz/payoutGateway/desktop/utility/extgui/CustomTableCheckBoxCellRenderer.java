package com.preziuzz.payoutGateway.desktop.utility.extgui;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.io.Serializable;

/**
 * Represents the custom table checkbox cell renderer.
 *
 * @author Erik P. Yuntantyo
 */
public final class CustomTableCheckBoxCellRenderer extends JCheckBox implements TableCellRenderer, Serializable {
    private int alignment;

    private CustomTableCheckBoxCellRenderer(int alignment) {
        this.alignment = alignment;
    }

    public static CustomTableCheckBoxCellRenderer initialize(int alignment) {
        return new CustomTableCheckBoxCellRenderer(alignment);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } else {
            if (row % 2 == 0) {
                setBackground(SystemColor.control.brighter());
            } else {
                setBackground(SystemColor.control);
            }

            setForeground(table.getForeground());
        }

        setHorizontalAlignment(alignment);
        setOpaque(true);
        setSelected((boolean)value);

        return this;
    }
}
