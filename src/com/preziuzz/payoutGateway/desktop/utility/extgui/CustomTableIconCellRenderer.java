package com.preziuzz.payoutGateway.desktop.utility.extgui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.io.Serializable;
import java.util.Map;

/**
 * Represents the custom table icon cell renderer.
 *
 * @author Erik P. Yuntantyo
 */
public class CustomTableIconCellRenderer extends JLabel implements TableCellRenderer, Serializable {
    private int alignment;
    private Map<Integer, ImageIcon> icons;

    private CustomTableIconCellRenderer(Map<Integer, ImageIcon> icons, int alignment) {
        this.alignment = alignment;

        if (icons != null) {
            this.icons = icons;
        }
    }

    public static CustomTableIconCellRenderer initialize(Map<Integer, ImageIcon> icons, int alignment) {
        return new CustomTableIconCellRenderer(icons, alignment);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setBackground(table.getSelectionBackground());
            setForeground(table.getSelectionForeground());
        } else {
            if (row % 2 == 0) {
                setBackground(SystemColor.control.brighter());
            } else {
                setBackground(SystemColor.control);
            }

            setForeground(table.getForeground());
        }

        setBorder(new EmptyBorder(new Insets(0, 4, 0, 4)));
        setOpaque(true);

        if (value != null) {
            ImageIcon icon;

            if (value instanceof Boolean) {
                icon = icons.get((Boolean)value ? 1 : 0);
            } else {
                icon = icons.get(Integer.parseInt((String)value));
            }

            setHorizontalAlignment(alignment);
            setIcon(icon);
            setSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
        }

        return this;
    }
}
