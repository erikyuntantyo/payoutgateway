package com.preziuzz.payoutGateway.desktop.utility.extgui;

import javax.swing.*;
import java.awt.*;

/**
 * Represents the extended component of JButton.
 *
 * @author Erik P. Yuntantyo
 */
public class JButtonExt extends JButton {
    //TODO: Makes it possible to put icon at the right or the left of text.

    private JLabel iconContainer;
    private JLabel labelContainer;
    private JSeparator separator;

    private final int LEFT_MARGIN = 6;

    public JButtonExt() {
        this(null, null);
    }

    public JButtonExt(Icon icon) {
        this(null, icon);
    }

    public JButtonExt(String text) {
        this(text, null);
    }

    public JButtonExt(String text, Icon icon) {
        this(text, icon, false);
    }

    public JButtonExt(String text, Icon icon, boolean showIconBorder) {
        setLayout(null);

        if (icon != null) {
            iconContainer = new JLabel(icon);
            iconContainer.setSize(icon.getIconWidth(), icon.getIconHeight());

            add(iconContainer);
        }

        if (showIconBorder) {
            separator = new JSeparator(JSeparator.VERTICAL);
            add(separator);
        }

        if (text != null) {
            labelContainer = new JLabel(text);
            labelContainer.setForeground(SystemColor.textInactiveText.darker());
            labelContainer.setHorizontalAlignment(CENTER);
            labelContainer.setVerticalAlignment(CENTER);

            add(labelContainer);
        }
    }

    @Override
    public void setBounds(Rectangle r) {
        super.setBounds(r);

        int _x = LEFT_MARGIN;

        if (iconContainer != null) {
            if (labelContainer != null) {
                iconContainer.setLocation(_x, (r.height - iconContainer.getHeight()) / 2);
                _x += iconContainer.getWidth() + 4;
            } else {
                iconContainer.setLocation((r.width - iconContainer.getWidth()) / 2, (r.height - iconContainer.getHeight()) / 2);
            }
        }

        if ((separator != null) && (labelContainer != null)) {
            separator.setBounds(_x, 4, 4, r.height - 8);
            _x += separator.getWidth() + 2;
        }

        if (labelContainer != null) {
            labelContainer.setBounds(_x, 4, r.width - (_x * 2), r.height - 8);
        }

        repaint();
    }

    @Override
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);

        int _x = LEFT_MARGIN;

        if (iconContainer != null) {
            if (labelContainer != null) {
                iconContainer.setLocation(_x, (height - iconContainer.getHeight()) / 2);
                _x += iconContainer.getWidth() + 4;
            } else {
                iconContainer.setLocation((width - iconContainer.getWidth()) / 2, (height - iconContainer.getHeight()) / 2);
            }
        }

        if ((separator != null) && (labelContainer != null)) {
            separator.setBounds(_x, 4, 4, height - 8);
            _x += separator.getWidth() + 2;
        }

        if (labelContainer != null) {
            labelContainer.setBounds(_x, 4, width - (_x + LEFT_MARGIN), height - 8);
        }

        repaint();
    }

    @Override
    public void setEnabled(boolean b) {
        if (iconContainer != null) {
            iconContainer.setEnabled(b);
        }

        if (separator != null) {
            separator.setEnabled(b);
        }

        if (labelContainer != null) {
            labelContainer.setEnabled(b);
        }

        super.setEnabled(b);
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);

        if (labelContainer != null) {
            labelContainer.setFont(font);
        }
    }

    @Override
    public void setForeground(Color fg) {
        super.setForeground(fg);

        if (labelContainer != null) {
            labelContainer.setForeground(fg);
        }
    }

    @Override
    public void setHorizontalAlignment(int alignment) {
        super.setHorizontalAlignment(alignment);

        if (labelContainer != null) {
            labelContainer.setHorizontalAlignment(alignment);
        }
    }

    @Override
    public void setIcon(Icon defaultIcon) {
        super.setIcon(defaultIcon);

        if (defaultIcon != null) {
            if (iconContainer != null) {
                iconContainer.setIcon(defaultIcon);
            } else {
                int _x = LEFT_MARGIN;

                iconContainer = new JLabel(defaultIcon);

                if (labelContainer != null) {
                    iconContainer.setBounds(_x, (getHeight() - iconContainer.getHeight()) / 2, defaultIcon.getIconWidth(), defaultIcon.getIconHeight());
                    labelContainer.setBounds(_x + iconContainer.getWidth() + 4, 4, getWidth() - (_x + LEFT_MARGIN), getHeight() - 8);
                } else {
                    iconContainer.setBounds(
                        (getWidth() - iconContainer.getWidth()) / 2,
                        (getHeight() - iconContainer.getHeight()) / 2,
                        defaultIcon.getIconWidth(),
                        defaultIcon.getIconHeight());
                }
            }
        }
    }

    @Override
    public void setText(String text) {
        super.setText(text);

        if (labelContainer != null) {
            labelContainer.setText(text);
        } else {
            int _x = LEFT_MARGIN;

            labelContainer = new JLabel(text);

            if (iconContainer != null) {
                iconContainer.setLocation(_x, (getHeight() - iconContainer.getHeight()) / 2);
                labelContainer.setBounds(_x + iconContainer.getWidth() + 4, 4, getWidth() - (_x + LEFT_MARGIN), getHeight() - 8);
            } else {
                labelContainer.setBounds(_x, 4, getWidth() - (_x + LEFT_MARGIN), getHeight() - 8);
            }
        }
    }
}
