package com.preziuzz.payoutGateway.desktop.utility.extgui;

import java.util.EventListener;

/**
 * Represents the custom table row selection listener.
 *
 * @author Erik P. Yuntantyo
 */
public interface CustomTableRowSelectionListener extends EventListener {
    void rowSelectionChanged(int row);
}
