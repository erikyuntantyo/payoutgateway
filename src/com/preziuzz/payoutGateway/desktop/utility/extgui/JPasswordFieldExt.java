package com.preziuzz.payoutGateway.desktop.utility.extgui;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.awt.*;

/**
 * Represents the extended component of JPasswordField.
 *
 * @author Erik P. Yuntantyo
 */
public final class JPasswordFieldExt extends JPasswordField {
    private boolean isEmpty;
    private JLabel lblPlaceHolder;
    private String placeHolder;

    public JPasswordFieldExt() {
        this(null, 0, null);
    }

    public JPasswordFieldExt(String placeHolder) {
        this(null, 0, placeHolder);
    }

    public JPasswordFieldExt(String text, String placeHolder) {
        this(text, 0, placeHolder);
    }

    public JPasswordFieldExt(int column, String placeHolder) {
        this(null, column, placeHolder);
    }

    public JPasswordFieldExt(String text, int column, String placeHolder) {
        this(null, text, column, placeHolder);
    }

    public JPasswordFieldExt(Document doc, String text, int column, String placeHolder) {
        super(doc, text, column);

        this.placeHolder = placeHolder;
        isEmpty = true;
        lblPlaceHolder = new JLabel(placeHolder);

        getDocument().addDocumentListener(getDocumentListener());
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        lblPlaceHolder.setBounds(3, 0, getWidth(), getHeight());
        lblPlaceHolder.setForeground(SystemColor.textInactiveText);

        add(lblPlaceHolder);
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
        lblPlaceHolder.setText(placeHolder);
    }

    public void setPlaceHolderFont(Font f) {
        if (lblPlaceHolder != null) {
            lblPlaceHolder.setFont(f);
        }
    }

    private void checkPlaceHolder() {
        lblPlaceHolder.setVisible(isEmpty && ((placeHolder != null) && !placeHolder.isEmpty()));
    }

    private DocumentListener getDocumentListener() {
        return new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                isEmpty = e.getDocument().getLength() == 0;
                checkPlaceHolder();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                isEmpty = e.getDocument().getLength() == 0;
                checkPlaceHolder();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        };
    }
}
