package com.preziuzz.payoutGateway.desktop.utility.extgui;

/**
 * Represents the custom table cell format listener.
 *
 * @author Erik P. Yuntantyo
 */
public interface CustomTableCellFormatListener {
    String format(Object value);
}
