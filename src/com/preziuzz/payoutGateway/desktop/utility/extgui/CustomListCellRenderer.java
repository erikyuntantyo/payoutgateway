package com.preziuzz.payoutGateway.desktop.utility.extgui;

import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;
import sun.swing.DefaultLookup;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Represents the custom list cell renderer.
 *
 * @author Erik P. Yuntantyo
 */
public final class CustomListCellRenderer extends DefaultListCellRenderer {
    private Border SAFE_NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);
    private Border DEFAULT_NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

    public static CustomListCellRenderer initialize() {
        return new CustomListCellRenderer();
    }

    private Border getNoFocusBorder() {
        Border border = DefaultLookup.getBorder(this, ui, "List.cellNoFocusBorder");

        if (System.getSecurityManager() != null) {
            if (border != null) {
                return border;
            }

            return SAFE_NO_FOCUS_BORDER;
        } else {
            if ((border != null) && ((noFocusBorder == null) || (noFocusBorder == DEFAULT_NO_FOCUS_BORDER))) {
                return border;
            }

            return noFocusBorder;
        }
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        setComponentOrientation(list.getComponentOrientation());

        Border border = null;
        Color bg = null;
        Color fg = null;
        JList.DropLocation dropLocation = list.getDropLocation();

        if ((dropLocation != null) && !dropLocation.isInsert() && (dropLocation.getIndex() == index)) {
            bg = DefaultLookup.getColor(this, ui, "List.dropCellBackground");
            fg = DefaultLookup.getColor(this, ui, "List.dropCellForeground");

            isSelected = true;
        }

        if (isSelected) {
            setBackground(bg == null ? list.getSelectionBackground() : bg);
            setForeground(fg == null ? list.getSelectionForeground() : fg);
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        if ((value != null) && (value instanceof CustomListItem)) {
            CustomListItem listItem = (CustomListItem)value;

            setText(!StringUtility.isNullOrEmpty(listItem.getDisplayMember()) ?
                listItem.getDisplayMember() :
                (listItem.getValueMember() != null) ? listItem.getValueMember().toString() : "");
        } else if (value != null) {
            setText(value.toString());
        } else {
            setText("");
        }

        setEnabled(list.isEnabled());
        setFont(list.getFont());

        if (cellHasFocus) {
            if (isSelected) {
                border = DefaultLookup.getBorder(this, ui, "List.focusSelectedCellHighlightBorder");
            }

            if (border == null) {
                border = DefaultLookup.getBorder(this, ui, "List.focusCellHighlightBorder");
            }
        } else {
            border = getNoFocusBorder();
        }

        setBorder(border);

        return this;
    }
}
