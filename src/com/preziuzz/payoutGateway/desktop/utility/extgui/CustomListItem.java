package com.preziuzz.payoutGateway.desktop.utility.extgui;

import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;

/**
 * Represents the custom list item.
 *
 * @author Erik P. Yuntantyo
 */
public final class CustomListItem {
    private String displayMember;
    private Object valueMember;

    private CustomListItem(Object valueMember, String displayMember) {
        this.displayMember = displayMember;
        this.valueMember = valueMember;
    }

    public static CustomListItem initialize() {
        return new CustomListItem(null, null);
    }

    public static CustomListItem initialize(Object valueMember) {
        return new CustomListItem(valueMember, null);
    }

    public static CustomListItem initialize(Object valueMember, String displayMember) {
        return new CustomListItem(valueMember, displayMember);
    }

    public String getDisplayMember() {
        return displayMember;
    }

    public Object getValueMember() {
        return valueMember;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        CustomListItem listItem = (CustomListItem)obj;

        return (listItem.hashCode() == hashCode()) || ((listItem.valueMember != null) && (listItem.valueMember.hashCode() == hashCode()));
    }

    public Object getValue() {
        return valueMember;
    }

    @Override
    public int hashCode() {
        return ((displayMember != null) ? displayMember.hashCode() : 0) + ((valueMember != null) ? valueMember.hashCode() : 0);
    }

    @Override
    public String toString() {
        return !StringUtility.isNullOrEmpty(displayMember) ?
            displayMember :
            (valueMember != null) ? valueMember.toString() : null;
    }
}
