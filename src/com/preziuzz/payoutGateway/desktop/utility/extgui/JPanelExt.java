package com.preziuzz.payoutGateway.desktop.utility.extgui;

import com.preziuzz.payoutGateway.desktop.utility.common.MessageEvent;
import com.preziuzz.payoutGateway.desktop.utility.common.MessageEventListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.util.List;
import java.util.Vector;

/**
 * Represents the extended component of JPanel.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class JPanelExt extends JPanel {
    private boolean released;
    private List<MessageEventListener> messageEventListeners;

    public JPanelExt() {
        initializeComponents();
    }

    public void addMessageListener(MessageEventListener listener) {
        if (messageEventListeners == null) {
            messageEventListeners = new Vector<>();
        }

        messageEventListeners.add(listener);
    }

    public void subscribe(Component source, String message) {
        subscribe(source, message, null);
    }

    public void subscribe(Component source, String message, Object data) {
        received(new MessageEvent(source, message, data));
    }

    protected abstract void preInitialize();

    protected void publish(String message) {
        publish(message, null);
    }

    protected void publish(String message, Object data) {
        if (messageEventListeners != null) {
            for (Object listener : messageEventListeners) {
                ((MessageEventListener)listener).published(new MessageEvent(this, message, data));
            }
        }
    }

    protected void received(MessageEvent e) {

    }

    protected void releaseResources() {

    }

    protected void resized() {

    }

    protected void shown() {

    }

    private void initializeComponents() {
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                resized();
            }

            @Override
            public void componentShown(ComponentEvent e) {
                shown();
            }
        });

        addHierarchyListener(new HierarchyListener() {
            @Override
            public void hierarchyChanged(HierarchyEvent e) {
                if (e.getChangeFlags() == HierarchyEvent.DISPLAYABILITY_CHANGED) {
                    if (!JPanelExt.this.isDisplayable() && !released) {
                        released = true;
                        releaseResources();
                    }
                }
            }
        });

        preInitialize();
    }
}
