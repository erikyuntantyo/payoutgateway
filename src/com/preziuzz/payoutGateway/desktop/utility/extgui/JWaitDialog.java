package com.preziuzz.payoutGateway.desktop.utility.extgui;

import com.preziuzz.payoutGateway.desktop.utility.common.Constants;
import com.preziuzz.payoutGateway.desktop.utility.common.ImageUtility;
import com.sun.awt.AWTUtilities;

import javax.swing.*;
import java.awt.event.*;

/**
 * Represents the progress dialog.
 *
 * @author Erik P. Yuntantyo
 */
public class JWaitDialog extends JDialog {
    private ActionEvent actionEvent;
    private ActionListener listener;
    private Thread worker;

    private JWaitDialog(ActionListener listener) {
        this.listener = listener;
        initilizeComponents();
    }

    private void initilizeComponents() {
        int height = 180;
        int width = 220;
        int x = ((int)Constants.SCREEN_SIZE.getWidth() - width) / 2;
        int y = ((int)Constants.SCREEN_SIZE.getHeight() - height) / 2;

        JLabel lblProgress = new JLabel(ImageUtility.ICON_WAIT);

        lblProgress.setBounds((width - 220) / 2, ((height - 180) / 2), 220, 200);
        lblProgress.setOpaque(true);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                if (listener != null) {
                    if (actionEvent == null) {
                        actionEvent = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null);
                    }

                    worker = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            listener.actionPerformed(actionEvent);
                            dispose();
                        }
                    });
                    worker.setDaemon(true);
                    worker.start();
                }
            }
        });
        setBounds(x, y, width, height);
        setModal(true);
        setUndecorated(true);

        getContentPane().setLayout(null);
        getContentPane().add(lblProgress);

        AWTUtilities.setWindowOpacity(this, 0.5f);
    }

    public static void startDialog(ActionListener listener) {
        new JWaitDialog(listener).setVisible(true);
    }
}
