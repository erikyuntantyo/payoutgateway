package com.preziuzz.payoutGateway.desktop.utility.extgui;

/**
 * Represents the enumeration of editor state.
 *
 * @author Erik P. Yuntantyo
 */
public enum EditorState {
    ADD_NEW,
    CHANGE_PASSWORD,
    EDIT,
    NONE
}
