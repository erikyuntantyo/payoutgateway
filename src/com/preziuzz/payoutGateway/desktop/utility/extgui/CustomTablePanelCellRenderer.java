package com.preziuzz.payoutGateway.desktop.utility.extgui;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.io.Serializable;

/**
 * Represents the custom table panel cell renderer.
 *
 * @author Erik P. Yuntantyo
 */
public final class CustomTablePanelCellRenderer extends JPanel implements TableCellRenderer, Serializable {
    private JTablePanelCell panelCell;

    public static CustomTablePanelCellRenderer initialize(JTablePanelCell panelCell) {
        return new CustomTablePanelCellRenderer(panelCell);
    }

    private CustomTablePanelCellRenderer(JTablePanelCell panelCell) {
        this.panelCell = panelCell;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        panelCell.setData(value);
        panelCell.setOpaque(true);

        if (isSelected) {
            panelCell.setBackground(table.getSelectionBackground());

            changeComponentsForeground(panelCell.getComponents(), table.getSelectionForeground());
        } else {
            if (row % 2 == 0) {
                panelCell.setBackground(SystemColor.window);
            } else {
                panelCell.setBackground(SystemColor.control);
            }

            changeComponentsForeground(panelCell.getComponents(), table.getForeground());
        }

        setLayout(new BorderLayout());
        add(panelCell);

        return this;
    }

    private void changeComponentsForeground(Component[] components, Color color) {
        if ((components == null) || (components.length <= 0)) {
            return;
        }

        for (Component component : components) {
            if (component instanceof JPanel) {
                changeComponentsForeground(((JPanel)component).getComponents(), color);
            } else if (component instanceof JLabel) {
                component.setForeground(color);
            }
        }
    }
}
