package com.preziuzz.payoutGateway.desktop.utility.extgui;

import javax.swing.*;
import java.util.Map;

/**
 * Represents the custom table column manager.
 *
 * @author Erik P. Yuntantyo
 */
public final class CustomTableColumnManager {
    private int alignment;
    private CustomTableCellFormatListener customTableCellFormatListener;
    private String dataMember;
    private String getterMethod;
    private String headerText;
    private Map<Integer, ImageIcon> icons;
    private boolean isRowNumber;
    private int length;
    private boolean usedCheckBox;

    private CustomTableColumnManager(String headerText, String dataMember, int length, int alignment, boolean isRowNumber, boolean usedCheckBox, Map<Integer, ImageIcon> icons, CustomTableCellFormatListener customTableCellFormatListener) {
        this.alignment = alignment;
        this.customTableCellFormatListener = customTableCellFormatListener;
        this.dataMember = dataMember;
        this.headerText = headerText;
        this.icons = icons;
        this.isRowNumber = isRowNumber;
        this.length = length;
        this.usedCheckBox = usedCheckBox;
    }

    /**
     *
     * @param headerText
     * @param length
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, int length) {
        return initialize(headerText, length, SwingConstants.RIGHT);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @param icons
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember, Map<Integer, ImageIcon> icons) {
        return initialize(headerText, dataMember, -1, icons);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @param length
     * @param icons
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember, int length, Map<Integer, ImageIcon> icons) {
        return new CustomTableColumnManager(headerText, dataMember, length, SwingConstants.CENTER, false, false, icons, null);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @param length
     * @param usedCheckBox
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember, int length, boolean usedCheckBox) {
        return new CustomTableColumnManager(headerText, dataMember, length, SwingConstants.CENTER, false, usedCheckBox, null, null);
    }

    /**
     *
     * @param headerText
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText) {
        return initialize(headerText, null);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember) {
        return initialize(headerText, dataMember, -1, SwingConstants.LEFT, null);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @param customTableCellFormatListener
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember, CustomTableCellFormatListener customTableCellFormatListener) {
        return initialize(headerText, dataMember, -1, SwingConstants.LEFT, customTableCellFormatListener);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @param length
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember, int length) {
        return initialize(headerText, dataMember, length, SwingConstants.LEFT, null);
    }

    /**
     *
     * @param headerText
     * @param length
     * @param alignment
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, int length, int alignment) {
        return initialize(headerText, null, length, alignment);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @param length
     * @param alignment
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember, int length, int alignment) {
        return initialize(headerText, dataMember, length, alignment, null);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @param alignment
     * @param customTableCellFormatListener
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember, int alignment, CustomTableCellFormatListener customTableCellFormatListener) {
        return initialize(headerText, dataMember, -1, alignment, customTableCellFormatListener);
    }

    /**
     *
     * @param headerText
     * @param dataMember
     * @param length
     * @param alignment
     * @param customTableCellFormatListener
     * @return
     */
    public static CustomTableColumnManager initialize(String headerText, String dataMember, int length, int alignment, CustomTableCellFormatListener customTableCellFormatListener) {
        return new CustomTableColumnManager(headerText, dataMember, length, alignment, false, false, null, customTableCellFormatListener);
    }

    /**
     *
     * @return
     */
    public int getAlignment() {
        return alignment;
    }

    /**
     *
     * @return
     */
    public CustomTableCellFormatListener getCustomTableCellFormatListener() {
        return customTableCellFormatListener;
    }

    /**
     *
     * @return
     */
    public String getDataMember() {
        return dataMember;
    }

    /**
     *
     * @return
     */
    public String getGetterMethod() {
        return getterMethod;
    }

    /**
     *
     * @param getterMethod
     */
    public void setGetterMethod(String getterMethod) {
        this.getterMethod = getterMethod;
    }

    /**
     *
     * @return
     */
    public String getHeaderText() {
        return headerText;
    }

    /**
     *
     * @return
     */
    public Map<Integer, ImageIcon> getIcons() {
        return icons;
    }

    /**
     *
     * @return
     */
    public boolean isRowNumber() {
        return isRowNumber;
    }

    /**
     *
     * @return
     */
    public int getLength() {
        return length;
    }

    /**
     *
     * @return
     */
    public boolean isUsedCheckBox() {
        return usedCheckBox;
    }
}
