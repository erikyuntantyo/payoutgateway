package com.preziuzz.payoutGateway.desktop.utility.extgui;

import com.preziuzz.payoutGateway.desktop.utility.common.Logger;
import com.preziuzz.payoutGateway.desktop.utility.common.SessionManager;
import com.preziuzz.payoutGateway.desktop.utility.common.StringUtility;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * Represents the custom table manager.
 *
 * @author Erik P. Yuntantyo
 */
public final class CustomTableManager {
    private List<CustomTableColumnManager> columns;
    private boolean isCustomPanel;
    private List<CustomTableRowSelectionListener> rowSelectionListeners;
    private JScrollPane scrollPane;
    private JTable table;
    private DefaultTableModel tableModel;

    private CustomTableManager(CustomTableColumnManager... columnManagers) {
        table = new JTable();
        scrollPane = new JScrollPane(table);

        if (columnManagers.length > 0) {
            columns = new Vector();

            ((DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);

            table.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (rowSelectionListeners != null) {
                        for (Object listener : rowSelectionListeners) {
                            ((CustomTableRowSelectionListener)listener).rowSelectionChanged(((JTable)e.getSource()).getSelectedRow());
                        }
                    }
                }
            });
            table.getTableHeader().setFont(table.getFont().deriveFont(Font.BOLD));
            table.getTableHeader().setForeground(SystemColor.textInactiveText.darker());
            table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
            table.setPreferredScrollableViewportSize(table.getPreferredSize());
            table.setRowHeight(table.getRowHeight() + 5);
            table.setShowGrid(false);

            tableModel = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            if (tableModel.getColumnCount() < columnManagers.length) {
                boolean noHeader = false;

                for (CustomTableColumnManager column : columnManagers) {
                    if (!noHeader) {
                        noHeader = StringUtility.isNullOrEmpty(column.getHeaderText());
                    }

                    tableModel.addColumn(column.getHeaderText());
                }

                if (noHeader) {
                    table.setTableHeader(null);
                }
            }

            table.setModel(tableModel);
            table.getColumnModel().setColumnMargin(0);

            for (int i = 0; i < columnManagers.length; i++) {
                if (columnManagers[i].getLength() != -1) {
                    table.getColumnModel().getColumn(i).setMaxWidth(columnManagers[i].getLength());
                    table.getColumnModel().getColumn(i).setMinWidth(columnManagers[i].getLength());
                }

                if (columnManagers[i].isUsedCheckBox()) {
                    table.getColumnModel().getColumn(i).setCellRenderer(CustomTableCheckBoxCellRenderer.initialize(columnManagers[i].getAlignment()));
                } else if (columnManagers[i].getIcons() != null) {
                    table.getColumnModel().getColumn(i).setCellRenderer(CustomTableIconCellRenderer.initialize(columnManagers[i].getIcons(), columnManagers[i].getAlignment()));
                } else {
                    table.getColumnModel().getColumn(i).setCellRenderer(CustomTableLabelCellRenderer.initialize(columnManagers[i].getAlignment()));
                }
            }

            Collections.addAll(columns, columnManagers);
        }
    }

    private CustomTableManager(String headerText, int alignment, JTablePanelCell panelCell) {
        columns = new Vector();
        isCustomPanel = true;
        table = new JTable();
        scrollPane = new JScrollPane(table);

        ((DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(alignment);

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (rowSelectionListeners != null) {
                    for (Object listener : rowSelectionListeners) {
                        ((CustomTableRowSelectionListener)listener).rowSelectionChanged(((JTable)e.getSource()).getSelectedRow());
                    }
                }
            }
        });
        table.getTableHeader().setFont(table.getFont().deriveFont(Font.BOLD));
        table.getTableHeader().setForeground(SystemColor.textInactiveText.darker());
        table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        table.setFillsViewportHeight(true);
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        table.setRowHeight(panelCell.getHeight());
        table.setShowGrid(false);

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn(headerText);

        if (StringUtility.isNullOrEmpty(headerText)) {
            table.setTableHeader(null);
        }

        table.setModel(tableModel);
        table.getColumnModel().setColumnMargin(0);
        table.getColumnModel().getColumn(0).setCellRenderer(CustomTablePanelCellRenderer.initialize(panelCell));
    }

    /**
     * Initializes table.
     *
     * @param columns
     * @return
     */
    public static CustomTableManager initializeTable(CustomTableColumnManager... columns) {
        return new CustomTableManager(columns);
    }

    /**
     *
     * @param panelCell
     * @return
     */
    public static CustomTableManager initializeTable(JTablePanelCell panelCell) {
        return initializeTable(null, panelCell);
    }

    /**
     *
     * @param headerText
     * @param panelCell
     * @return
     */
    public static CustomTableManager initializeTable(String headerText, JTablePanelCell panelCell) {
        return initializeTable(headerText, SwingConstants.CENTER, panelCell);
    }

    /**
     *
     * @param headerText
     * @param alignment
     * @param panelCell
     * @return
     *
     * @see SwingConstants
     * @see JLabel#setHorizontalAlignment
     * @beaninfo
     * description: The alignment of the label's content along the X axis.
     *        enum: LEFT     SwingConstants.LEFT
     *              CENTER   SwingConstants.CENTER
     *              RIGHT    SwingConstants.RIGHT
     *              LEADING  SwingConstants.LEADING
     *              TRAILING SwingConstants.TRAILING
     */
    public static CustomTableManager initializeTable(String headerText, int alignment, JTablePanelCell panelCell) {
        return new CustomTableManager(headerText, alignment, panelCell);
    }

    /**
     *
     * @param data
     * @return
     */
    public boolean addData(List<?> data) {
        try {
            int rowNumber = 1;

            Field field;
            Class<?> objectType;
            Object result;
            List<Object> row;
            Class<?> rowType;

            tableModel.setRowCount(0);

            if ((data != null) && (data.size() > 0)) {
                objectType = data.get(0).getClass();

                if (!isCustomPanel) {
                    for (CustomTableColumnManager column : columns) {
                        if (column.getDataMember() == null) {
                            continue;
                        }

                        try {
                            field = objectType.getDeclaredField(column.getDataMember());

                            if (field.getType() == Boolean.TYPE) {
                                if (column.getDataMember().startsWith("is") || column.getDataMember().startsWith("can")) {
                                    column.setGetterMethod(column.getDataMember());
                                } else {
                                    column.setGetterMethod("is" + StringUtility.firstCaseToUpper(column.getDataMember()));
                                }
                            } else {
                                column.setGetterMethod("get" + StringUtility.firstCaseToUpper(column.getDataMember()));
                            }
                        } catch (NoSuchFieldException ignore) {
                            throw new Exception("Field '" + column.getDataMember() + "' cannot be found in the class '" + data.get(0).getClass().getName() + "'.");
                        }
                    }
                }

                for (Object datum : data) {
                    row = new Vector<>();

                    if (!isCustomPanel) {
                        rowType = datum.getClass();

                        for (CustomTableColumnManager column : columns) {
                            if (column.isRowNumber()) {
                                row.add(rowNumber++);
                                continue;
                            }

                            try {
                                result = rowType.getDeclaredMethod(column.getGetterMethod()).invoke(datum);

                                if ((column.getCustomTableCellFormatListener() != null) && (result != null)) {
                                    result = column.getCustomTableCellFormatListener().format(result);
                                    row.add(result);
                                } else {
                                    row.add(result);
                                }
                            } catch (NoSuchMethodException ignore) {
                                throw new Exception("Method '" + column.getGetterMethod() + "' cannot be found in the class '" + datum.getClass().getName() + "'.");
                            }
                        }
                    } else {
                        row.add(datum);
                    }

                    tableModel.addRow(row.toArray());
                }

                table.setModel(tableModel);
                table.getSelectionModel().setSelectionInterval(0, 0);
            }

            return true;
        } catch (RuntimeException exc) {
            String initial = "CustomTableManager.addData.RuntimeException";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        } catch (Exception exc) {
            String initial = "CustomTableManager.addData.Exception";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        }

        return false;
    }

    /**
     *
     * @param dataColumns
     */
    public void addDataRow(int row, Object... dataColumns) {
        if (dataColumns == null) {
            return;
        }

        ((DefaultTableModel)table.getModel()).insertRow(row, dataColumns);
        table.getSelectionModel().setSelectionInterval(0, 0);
    }

    /**
     *
     * @param listener
     */
    public void addRowSelectionChangedListener(CustomTableRowSelectionListener listener) {
        if (rowSelectionListeners == null) {
            rowSelectionListeners = new Vector<>();
        }

        rowSelectionListeners.add(listener);
    }

    /**
     *
     * @return
     */
    public JComponent getComponent() {
        return scrollPane;
    }

    /**
     *
     * @return
     */
    public int getHeight() {
        return scrollPane.getHeight();
    }

    /**
     *
     * @return
     */
    public int getSelectedRow() {
        if (table != null) {
            return table.getSelectedRow();
        } else {
            return -1;
        }
    }

    /**
     *
     * @return
     */
    public int getWidth() {
        return scrollPane.getWidth();
    }

    /**
     *
     * @return
     */
    public int getX() {
        return scrollPane.getX();
    }

    /**
     *
     * @return
     */
    public int getY() {
        return scrollPane.getY();
    }

    /**
     *
     * @param row
     */
    public void removeData(int row) {
        tableModel.removeRow(row);
        table.getSelectionModel().setSelectionInterval(0, 0);
    }

    /**
     *
     * @param rectangle
     */
    public void setBounds(Rectangle rectangle) {
        setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    /**
     *
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void setBounds(int x, int y, int width, int height) {
        scrollPane.setBounds(x, y, width, height);
        table.setBounds(scrollPane.getBounds());
        table.revalidate();
    }

    /**
     *
     * @param enabled
     */
    public void setEnabled(boolean enabled) {
        table.setEnabled(enabled);
    }

    /**
     *
     * @param selectionMode
     *
     * @see JList#setSelectionMode
     * @beaninfo
     * description: The selection mode used by the row and column selection models.
     *        enum: SINGLE_SELECTION            ListSelectionModel.SINGLE_SELECTION
     *              SINGLE_INTERVAL_SELECTION   ListSelectionModel.SINGLE_INTERVAL_SELECTION
     *              MULTIPLE_INTERVAL_SELECTION ListSelectionModel.MULTIPLE_INTERVAL_SELECTION
     */
    public void setSelectionMode(int selectionMode) {
        table.setSelectionMode(selectionMode);
    }
}
