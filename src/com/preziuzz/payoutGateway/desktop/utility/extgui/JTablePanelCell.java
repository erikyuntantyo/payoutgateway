package com.preziuzz.payoutGateway.desktop.utility.extgui;

import javax.swing.*;

/**
 * Represents the table panel cell.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class JTablePanelCell extends JPanel {
    public JTablePanelCell() {
        setSize(1, 30);
    }

    protected void setData(Object data) {

    }
}
