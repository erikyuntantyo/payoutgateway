package com.preziuzz.payoutGateway.desktop.utility.db;

/**
 * Represents the connection string builder methods.
 *
 * @author Erik P. Yuntantyo
 */
public final class ConnectionStringBuilder {
    /**
     *
     */
    public enum ConnectionType {
        /**
         *
         */
        MYSQL,
        /**
         *
         */
        ODBC,
        /**
         *
         */
        SQLITE;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    /**
     *
     */
    public String connectionString = null;

    /**
     *
     */
    public String userId = null;

    /**
     *
     */
    public String password = null;

    private ConnectionType connectionType;

    private ConnectionStringBuilder(ConnectionType connectionType, String filePath, String userId, String password) {
        this.userId = userId;
        this.password = password;
        this.connectionType = connectionType;
        connectionString = String.format("jdbc:%s:%s", connectionType.toString(), filePath);
    }

    /**
     *
     * @param connectionType
     * @param filePath
     * @return
     */
    public static ConnectionStringBuilder getConnectionString(ConnectionType connectionType, String filePath) {
        return getConnectionString(connectionType, filePath, null, null);
    }

    /**
     *
     * @param connectionType
     * @param filePath
     * @param userId
     * @param password
     * @return
     */
    public static ConnectionStringBuilder getConnectionString(ConnectionType connectionType, String filePath, String userId, String password) {
        return new ConnectionStringBuilder(connectionType, filePath, userId, password);
    }

    /**
     * @throws Exception
     */
    public void loadClass() throws Exception {
        try {
            switch (connectionType) {
                case MYSQL:
                    Class.forName("com.mysql.jdbc.Driver");
                    break;
                case ODBC:
                    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                    break;
                case SQLITE:
                    Class.forName("org.sqlite.JDBC");
                    break;
            }
        } catch (ClassNotFoundException exc) {
            throw new Exception(String.format("Class %s cannot be found.", exc.getMessage()));
        }
    }
}
