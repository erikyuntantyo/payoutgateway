package com.preziuzz.payoutGateway.desktop.utility.db;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Represents the annotation type to indicate a field as a primary key.
 *
 * @author Erik P. Yuntantyo
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PrimaryKey {
    boolean generatable() default false;
}
