package com.preziuzz.payoutGateway.desktop.utility.db;

import com.preziuzz.payoutGateway.desktop.core.EntityFactory;

/**
 * Represents the query builder methods.
 *
 * @author Erik P. Yuntantyo
 */
public final class QueryBuilder {
    private boolean hasQuery = false;
    private String query;

    /**
     * @return
     */
    public static QueryBuilder select() {
        QueryBuilder instance = new QueryBuilder();

        instance.hasQuery = true;
        instance.query = "select * from %s";

        return instance;
    }

    /**
     * @param clause
     * @return
     */
    public static QueryBuilder select(String clause) {
        QueryBuilder instance = new QueryBuilder();

        instance.hasQuery = true;
        instance.query = String.format("select %s from %%s", clause);

        return instance;
    }

    public String[] getSelectedFields() {
        if (!hasQuery || query.contains("*")) {
            return null;
        }

        return query.substring("select".length(), query.indexOf("from")).trim().split(",");
    }

    /**
     * @param alias
     * @return
     */
    public QueryBuilder alias(String alias) {
        query += " " + alias;
        return this;
    }

    /**
     * @return
     */
    public String getQuery() {
        return query;
    }

    /**
     * @return
     */
    public boolean hasQuery() {
        return hasQuery;
    }

    /**
     * @param type
     * @param alias
     * @param on
     * @return
     */
    public QueryBuilder join(Class<? extends EntityFactory> type, String alias, String on) {
        if ((type != null) && !on.trim().isEmpty()) {
            query += String.format(" join %s %s on %s", type.getSimpleName(), alias, on);
        }

        return this;
    }

    /**
     * @param limit
     * @return
     */
    public QueryBuilder limit(int limit) {
        limit(0, limit);
        return this;
    }

    /**
     * @param offset
     * @param limit
     * @return
     */
    public QueryBuilder limit(int offset, int limit) {
        if ((limit > 0) && !query.contains("limit")) {
            if (offset >= 0) {
                query += String.format(" limit %d %d", offset, limit);
            } else {
                query += String.format(" limit %d", limit);
            }
        }

        return this;
    }

    /**
     * @param clause
     * @return
     */
    public QueryBuilder orderBy(String clause) {
        if (!clause.trim().isEmpty()) {
            if (!query.contains("order by")) {
                query += " order by " + clause;
            } else {
                query += ", " + clause;
            }
        }

        return this;
    }

    /**
     * @param clausePattern
     * @return
     */
    public QueryBuilder where(String clausePattern) {
        where(clausePattern, "");
        return this;
    }

    /**
     * @param clausePattern
     * @param args
     * @return
     */
    public QueryBuilder where(String clausePattern, Object... args) {
        if (!clausePattern.trim().isEmpty() && (args.length > 0) && !query.contains("where")) {
            query += " where " + String.format(clausePattern.replace("%%", "%%%%"), args);
        }

        return this;
    }
}
