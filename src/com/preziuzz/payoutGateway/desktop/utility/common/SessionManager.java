package com.preziuzz.payoutGateway.desktop.utility.common;

import com.preziuzz.payoutGateway.desktop.core.DatabaseManager;
import com.preziuzz.payoutGateway.desktop.core.model.BankAccountModel;
import com.preziuzz.payoutGateway.desktop.core.model.SettingModel;
import com.preziuzz.payoutGateway.desktop.core.model.UserModel;
import com.preziuzz.payoutGateway.desktop.core.service.BankAccountService;
import com.preziuzz.payoutGateway.desktop.core.service.SettingService;
import com.preziuzz.payoutGateway.desktop.core.service.UserService;
import com.preziuzz.payoutGateway.desktop.utility.db.ConnectionStringBuilder;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;

import java.sql.SQLException;
import java.util.List;

/**
 * Represents the session manager methods.
 *
 * @author Erik P. Yuntantyo
 */
public final class SessionManager {
    private static final String databaseName = "config.dat";
    private static boolean firstRunning = false;
    private static SessionManager instance;

    private BankAccountModel currentBankAccount;
    private SettingModel currentSettings;
    private UserModel currentUser;
    private boolean initialized;

    private SessionManager() {
        currentBankAccount = new BankAccountModel();
        currentSettings = new SettingModel();
        currentUser = new UserModel();
        initialized = false;
    }

    /**
     * Disposes session manager instance and contents.
     */
    public static void dispose() {
        try {
            DatabaseManager.closeConnection();

            instance.currentBankAccount = null;
            instance.currentSettings = null;
            instance.currentUser = null;
            instance = null;
        } catch (SQLException exc) {
            MessageDialog.showError(exc);
            System.exit(0);
        }
    }

    /**
     * Gets session manager instance.
     *
     * @return Instance of session manager.
     */
    public static SessionManager getInstance() {
        if (instance == null) {
            instance = new SessionManager();
        }

        return instance;
    }

    /**
     * Gets current bank account in the session.
     *
     * @return Current bank account in the session.
     */
    public static BankAccountModel getCurrentBankAccount() {
        return instance.currentBankAccount;
    }

    /**
     * Gets current settings in the session.
     *
     * @return Current settings in the session.
     */
    public static SettingModel getCurrentSettings() {
        return instance.currentSettings;
    }

    /**
     * Gets current user in the session.
     *
     * @return Current user in the session.
     */
    public static UserModel getCurrentUser() {
        return instance.currentUser;
    }

    /**
     * Initializes session data.
     *
     * @throws Exception
     */
    public static void initialize() throws Exception {
        instance = new SessionManager();
        instance.initializeSession();
    }

    /**
     * Checks whether the session is initialized.
     *
     * @return True if initialized, otherwise false.
     * @see boolean
     */
    public static boolean isInitialized() {
        return (instance != null) && instance.initialized;
    }

    /**
     * Refresh session data.
     */
    public static void refreshSessionData() {
        instance.currentUser = UserService.getUserById(instance.currentUser.getId());
        instance.currentSettings = SettingService.getSetting();

        if ((instance.currentUser.getBankId() != null) && !instance.currentUser.getBankId().isEmpty()) {
            instance.currentBankAccount = BankAccountService.getBankAccountById(instance.getCurrentUser().getBankId());
        }
    }

    /**
     * Sets current user session.
     *
     * @param model User data model.
     */
    public static void setCurrentUser(UserModel model) {
        instance.currentUser = model;
        instance.initialized = true;

        if ((model.getBankId() != null) && !model.getBankId().isEmpty()) {
            instance.currentBankAccount = BankAccountService.getBankAccountById(model.getBankId());
        }
    }

    private void initializeSession() throws Exception {
        DatabaseManager.createConnection(ConnectionStringBuilder.getConnectionString(ConnectionStringBuilder.ConnectionType.SQLITE, databaseName));

        List<String> tables = DatabaseManager.getTables();
        initialized = false;

        for (DatabaseManager.Table table : DatabaseManager.Table.values()) {
            if (firstRunning) {
                break;
            }

            firstRunning = !tables.contains(table.toString());
        }

        if (firstRunning) {
            if (!DatabaseManager.createTables() || !DatabaseManager.createDefaultData()) {
                return;
            }
        }

        currentSettings = SettingService.getSetting();
    }
}
