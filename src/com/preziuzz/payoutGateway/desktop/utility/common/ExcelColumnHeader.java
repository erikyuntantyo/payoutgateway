package com.preziuzz.payoutGateway.desktop.utility.common;

import java.lang.reflect.Type;

/**
 * Represents the excel column header.
 *
 * @author Erik P. Yuntantyo
 */
public final class ExcelColumnHeader {
    private int columnPosition;
    private String fieldName;
    private Type fieldType;

    private ExcelColumnHeader(String fieldName, int columnPosition, Type fieldType) {
        this.columnPosition = columnPosition;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
    }

    public static ExcelColumnHeader initialize(String headerName, int columnPosition) {
        return initialize(headerName, columnPosition, String.class);
    }

    public static ExcelColumnHeader initialize(String headerName, int columnPosition, Type fieldType) {
        return new ExcelColumnHeader(headerName, columnPosition, fieldType);
    }

    public int getColumnPosition() {
        return columnPosition;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Type getFieldType() {
        return fieldType;
    }
}
