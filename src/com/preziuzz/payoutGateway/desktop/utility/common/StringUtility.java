package com.preziuzz.payoutGateway.desktop.utility.common;

/**
 * Represents the string utility methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class StringUtility {
    /**
     * Decodes septet string.
     *
     * @param value The string value that would be decoded.
     * @param length The length of decode results.
     * @return The decoded string.
     * @see String
     */
    public static String decodeSeptetString(String value, int length) {
        char c;
        int counter = 0;
        int o;
        int olen;
        int r = 0;
        String rest = "";
        int rlen = 0;

        for (int i = 0; ((i + 1) < value.length()) && (counter < length); i += 2) {
            o = Integer.parseInt(value.substring(i, i + 2), 16);
            olen = 8;

            o <<= rlen;
            o |= r;
            olen += rlen;

            c = (char)(o & 127);
            rest += c;
            o >>>= 7;
            olen -= 7;

            r = o;
            rlen = olen;

            counter++;

            if (rlen >= 7) {
                c = (char)(r & 127);
                rest += c;
                r >>>= 7;
                rlen -= 7;
                counter++;
            }
        }

        if ((rlen > 0) && (counter < length)) {
            rest += (char)r;
        }

        return rest;
    }

    /**
     * Encodes septet string.
     *
     * @param value The string value that would be encoded.
     * @return The encoded string.
     * @see String
     */
    public static String encodeSeptetString(String value) {
        String rest = "";
        int bitBuffer = 0;
        int bitBufferLength = 0;

        for (int i = 0; i < value.length() || bitBufferLength >= 8; i++) {
            bitBuffer |= ((value.charAt(i) & ~(1 << 7)) << bitBufferLength);
            bitBufferLength += 7;

            while (bitBufferLength >= 8) {
                rest += String.format("%02X", bitBuffer & 0xFF);
                bitBuffer >>>= 8;
                bitBufferLength -= 8;
            }
        }

        if ((bitBufferLength > 0)) {
            rest += String.format("%02X", bitBuffer & 0xFF);
        }

        return rest;
    }

    /**
     * Changes the first character to uppercase.
     *
     * @param value The string value to be changed.
     * @return String with uppercase first character.
     * @see String
     */
    public static String firstCaseToUpper(String value) {
        String result = "";

        if (value != null) {
            result = Character.toUpperCase(value.charAt(0)) + value.substring(1, value.length());
        }

        return result;
    }

    /**
     * Checks whether the string is null or empty.
     *
     * @param value The string value to be tested.
     * @return True if the value is null or empty, otherwise false.
     * @see String
     */
    public static boolean isNullOrEmpty(String value) {
        return (value == null) || value.isEmpty();
    }

    /**
     * Swaps semi octet string.
     *
     * @param value The string value that would be swapped.
     * @return The swapped string.
     * @see String
     */
    public static String swapSemiOctetString(String value) {
        if ((value.length() % 2) == 1) {
            value += "F";
        }

        StringBuffer buffer = new StringBuffer(value.length());

        for (int i = 0; (i + 1) < value.length(); i = i + 2) {
            buffer.append(value.charAt(i + 1));
            buffer.append(value.charAt(i));
        }

        value = new String(buffer);

        if (((value.length() % 2) == 0) && value.endsWith("F")) {
            value = value.replace("F", "");
        }

        return value;
    }
}
