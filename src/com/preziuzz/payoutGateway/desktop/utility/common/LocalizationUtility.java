package com.preziuzz.payoutGateway.desktop.utility.common;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents the localizations utility.
 *
 * @author Erik P. Yuntantyo
 */
public final class LocalizationUtility {
    public static Map<String, String> getLanguage(String language) {
        String path = LocalizationUtility.class.getResource(String.format("%s/%s/%s.%s", Constants.RESOURCES_PATH, "localization", language, ".xml")).getPath();
        //TODO: Read from localization files, then split the data by rows.
        return new HashMap<>();
    }
}
