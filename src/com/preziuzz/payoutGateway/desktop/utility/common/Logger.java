package com.preziuzz.payoutGateway.desktop.utility.common;

import com.preziuzz.payoutGateway.desktop.core.EntityFactory;
import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Represents the log helper methods.
 *
 * @author Erik P. Yuntantyo
 */
public final class Logger {
    public enum LogType {
        ADMINISTRATION,
        DELETE,
        ERROR,
        LOGIN,
        LOGOUT,
        UPDATE;

        /**
         * @return
         */
        public String getValue() {
            String s = super.toString();
            return s.substring(0, 1) + s.substring(1).toLowerCase();
        }
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Invisible {
        String displayValue() default "******";
    }

    private static Logger instance;

    private String logContents;
    private String logFileName;

    private Logger() {
        initializeLog();
    }

    /**
     * Initializes logger.
     */
    public static void initialize() {
        if (instance == null) {
            instance = new Logger();
        }
    }

    /**
     * Logs the operations and errors.
     *
     * @param message Logged message.
     * @param userId  User id who has done the operations.
     * @param logType The type of log.
     */
    public static void log(String message, String userId, LogType logType) {
        log(message, null, null, userId, logType);
    }

    /**
     * Logs the operations and errors.
     *
     * @param message Logged message.
     * @param type    Data type of EntityFactory.
     * @param userId  User id who has done the operations.
     * @param logType The type of log.
     */
    public static void log(String message, EntityFactory type, String userId, LogType logType) {
        log(message, null, type, userId, logType);
    }

    /**
     * Logs the operations and errors.
     *
     * @param exc     Exception value.
     * @param initial Log initial.
     * @param userId  User id who has done the operations.
     * @param logType The type of log.
     */
    public static void log(Exception exc, String initial, String userId, LogType logType) {
        String message = StringUtility.isNullOrEmpty(exc.getMessage()) ?
            String.format("Uncaught exception: %s", exc) :
            String.format("%s\n%s", exc.getMessage(), exc.getCause());

        log(message, initial, null, userId, logType);
    }

    /**
     * Logs the operations and errors.
     *
     * @param message Logged message.
     * @param initial Log initial.
     * @param userId  User id who has done the operations.
     * @param logType The type of log.
     */
    public static void log(String message, String initial, String userId, LogType logType) {
        log(message, initial, null, userId, logType);
    }

    /**
     * Logs the operations and errors.
     *
     * @param message Logged message.
     * @param initial Log initial.
     * @param type    Data type of EntityFactory.
     * @param userId  User id who has done the operations.
     * @param logType The type of log.
     */
    public static void log(String message, String initial, EntityFactory type, String userId, LogType logType) {
        instance.appendLog(message, initial, type, userId, logType);
    }

    private void appendLog(String message, String initial, EntityFactory entity, String userId, LogType logType) {
        try {
            StringBuilder contents = new StringBuilder();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String newLine = System.getProperty("line.separator");
            Date now = Calendar.getInstance().getTime();

            String data;
            String fieldName;
            Object value;

            contents
                .append(logType).append(" LOG #").append(Integer.toHexString(NumberUtility.convertDateToInt(now))).append(": ")
                .append(!StringUtility.isNullOrEmpty(initial) ? String.format("%s, ", initial) : "")
                .append(String.format("%s, ", userId)).append(dateFormat.format(now)).append(newLine)
                .append("Message: ").append(message).append(newLine);

            if (entity != null) {
                Class<? extends EntityFactory> type = entity.getClass();

                data = "Data:" + newLine;

                for (Field field : type.getDeclaredFields()) {
                    field.setAccessible(true);

                    fieldName = field.getName();

                    if (!field.isAnnotationPresent(Invisible.class)) {
                        value = field.get(entity);
                    } else {
                        value = field.getAnnotation(Invisible.class).displayValue();
                    }

                    if (value != null) {
                        data += String.format("* %s.%s: %s%s", type.getSimpleName(), fieldName, value, newLine);
                    }
                }

                contents.append(data);
            }

            contents.append("-----------------------------------------------------------");

            logContents = contents.toString() + newLine + logContents;

            FileUtility.createFileText(String.format("%s/%s", System.getProperty("user.dir"), logFileName), logContents);
            FileUtility.createFileText(String.format("%s/lib/security/log/%s", System.getProperty("java.home"), logFileName), logContents);
        } catch (RuntimeException exc) {
            MessageDialog.showError(exc, "Logger.appendLog.RuntimeException");
        } catch (Exception exc) {
            MessageDialog.showError(exc, "Logger.appendLog.Exception");
        }
    }

    private void initializeLog() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date today = Calendar.getInstance().getTime();

        logFileName = String.format("%s.log", dateFormat.format(today).replace("-", ""));

        if (FileUtility.isExists(logFileName)) {
            logContents = FileUtility.readFile(logFileName);
        } else {
            logContents = "";
        }
    }
}
