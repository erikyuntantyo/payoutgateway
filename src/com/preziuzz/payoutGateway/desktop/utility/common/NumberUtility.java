package com.preziuzz.payoutGateway.desktop.utility.common;

import java.util.Date;

/**
 * Represents the number utility methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class NumberUtility {
    /**
     * Convert date type to time in integer type.
     *
     * @param date Converted date.
     * @return Time value in integer type from date value.
     * @see int
     */
    public static int convertDateToInt(Date date) {
        return (int)(date.getTime() / 1000l);
    }

    /**
     * Convert integer type of time to date type.
     *
     * @param time Converted time.
     * @return Date value from integer type of time.
     * @see Date
     */
    public static Date convertIntToDate(int time) {
        return new Date(time * 1000l);
    }
}
