package com.preziuzz.payoutGateway.desktop.utility.common;

import java.awt.*;

/**
 * @author Erik P. Yuntantyo
 */
public class MessageEvent {
    private Object data;
    private String message;
    private Component source;

    public MessageEvent(Component source, String message, Object data) {
        this.source = source;
        this.message = message;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public Component getSource() {
        return source;
    }
}
