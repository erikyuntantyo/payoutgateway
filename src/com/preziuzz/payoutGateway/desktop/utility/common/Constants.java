package com.preziuzz.payoutGateway.desktop.utility.common;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Represents the constants that used within the application.
 *
 * @author Erik P. Yuntantyo
 */
public final class Constants {
    public static final String COMPANY_NAME = "GLUTERA™ Glutathione";
    public static final String PRODUCT_NAME = "Payout Gateway";
    public static final String PRODUCT_VERSION = "1.0.0.0";
    public static final String RESOURCES_PATH = "/com/preziuzz/payoutGateway/desktop/resources/";

    public static final Dimension SCREEN_SIZE = new JDialog().getToolkit().getScreenSize();

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static final DateFormat DATE_FORMAT_SHORT = new SimpleDateFormat("dd/MM/yyyy");
}
