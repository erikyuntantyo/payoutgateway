package com.preziuzz.payoutGateway.desktop.utility.common;

import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.Properties;

/**
 * Represents the file utility methods.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class FileUtility {
    /**
     * Copies file.
     *
     * @param fileName            File name.
     * @param destinationFileName Destination file name.
     * @return True if file succeeded to copy, otherwise false.
     * @see boolean
     */
    public static boolean copyTo(String fileName, String destinationFileName) {
        try {
            FileChannel inChannel = new FileInputStream(fileName).getChannel();
            FileChannel outChannel = new FileOutputStream(destinationFileName).getChannel();

            try {
                int maxCount = (64 * 1024 * 1024) - (32 * 1024);
                long size = inChannel.size();
                long position = 0;

                while (position < size) {
                    position += inChannel.transferTo(position, maxCount, outChannel);
                }
            } finally {
                if (inChannel != null) {
                    inChannel.close();
                }

                if (outChannel != null) {
                    outChannel.close();
                }
            }

            return false;
        } catch (FileNotFoundException exc) {
            MessageDialog.showError(exc, "FileUtility.copyTo.FileNotFoundException");
            return false;
        } catch (IOException exc) {
            MessageDialog.showError(exc, "FileUtility.copyTo.IOException");
            return false;
        }
    }

    /**
     * Create file text.
     *
     * @param fileName File name.
     * @param contents File contents.
     * @return True if file succeeded to create, otherwise false.
     * @see boolean
     */
    public static boolean createFileText(String fileName, String contents) {
        try {
            File file = new File(fileName);

            if (!file.getParentFile().exists()) {
                if (!file.getParentFile().mkdirs()) {
                    MessageDialog.showError("Cannot create folder.\nFileUtility.createFileText.CreateDirectoryError");
                }
            }

            PrintWriter writer = new PrintWriter(file, "utf-8");

            writer.write(contents);
            writer.close();
            writer.flush();

            return true;
        } catch (FileNotFoundException exc) {
            MessageDialog.showError(exc, "FileUtility.createFileText.FileNotFoundException");
        } catch (UnsupportedEncodingException exc) {
            MessageDialog.showError(exc, "FileUtility.createFileText.UnsupportedEncodingException");
        }

        return false;
    }

    /**
     * Deletes file.
     *
     * @param fileName File name.
     * @return True if the named file does not exist and was successfully created and false if the named file already exists.
     */
    public static boolean delete(String fileName) {
        File file = new File(fileName);
        return file.delete();
    }

    /**
     * Gets file creation date.
     *
     * @param fileName File name.
     * @return Creation date of file.
     * @see Date
     */
    public static Date getCreationDate(String fileName) {
        try {
            Path filePath = FileSystems.getDefault().getPath(fileName);
            BasicFileAttributes attributes = Files.readAttributes(filePath, BasicFileAttributes.class);

            return new Date(attributes.creationTime().toMillis());
        } catch (IOException exc) {
            MessageDialog.showError(exc, "FileUtility.getCreationDate.IOException");
        }

        return null;
    }

    /**
     * Get file size.
     *
     * @param fileName File name.
     * @return The size of file.
     * @see long
     */
    public static long getSize(String fileName) {
        return new File(fileName).length();
    }

    /**
     * Check whether the file is exists or not.
     *
     * @param fileName File name.
     * @return True if file is exist, otherwise false.
     * @see boolean
     */
    public static boolean isExists(String fileName) {
        File file = new File(fileName);
        return file.exists() && file.isFile();
    }

    /**
     * Read file content.
     *
     * @param fileName
     * @return
     */
    public static String readFile(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            StringBuilder builder = new StringBuilder();
            String line = reader.readLine();

            while (line != null) {
                builder.append(line).append("\n");
                line = reader.readLine();
            }

            reader.close();

            return builder.toString();
        } catch (FileNotFoundException exc) {
            MessageDialog.showError(exc, "FileUtility.readFile.FileNotFoundException");
        } catch (IOException exc) {
            MessageDialog.showError(exc, "FileUtility.readFile.IOException");
        }

        return null;
    }

    /**
     * Read file content by matched line number.
     *
     * @param fileName
     * @param lineNumber
     * @return
     */
    public static String readFileByLine(String fileName, int lineNumber) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = reader.readLine();
            int row = 0;

            while (line != null) {
                if (row == lineNumber) {
                    break;
                }

                row++;
                line = reader.readLine();
            }

            reader.close();

            return line;
        } catch (FileNotFoundException exc) {
            MessageDialog.showError(exc, "FileUtility.readFile.FileNotFoundException");
        } catch (IOException exc) {
            MessageDialog.showError(exc, "FileUtility.readFile.IOException");
        }

        return null;
    }

    /**
     * Read data from xml file.
     *
     * @param fileName     Xml file name.
     * @param key          The key of xml node.
     * @param defaultValue The default value if the value is not exist.
     * @return The value of xml node.
     * @throws IOException If the specified xml file is not found.
     * @see String
     */
    public static String readXMLData(String fileName, String key, String defaultValue) throws IOException {
        Properties properties = new Properties();
        properties.loadFromXML(new FileInputStream(fileName));
        return properties.getProperty(key, defaultValue);
    }

    /**
     * Rename file.
     *
     * @param fileName    File to be renamed.
     * @param newFileName New file name.
     * @return True if file succeeded to rename, otherwise false.
     */
    public static boolean rename(String fileName, String newFileName) {
        File file = new File(fileName);

        if (isExists(newFileName)) {
            MessageDialog.showError("Cannot be renamed to new file.\nFile is already exists.");
            return false;
        }

        return file.renameTo(new File(newFileName));
    }
}
