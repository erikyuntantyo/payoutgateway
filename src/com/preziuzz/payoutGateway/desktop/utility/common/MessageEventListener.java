package com.preziuzz.payoutGateway.desktop.utility.common;

import java.util.EventListener;

/**
 * Represents the interface of message listener.
 *
 * @author Erik P. Yuntantyo
 */
public interface MessageEventListener extends EventListener {
    void published(MessageEvent e);
}
