package com.preziuzz.payoutGateway.desktop.utility.common;

import javax.swing.*;

/**
 * Represents the image utility.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class ImageUtility {
    public static final ImageIcon ICON_ADD = getImageIconResource("add.png");
    public static final ImageIcon ICON_ATTACH = getImageIconResource("attach.png");
    public static final ImageIcon ICON_CANCEL = getImageIconResource("cancel.png");
    public static final ImageIcon ICON_CONNECT = getImageIconResource("connect.png");
    public static final ImageIcon ICON_CROSS = getImageIconResource("cross.png");
    public static final ImageIcon ICON_DELETE = getImageIconResource("delete.png");
    public static final ImageIcon ICON_EDIT = getImageIconResource("edit.png");
    public static final ImageIcon ICON_EDIT_PASSWORD = getImageIconResource("edit_password.png");
    public static final ImageIcon ICON_ERROR = getImageIconResource("error.png");
    public static final ImageIcon ICON_GO = getImageIconResource("go.png");
    public static final ImageIcon ICON_INFORMATION = getImageIconResource("information.png");
    public static final ImageIcon ICON_INFORMATION_SMALL = getImageIconResource("information_small.png");
    public static final ImageIcon ICON_LICENSE = getImageIconResource("license.png");
    public static final ImageIcon ICON_LOCK = getImageIconResource("lock.png");
    public static final ImageIcon ICON_LOCK_OPEN = getImageIconResource("lock_open.png");
    public static final ImageIcon ICON_LOGIN = getImageIconResource("login.png");
    public static final ImageIcon ICON_LOGO = getImageIconResource("logo.png");
    public static final ImageIcon ICON_MAIL = getImageIconResource("mail.png");
    public static final ImageIcon ICON_MAIL_OPEN = getImageIconResource("mail_open.png");
    public static final ImageIcon ICON_MAIN = getImageIconResource("main.png");
    public static final ImageIcon ICON_PUT = getImageIconResource("put.png");
    public static final ImageIcon ICON_RECEIVE = getImageIconResource("receive.png");
    public static final ImageIcon ICON_SAVE = getImageIconResource("save.png");
    public static final ImageIcon ICON_SEARCH = getImageIconResource("search.png");
    public static final ImageIcon ICON_SEND = getImageIconResource("send.png");
    public static final ImageIcon ICON_SETTING = getImageIconResource("setting.png");
    public static final ImageIcon ICON_SIGNAL = getImageIconResource("signal.png");
    public static final ImageIcon ICON_TICK = getImageIconResource("tick.png");
    public static final ImageIcon ICON_USER = getImageIconResource("user.png");
    public static final ImageIcon ICON_WAIT = getImageIconResource("wait.png");

    public static ImageIcon getImageIconResource(String fileName) {
        return new ImageIcon(ImageUtility.class.getResource(String.format("%s/%s/%s", Constants.RESOURCES_PATH, "img", fileName)));
    }
}
