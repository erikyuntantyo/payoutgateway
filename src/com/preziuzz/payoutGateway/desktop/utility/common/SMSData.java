package com.preziuzz.payoutGateway.desktop.utility.common;

import java.util.Date;

/**
 * Represents the model of sms data.
 *
 * @author Erik P. Yuntantyo
 */
public class SMSData {
    private int index;
    private String sender;
    private String contents;
    private Date sentDate;
    private Date receivedDate;
    private boolean read;

    private SMSData(int index, String sender, String contents, Date sentDate, Date receivedDate, boolean read) {
        this.index = index;
        this.sender = sender;
        this.contents = contents;
        this.sentDate = sentDate;
        this.receivedDate = receivedDate;
        this.read = read;
    }

    public static SMSData initialize(int index, String sender, String contents, Date sentDate, Date receivedDate, boolean status) {
        return  new SMSData(index, sender, contents, sentDate, receivedDate, status);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
