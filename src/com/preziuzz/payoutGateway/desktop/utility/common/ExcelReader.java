package com.preziuzz.payoutGateway.desktop.utility.common;

import com.preziuzz.payoutGateway.desktop.utility.extgui.MessageDialog;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;

import java.io.FileInputStream;
import java.util.*;

/**
 * Represents the excel reader methods.
 *
 * @author Erik P. Yuntantyo
 */
public final class ExcelReader {
    private Map<Integer, ExcelColumnHeader> columnHeaders;

    private ExcelReader(ExcelColumnHeader... columnHeaders) {
        if (columnHeaders.length > 0) {
            this.columnHeaders = new HashMap<>();

            for (ExcelColumnHeader columnHeader : columnHeaders) {
                this.columnHeaders.put(columnHeader.getColumnPosition(), columnHeader);
            }
        }
    }

    public static ExcelReader initialize(ExcelColumnHeader... columnHeaders) {
        return new ExcelReader(columnHeaders);
    }

    public <T> List<T> read(String filePath, int startRowIndex, Class<T> modelType) {
        try {
            String comments;
            ExcelColumnHeader excelColumnHeader;
            T row;
            String setter;
            HSSFCell xlsCell;
            HSSFRow xlsRow;

            POIFSFileSystem fileSystem = new POIFSFileSystem(new FileInputStream(filePath));
            HSSFWorkbook workbook = new HSSFWorkbook(fileSystem);
            HSSFSheet sheet = workbook.getSheetAt(0);

            List<T> rows = new Vector<>();

            for (int r = startRowIndex; r < sheet.getPhysicalNumberOfRows(); r++) {
                comments = "";
                xlsRow = sheet.getRow(r);

                if (xlsRow == null) {
                    continue;
                }

                row = modelType.newInstance();

                for (int c = 0; c < xlsRow.getPhysicalNumberOfCells(); c++) {
                    xlsCell = xlsRow.getCell(c);

                    if (xlsCell == null) {
                        continue;
                    }

                    excelColumnHeader = columnHeaders.get(c);

                    if (excelColumnHeader == null) {
                        continue;
                    }

                    setter = String.format("set%s", StringUtility.firstCaseToUpper(excelColumnHeader.getFieldName()));

                    try {
                        switch (xlsCell.getCellType()) {
                            case Cell.CELL_TYPE_NUMERIC:
                                if (excelColumnHeader.getFieldType() == Double.TYPE) {
                                    row.getClass()
                                        .getDeclaredMethod(setter, Double.TYPE)
                                        .invoke(row, xlsCell.getNumericCellValue());
                                } else if (excelColumnHeader.getFieldType() == Date.class) {
                                    row.getClass()
                                        .getDeclaredMethod(setter, Date.class )
                                        .invoke(row, xlsCell.getDateCellValue());
                                } else if (excelColumnHeader.getFieldType() == Integer.TYPE) {
                                    row.getClass()
                                        .getDeclaredMethod(setter, Integer.TYPE)
                                        .invoke(row, (int)xlsCell.getNumericCellValue());
                                } else if (excelColumnHeader.getFieldType() == String.class) {
                                    row.getClass()
                                        .getDeclaredMethod(setter, String.class)
                                        .invoke(row, Long.toString((long)xlsCell.getNumericCellValue()));
                                }

                                break;
                            case Cell.CELL_TYPE_STRING:
                                row.getClass()
                                    .getDeclaredMethod(setter, String.class)
                                    .invoke(row, xlsCell.getStringCellValue());

                                break;
                            case Cell.CELL_TYPE_BOOLEAN:
                                row.getClass()
                                    .getDeclaredMethod(setter, Boolean.TYPE)
                                    .invoke(row, xlsCell.getBooleanCellValue());

                                break;
                            case Cell.CELL_TYPE_ERROR:
                                comments += String.format("Cell[%s, %s] has error, %s;", CellReference.convertNumToColString(c), r, xlsCell.getErrorCellValue());
                                break;
                            case Cell.CELL_TYPE_BLANK:
                            default:
                                comments += String.format("Cell[%s, %s] is empty;", CellReference.convertNumToColString(c), r);

                                if (excelColumnHeader.getFieldType() == Double.TYPE) {
                                    row.getClass()
                                        .getDeclaredMethod(setter, Double.TYPE)
                                        .invoke(row, -1);
                                } else if (excelColumnHeader.getFieldType() == Date.class) {
                                    row.getClass()
                                        .getDeclaredMethod(setter, Date.class)
                                        .invoke(row, new Date());
                                } else if (excelColumnHeader.getFieldType() == Integer.TYPE) {
                                    row.getClass()
                                        .getDeclaredMethod(setter, Integer.TYPE)
                                        .invoke(row, -1);
                                }
                        }
                    } catch (NoSuchMethodException ignore) {
                    }
                }

                if (columnHeaders.get(-1) != null) {
                    setter = String.format("set%s", StringUtility.firstCaseToUpper(columnHeaders.get(-1).getFieldName()));

                    try {
                        row.getClass()
                            .getDeclaredMethod(setter, String.class)
                            .invoke(row, !StringUtility.isNullOrEmpty(comments) ? comments : "succeeded");
                    } catch (NoSuchMethodException ignore) {
                    }
                }

                if (row != null) {
                    rows.add(row);
                }
            }

            return rows;
        } catch (RuntimeException exc) {
            String initial = "EntityFactory.setDataToModel.RuntimeException";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        } catch (Exception exc) {
            String initial = "ExcelReader.read.Exception";

            MessageDialog.showError(exc, initial);
            Logger.log(
                exc,
                initial,
                SessionManager.isInitialized() ? SessionManager.getCurrentUser().getId() : "admin",
                Logger.LogType.ERROR);
        }

        return null;
    }
}
